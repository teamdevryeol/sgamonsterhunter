#pragma once
#include "../Global/Define/KGlobalDefine.h"
class CSkinnedMesh;
class CStaticMesh;
class Player 
{
public:					// Enum
	enum class EPlayerControl
	{
		FREE,
		FORCE,
		NONE
	};
	enum class EPlayerState
	{
		WALK,
		RUN,
		MOVE,
		ATTACK,
		PREBLOCK,
		BLOCK,
		POSTBLOCK,
		LHIT,
		SHIT,
		USING,
		JUMP,
		JUMPING,
		IDLE,
		FORCE,
		NONE
	};

	enum class EAttackType
	{
		WEEK1,
		WEEK2,
		WEEK3,
		WEEK4,
		DASH1,
		SHDATK1,
		SHDATK2,
		NONE
	};

	enum class EPlayerDirection
	{
		FRONT,
		BACK,
		LEFT,
		RIGHT,
		NONE
	};

public:					// getter


public:					// 생성자, 소멸자
	Player(void);
	virtual ~Player(void);

public:					// public 함수
	virtual void init(void);
	virtual void update(void);
	virtual void render(void);

	CC_SYNTHESIZE(float, m_fHp, Hp);
	CC_SYNTHESIZE(float, m_fMaxHp, MaxHp);
	CC_SYNTHESIZE(float, m_fSp, Sp);
	CC_SYNTHESIZE(float, m_fMaxSp, MaxSp);
	CC_SYNTHESIZE(D3DXVECTOR3, m_stPosition, Position);
	CC_SYNTHESIZE(EPlayerState, m_ePlayerState, State);
	CC_SYNTHESIZE(EAttackType, m_eAttackType, Attack);
	CC_SYNTHESIZE(EPlayerControl, m_ePlayerControl, PlayerControl);
	CC_SYNTHESIZE(float, m_fHeight, Height);
	CC_SYNTHESIZE(float, m_fDamage, Damage);
	CC_SYNTHESIZE(float, m_fMoveSpeed, MoveSpd);
	CC_SYNTHESIZE(float, m_fJumpSpeed, JumpSpd);


private:				// private 함수
	
	// 루프애니메이션을 작동한다
	void runLoopAnimation(CSkinnedMesh * a_pMesh,int a_nIndex1, int a_nIndex2);

	// 노말 애니메이션을 관리한다
	void NormalAnimationManager(CSkinnedMesh * a_pMesh);

	// 전투 애니메이션을 관리한다
	void CombatAnimationManager(CSkinnedMesh * a_pMesh);

	// 공격 애니메이션을 관리한다
	void AttackAnimationManager(CSkinnedMesh* a_pMesh);

	// 애니메이션 좌표움직임을 관리한다.
	void AnimPosManager(CSkinnedMesh* a_pMesh);

	// 조작을 관리한다
	void ControlManager(CSkinnedMesh * a_pMesh);

	// 노말조작을 관리한다
	void NoControlManager(CSkinnedMesh* a_pMesh);

	// 아이들 모션으로 돌아온다
	void returnIdle(CSkinnedMesh* a_pMesh);

	// DBL 모션제어
	void ControlDBLMotion(CSkinnedMesh* a_pMesh);

	// 높이를 설정한다
	void setHeightMap(CSkinnedMesh* a_pMesh);

	// 구와 박스를 셋팅한다
	void setHitBox(CSkinnedMesh* a_pMesh);

public:			// getter
	
	STObjectBox & getAtkObjectBox(void);
	STObjectBox & getSAtkObjectBox(void);
	STObjectBox & getHitObjectBox(void);
	CSkinnedMesh* m_pBaseMesh = nullptr;

private:				// private 변수

	CSkinnedMesh* m_pNormalMesh = nullptr;

	CStaticMesh* m_pHitBox = nullptr;
	CStaticMesh* m_pAtkSphere = nullptr;
	CStaticMesh* m_pSAtkSphere = nullptr;
	CStaticMesh* m_pPosBox = nullptr;
	CStaticMesh* m_pPosSphere = nullptr;
	CStaticMesh* m_pSPosSphere = nullptr;

	int m_nAnimIndex = 0;
	int m_nCurrAnimIndex = 0;
	double m_fDbl = 0.0000f;

	bool m_bIsSwap = false;
	bool m_bIsChanged = false;
	bool m_bIsPoison = false;
	bool m_bIsArmed;
	bool m_bIsReturn = false;

	EPlayerDirection m_eDirection = EPlayerDirection::NONE;

	

	// 행동제어
	bool m_bIsCanMove = true;
	bool m_bIsCanAtk = true;
	bool m_bIsCanJump = true;
	std::vector<std::string> m_oAnimationList;
	std::vector<std::string> m_oNoAnimationList;
};