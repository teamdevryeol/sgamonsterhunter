#include "Player.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CStaticMesh.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"

Player::Player(void)
{
	// Do Nothing
}

Player::~Player(void)
{
	// Do Nothing
}

void Player::init(void)
{
	m_pBaseMesh = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Player/WeaponOnly.x",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,
		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pBaseMesh->setPosition(m_stPosition);
	m_pBaseMesh->setTimeScale(1.8f);
	m_pBaseMesh->setRenderEnable(true);

	m_pNormalMesh = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Player/NormalOnly.x",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,
		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pNormalMesh->setPosition(m_stPosition);
	m_pNormalMesh->setTimeScale(1.8f);
	m_pNormalMesh->setRenderEnable(false);

	m_ePlayerState = EPlayerState::IDLE;
	m_oAnimationList = m_pBaseMesh->getAnimationNameList();
	m_oNoAnimationList = m_pNormalMesh->getAnimationNameList();
	m_pNormalMesh->playAnimation(m_oNoAnimationList[0], true);
	m_bIsArmed = true;


	// 피격 박스를 설정한다
	m_pPosBox = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	
	m_pHitBox = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	m_pPosBox->addChildObject(m_pHitBox);


	m_pHitBox->setDebugEnable(true);
	m_pHitBox->setScale(D3DXVECTOR3(50.0f, 50.0, 150.0f));
	//m_pHitBox->setPosition(D3DXVECTOR3(30.0, -30.0f, -30.0f));

	// 타격 구를 설정한다
	m_pPosSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	
	m_pAtkSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	m_pPosSphere->addChildObject(m_pAtkSphere);
	m_pAtkSphere->setScale(D3DXVECTOR3(30.0f, 30.0f, 30.0f));

	m_pAtkSphere->setDebugEnable(true);
	m_pAtkSphere->setPosition(D3DXVECTOR3(90.0f, 80.0f, 37.0f));

	// 타격 구를 설정한다
	m_pSPosSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});

	m_pSAtkSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});

	m_pSPosSphere->addChildObject(m_pSAtkSphere);
	m_pSAtkSphere->setScale(D3DXVECTOR3(45.0f, 45.0f, 45.0f));

	m_pSAtkSphere->setDebugEnable(true);
	m_pSAtkSphere->setPosition(D3DXVECTOR3(0.0f, -80.0f, 50.0f));
	
	// 이동속도와 점프속도를 설정한다
	m_fMoveSpeed = 5.0f;
	m_fJumpSpeed = 5.0f;
}

void Player::update(void)
{

	
	// 전투 매쉬용
	if (m_bIsArmed)
	{
		m_pBaseMesh->setRenderEnable(true);
		m_pNormalMesh->setRenderEnable(false);
		m_fDbl = m_pBaseMesh->getDblPercent();
		m_pBaseMesh->update();
		this->CombatAnimationManager(m_pBaseMesh);
		this->ControlManager(m_pBaseMesh);
		this->ControlDBLMotion(m_pBaseMesh);
		this->AnimPosManager(m_pBaseMesh);
		this->setHeightMap(m_pBaseMesh);
		this->setHitBox(m_pBaseMesh);

		
	}

	else
	{
		// 노말 매쉬용
		m_pNormalMesh->setRenderEnable(true);
		m_pBaseMesh->setRenderEnable(false);
		m_fDbl = m_pNormalMesh->getDblPercent();
		m_pNormalMesh->update();
		this->NoControlManager(m_pNormalMesh);
		this->ControlDBLMotion(m_pNormalMesh);
		this->AnimPosManager(m_pNormalMesh);
		this->NormalAnimationManager(m_pNormalMesh);
		this->setHeightMap(m_pNormalMesh);
		this->setHitBox(m_pNormalMesh);

	}

	//printf("%.2f\n", m_fDbl);

}

void Player::render(void)
{
	if (m_bIsArmed)
	{
	m_pBaseMesh->render();
	}
	else
	{
	m_pNormalMesh->render();
	}

}

void Player::runLoopAnimation(CSkinnedMesh * a_pMesh, int a_nIndex1, int a_nIndex2)
{
	auto &rAnimList = a_pMesh->getAnimationNameList();
	m_fDbl = a_pMesh->getDblPercent();
	if (m_nCurrAnimIndex != a_nIndex1 &&
		m_nCurrAnimIndex != a_nIndex2)
	{
		m_bIsSwap = false;
		m_bIsChanged = false;
		m_nCurrAnimIndex = a_nIndex1;
		a_pMesh->playAnimation(rAnimList[m_nCurrAnimIndex], false);

	}
	else
	{
		if (m_nCurrAnimIndex >= 0)
		{
			if (m_fDbl > 0.8f)
			{
				a_pMesh->playAnimation(rAnimList[m_nCurrAnimIndex], false);
				m_bIsChanged = true;
			}

			if (m_fDbl == 0.0f && m_bIsChanged)
			{
				if (m_bIsSwap)
				{
					m_nCurrAnimIndex = a_nIndex1;
					m_bIsSwap = false;
				}
				else
				{
					m_nCurrAnimIndex = a_nIndex2;
					m_bIsSwap = true;
				}
				m_bIsChanged = false;
			}

		}
	}

}

void Player::NormalAnimationManager(CSkinnedMesh * a_pMesh)
{
	if (!m_bIsArmed)
	{
		switch (m_ePlayerState)
		{
		case EPlayerState::IDLE: a_pMesh->playAnimation(m_oNoAnimationList[0], true); break;
		case EPlayerState::WALK: a_pMesh->playAnimation(m_oNoAnimationList[1], true); break;
		case EPlayerState::RUN: a_pMesh->playAnimation(m_oNoAnimationList[2], true); break;
		}


	}
}

void Player::CombatAnimationManager(CSkinnedMesh * a_pMesh)
{
	if (m_bIsArmed)
	{
		switch (m_ePlayerState)
		{
		case EPlayerState::ATTACK : this->AttackAnimationManager(a_pMesh); break;
		case EPlayerState::MOVE : a_pMesh->playAnimation(m_oAnimationList[3], true); break;
		case EPlayerState::PREBLOCK: a_pMesh->playAnimation(m_oAnimationList[4], true); break;
		case EPlayerState::BLOCK: a_pMesh->playAnimation(m_oAnimationList[5], true); break;
		case EPlayerState::POSTBLOCK: a_pMesh->playAnimation(m_oAnimationList[6], true); break;
		case EPlayerState::LHIT: ; break;
		case EPlayerState::SHIT: a_pMesh->playAnimation(m_oAnimationList[21], false); break;
		case EPlayerState::IDLE: a_pMesh->playAnimation(m_oAnimationList[0], true); break;
		case EPlayerState::JUMP :
		{
			if (IS_KEY_DOWN(DIK_A))
			{
				a_pMesh->playAnimation(m_oAnimationList[8], false);
				m_ePlayerState = EPlayerState::JUMPING;
				m_eDirection = EPlayerDirection::LEFT;
			}
			else if (IS_KEY_DOWN(DIK_D))
			{
				a_pMesh->playAnimation(m_oAnimationList[9], false);
				m_ePlayerState = EPlayerState::JUMPING;
				m_eDirection = EPlayerDirection::RIGHT;
			}
			else
			{
				// 기본 점프
			}
		}
		break;
		}


	}
}

void Player::AttackAnimationManager(CSkinnedMesh * a_pMesh)
{
	switch (m_eAttackType)
	{
	case EAttackType::WEEK1: a_pMesh->playAnimation(m_oAnimationList[11], false); break;
	case EAttackType::WEEK2: a_pMesh->playAnimation(m_oAnimationList[12], false); break;
	case EAttackType::WEEK3: a_pMesh->playAnimation(m_oAnimationList[15], false); break;
	case EAttackType::WEEK4: a_pMesh->playAnimation(m_oAnimationList[16], false); break;
	case EAttackType::DASH1: a_pMesh->playAnimation(m_oAnimationList[14], false); break;
	case EAttackType::SHDATK1: a_pMesh->playAnimation(m_oAnimationList[17], false); break;
	case EAttackType::SHDATK2: a_pMesh->playAnimation(m_oAnimationList[18], false); break;
	
	}
}



void Player::AnimPosManager(CSkinnedMesh* a_pMesh)
{
	// 구르기 모션
	// {
	if (m_ePlayerState == EPlayerState::JUMPING)
	{
		if (m_eDirection == EPlayerDirection::LEFT)
		{
			a_pMesh->moveByForwardDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}

		else if (m_eDirection == EPlayerDirection::RIGHT)
		{
			a_pMesh->moveByForwardDirection(-m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	}
	// }

	// 3타 회전 공격
	if (m_eAttackType == EAttackType::WEEK3)
	{
		if (m_fDbl < 0.4f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}

	}

	if (m_eAttackType == EAttackType::WEEK4)
	{
		if (m_fDbl < 0.3f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}

	}

	// 점프 공격 모션
	if (m_eAttackType == EAttackType::DASH1)
	{
		if (m_fDbl < 0.5f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	
	}

	// 방패 공격 모션
	if (m_eAttackType == EAttackType::SHDATK1)
	{
		if (m_fDbl < 0.2f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	}
	if (m_eAttackType == EAttackType::SHDATK2)
	{
		if (m_fDbl < 0.2f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	}
}

void Player::ControlManager(CSkinnedMesh * a_pMesh)
{
	// 캐릭터를 회전한다
	if (IS_KEY_DOWN(DIK_Q))
	{
		a_pMesh->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
	}
	else if (IS_KEY_DOWN(DIK_E))
	{
		a_pMesh->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
	}
	// 움직임을 설정한다
	if (m_bIsCanMove)
	{
		// 이동 
		// {
		if (IS_KEY_DOWN(DIK_W))
		{
			a_pMesh->moveByRightDirection(m_fMoveSpeed * GET_DELTA_TIME()), true;
			m_ePlayerState = EPlayerState::MOVE;
		}
		// 추후 작업
		//else if (IS_KEY_DOWN(DIK_S))
		//{
		//	a_pMesh->moveByRightDirection(-m_fMoveSpeed * GET_DELTA_TIME()),true;
		//	m_ePlayerState = EPlayerState::MOVE;
		//}

		if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
		{
			m_ePlayerState = EPlayerState::IDLE;
		}
		// }

	}
	// 구르기
		// {
	if (IS_KEY_PRESSED(DIK_SPACE) &&
		m_bIsCanJump)
	{
		m_ePlayerState = EPlayerState::JUMP;
		m_bIsCanAtk = false;
		m_bIsCanMove = false;
		m_bIsReturn = true;

	}

	// }

	// 공격 모션
	if (IS_MOUSE_BUTTON_PRESSED(EMouseButton::LEFT) && 
		m_bIsCanAtk)
	{
		m_bIsReturn = false;
		m_bIsCanMove = false;
		m_bIsCanJump = false;
		m_ePlayerState = EPlayerState::ATTACK;
		
		if (m_eAttackType == EAttackType::NONE &&
			IS_KEY_DOWN(DIK_W))
		{
			m_eAttackType = EAttackType::DASH1;
			m_bIsReturn = true;
			return;
		}

		else if (m_eAttackType == EAttackType::NONE)
		{
			m_eAttackType = EAttackType::WEEK1;
			m_bIsReturn = true;
			return;
		}
	
		if (m_fDbl > 0.35f)
		{
			switch (m_eAttackType)
			{
			case EAttackType::WEEK1: m_eAttackType = EAttackType::WEEK2; break;
			case EAttackType::DASH1: m_eAttackType = EAttackType::WEEK2; break;
			case EAttackType::WEEK2: m_eAttackType = EAttackType::WEEK3; break;
			case EAttackType::WEEK3: m_eAttackType = EAttackType::WEEK4; break;
			}
		}

		m_bIsReturn = true;
	}

	// 방어 모션
	if (IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT) &&
		m_ePlayerState != EPlayerState::ATTACK)
	{
		m_bIsReturn = false;
		m_bIsCanMove = false;
		m_bIsCanJump = false;
		
		if (m_ePlayerState == EPlayerState::IDLE ||
			m_ePlayerState == EPlayerState::MOVE)
		{
			m_ePlayerState = EPlayerState::PREBLOCK;
		}

		if (m_ePlayerState == EPlayerState::PREBLOCK &&
			m_fDbl > 0.5f)
		{
			m_ePlayerState = EPlayerState::BLOCK;
		}

	}

	else if (IS_MOUSE_BUTTON_RELEASED(EMouseButton::RIGHT) &&
		m_ePlayerState != EPlayerState::ATTACK)
	{
		m_ePlayerState = EPlayerState::POSTBLOCK;
		m_bIsReturn = true;
	}

	// 쉴드 공격
	if (IS_MOUSE_BUTTON_PRESSED(EMouseButton::RIGHT)
		&&m_ePlayerState == EPlayerState::ATTACK)
	{

		
		if (m_fDbl > 0.35f && m_eAttackType == EAttackType::WEEK2)
		{
			m_bIsReturn = false;
			m_bIsCanMove = false;
			m_bIsCanJump = false;
			m_eAttackType = EAttackType::SHDATK1;
		}
		else if (m_fDbl > 0.35f && m_eAttackType == EAttackType::SHDATK1)
		{
			m_bIsReturn = false;
			m_bIsCanMove = false;
			m_bIsCanJump = false;
			m_eAttackType = EAttackType::SHDATK2;
		}
		

		m_bIsReturn = true;
	}
}

void Player::NoControlManager(CSkinnedMesh* a_pMesh)
{
	// 캐릭터를 회전한다
	if (IS_KEY_DOWN(DIK_Q))
	{
		a_pMesh->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
	}
	else if (IS_KEY_DOWN(DIK_E))
	{
		a_pMesh->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
	}
	// 움직임을 설정한다
	if (m_bIsCanMove)
	{
		// 이동 
		// {

		if (IS_KEY_DOWN(DIK_LSHIFT))
		{
			if (IS_KEY_DOWN(DIK_W))
			{
				a_pMesh->moveByRightDirection(m_fMoveSpeed * GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::RUN;
			}
			else if (IS_KEY_DOWN(DIK_S))
			{
				a_pMesh->moveByRightDirection(-m_fMoveSpeed * GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::RUN;
			}

			if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
			{
				m_ePlayerState = EPlayerState::IDLE;
			}
		}

		else
		{
			if (IS_KEY_DOWN(DIK_W))
			{
				a_pMesh->moveByRightDirection(m_fMoveSpeed/2.0f * GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::WALK;
			}
			else if (IS_KEY_DOWN(DIK_S))
			{
				a_pMesh->moveByRightDirection(-m_fMoveSpeed/2.0f * GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::WALK;
			}

			if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
			{
				m_ePlayerState = EPlayerState::IDLE;
			}
		}
	
		// }

	}
}

void Player::ControlDBLMotion(CSkinnedMesh* a_pMesh)
{
	// 아이들로 리턴
	if (m_bIsReturn)
	{
	
		if (m_fDbl > 0.95)
		{
			this->returnIdle(a_pMesh);
			m_bIsReturn = false;
		}
		// 점프제어
		else if (m_fDbl > 0.5)
		{
			m_bIsCanJump = true;
		}
	}

}
void Player::setHeightMap(CSkinnedMesh * a_pMesh)
{
	if (m_ePlayerControl != EPlayerControl::FORCE)
	{
		auto pstPos = a_pMesh->getPosition();
		m_stPosition = pstPos;
		a_pMesh->setPosition(D3DXVECTOR3(pstPos.x,
			m_fHeight + 1.8f, pstPos.z));
	}
	
	else
	{
		a_pMesh->setPosition(D3DXVECTOR3(m_stPosition.x, m_fHeight + 2.5f,m_stPosition.z));
	}
}

void Player::setHitBox(CSkinnedMesh * a_pMesh)
{
	m_pPosBox->update();
	m_pHitBox->update();
	m_pPosSphere->update();
	m_pAtkSphere->update();
	m_pSPosSphere->update();
	m_pSAtkSphere->update();

	m_pPosSphere->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_SW"));
	m_pSPosSphere->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_WP_00"));
	m_pPosBox->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_BD_01"));

}


STObjectBox & Player::getAtkObjectBox(void)
{
	return m_pAtkSphere->getFinalObjectBoxList()[0];
}

STObjectBox& Player::getSAtkObjectBox(void)
{
	return m_pSAtkSphere->getFinalObjectBoxList()[0];
}

STObjectBox& Player::getHitObjectBox(void)
{
	return m_pHitBox->getFinalObjectBoxList()[0];
}


void Player::returnIdle(CSkinnedMesh * a_pMesh)
{
	m_eAttackType = EAttackType::NONE;
	m_ePlayerState = EPlayerState::IDLE;
	// 루프 모션으로 수정
	a_pMesh->playAnimation(m_oAnimationList[0], true);
	m_bIsCanMove = true;
	m_bIsCanAtk = true;
	m_eDirection = EPlayerDirection::NONE;

}
