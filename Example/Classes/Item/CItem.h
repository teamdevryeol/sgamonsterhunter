#pragma once

#include "../Global/Define/KGlobalDefine.h"
#include "../Global/Utility/UI/CUIImage.h"

class CItem : public CUIImage
{
public:

	enum class EITEMTYPE
	{
		NONE = 0,
		HPPOTION,
		MEAT,
		CUREPOTION,
		FLASHBOMB,
	};

	struct STParameters
	{
		EITEMTYPE m_eItemType;
		int m_nOption;
	};

public:
	virtual void update(void) override;
	virtual void render(void) override;
	
public:

	CItem(std::string a_oImageFile);
	virtual ~CItem(void);

};

