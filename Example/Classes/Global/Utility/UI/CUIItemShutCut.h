#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CUIObject.h"

class CUIItemShutCut : public CUIObject
{
public:

	enum EITEMMENUTAG
	{
		TAG_SHUTCUTMAIN = 10,
		TAG_SHUTCUTRARROW,
		TAG_SHUTCUTLARROW,
		TAG_SHUTCUTL,
		TAG_SHUTCUTRR,
		TAG_SHUTCUTLL,
		TAG_SHUTCUTR,
	};

public:
	virtual void update(void) override;

public:



public:
	CUIItemShutCut(void);
	virtual ~CUIItemShutCut(void);
};
