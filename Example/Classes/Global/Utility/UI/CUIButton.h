#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"
#include "CUIImage.h"

class CUIButton;
class IUIButtonDelegate
{
public:
	virtual void OnClick(CUIButton* a_pSender) = 0;
};

class CUIButton : public CUIObject
{
public:
	virtual void update(void) override;
	virtual void render(void) override;

public:
	CUIButton(void);
	virtual ~CUIButton(void);

protected:
	CC_SYNTHESIZE(std::string, m_oNorImage, NorImage);
	CC_SYNTHESIZE(std::string, m_oOvrImage, OvrImage);
	CC_SYNTHESIZE(std::string, m_oSelImage, SelImage);
	CC_SYNTHESIZE(IUIButtonDelegate*, m_pDelegate, Delegate);
	
protected:
	enum EButtonState
	{
		NORMAL,
		MOUSE_OVER,
		SELECTED
	};
	CUIImage*		m_pImage;
	EButtonState	m_eButtonState;
};
