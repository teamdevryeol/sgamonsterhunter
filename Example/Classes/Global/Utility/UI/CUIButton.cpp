#include "CUIButton.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Manager/CWindowManager.h"
#include "../Render/CShader.h"

CUIButton::CUIButton(void)
	: m_oNorImage("Resources/Textures/window-close-nor.png")
	, m_oOvrImage("Resources/Textures/window-close-ovr.png")
	, m_oSelImage("Resources/Textures/window-close-sel.png")
	, m_eButtonState(EButtonState::NORMAL)
	, m_pDelegate(nullptr)
{
	m_pImage = new CUIImage(m_oNorImage);
	m_pImage->setScale(m_pImage->getImageSize());
	m_stScale = m_pImage->getScale();
	m_pImage->setLocalPosition(D3DXVECTOR2(0, 0));

	addChildObject(m_pImage);
}

CUIButton::~CUIButton(void)
{
}

void CUIButton::update(void)
{
	CUIObject::update();
	POINT pt;
	GetCursorPos(&pt);
	ScreenToClient(GET_WINDOW_HANDLE(), &pt);
	SIZE stWinSize = GET_WINDOW_SIZE();
	RECT rc;
	SetRect(&rc, m_stWorldPosition.x,
		stWinSize.cy - m_stWorldPosition.y - m_stScale.y,
		m_stWorldPosition.x + m_stScale.x,
		stWinSize.cy - m_stWorldPosition.y);
	if (PtInRect(&rc, pt))
	{
		if (GetKeyState(VK_LBUTTON) & 0x8000)
		{
			if (m_eButtonState == EButtonState::MOUSE_OVER)
			{
				m_eButtonState = EButtonState::SELECTED;
				if (m_pDelegate)
				{
					m_pDelegate->OnClick(this);
				}
			}
		}
		else
		{
			m_eButtonState = EButtonState::MOUSE_OVER;
		}
	}
	else
	{
		m_eButtonState = EButtonState::NORMAL;
	}
}

void CUIButton::render(void)
{
	if(m_eButtonState == EButtonState::NORMAL)
		m_pImage->setImageFile(m_oNorImage);
	else if (m_eButtonState == EButtonState::MOUSE_OVER)
		m_pImage->setImageFile(m_oOvrImage);
	else if (m_eButtonState == EButtonState::SELECTED)
		m_pImage->setImageFile(m_oSelImage);

	CUIObject::render();
}

