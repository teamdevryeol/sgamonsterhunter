#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CUIObject.h"

class CUIImage;

#define PROGRESSBARMIN 10

class CUIProgressBar : public CUIObject
{
public:

	enum class EProgressBarType
	{
		HPBar = 0,
		StaminaBar,
	};

public:

	void setCurrentValue(int a_nCurrentValue);

	void setScale(const D3DXVECTOR2 &a_rstScale);

public:

	virtual void update(void) override;

public:

	CUIProgressBar(EProgressBarType a_eProgressBarType, int a_nMaxValue = PROGRESSBARMIN);
	virtual ~CUIProgressBar(void);

private:

	void setupImagepath(const std::string a_oImagepath);

private:

	CC_SYNTHESIZE_READONLY(std::string, m_oImageFilepath[4], ImageFilepath);
	CC_SYNTHESIZE(D3DXCOLOR, m_stColor, Color);
	CC_SYNTHESIZE(EProgressBarType, m_eProgressBarType, ProgressBarType);
	CC_SYNTHESIZE(int, m_nMaxValue, MaxValue);
	CC_SYNTHESIZE_READONLY(int, m_nCurrentValue, CurrentValue);
	CC_SYNTHESIZE(bool, m_bIsViewEnable, IsViewEnable);

	bool m_bIsEmitEnable = true;
	
	float m_fBarLength = 0.0f;

	CUIImage *m_pStartImage;
	std::vector<CUIImage *> m_oBarImageList;
	CUIImage *m_pEndImage;
	CUIImage *m_pProgressBarImage;
};