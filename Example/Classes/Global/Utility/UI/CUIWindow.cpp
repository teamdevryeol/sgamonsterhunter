#include "CUIWindow.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Render/CShader.h"
#include "../Manager/CWindowManager.h"
#include "CUIProgressBar.h"
#include "CUIItemShutCut.h"
#include "CUIImage.h"


CUIWindow::CUIWindow(void)
	: m_oClockImage("Resources/Textures/UI/cock00_d_clock.png")
	, m_oTimeImage("Resources/Textures/UI/cock00_d_time.png")
	, m_oSharpImage("Resources/Textures/UI/cock00_d_sharp.png")
	, m_oIDImage("Resources/Textures/UI/cock00_d_ID.png")
	, m_oHPImage("Resources/Textures/UI/cock00_d_HP.png")
	, m_oStaminaImage("Resources/Textures/UI/cock00_d_itemshotcut.png")
	, m_oItemShotCutImage("Resources/Textures/UI/cock00_d_itemshotcut.png")
{
	D3D11_DEPTH_STENCIL_DESC stDepthStencilDesc = { 0 };
	stDepthStencilDesc.DepthEnable = true;
	stDepthStencilDesc.StencilEnable = true;
	GET_DEVICE()->CreateDepthStencilState(&stDepthStencilDesc, &m_pDepthStencilState);

	D3D11_DEPTH_STENCIL_DESC stDepthDisabledStencilDesc = { 0 };
	stDepthDisabledStencilDesc.DepthEnable = false;
	stDepthDisabledStencilDesc.StencilEnable = false;
	GET_DEVICE()->CreateDepthStencilState(&stDepthDisabledStencilDesc, &m_pDepthDisabledStencilState);

	D3DXVECTOR2 stWindowSize = D3DXVECTOR2(
		GET_WINDOW_SIZE().cx, GET_WINDOW_SIZE().cy);

	CUIImage* pClock = Create<CUIImage>(m_oClockImage);
	pClock->setLocalPosition(D3DXVECTOR2(40.0f, stWindowSize.y - 200.0f));
	pClock->setScale(pClock->getImageSize() * 2.0f);
	pClock->setTag(TAG_CLOCK);

	this->addChildObject(pClock);

	CUIImage* pSharp = Create<CUIImage>(m_oSharpImage);
	pSharp->setLocalPosition(D3DXVECTOR2(180.0f, stWindowSize.y - 200.0f));
	pSharp->setScale(pSharp->getImageSize() * 2.0f);
	pSharp->setTag(TAG_SHARP);

	this->addChildObject(pSharp);

	CUIImage* pID = Create<CUIImage>(m_oIDImage);
	pID->setLocalPosition(D3DXVECTOR2(40.0f, stWindowSize.y - 280.0f));
	pID->setScale(pID->getImageSize());
	pID->setTag(TAG_ID);

	this->addChildObject(pID);

	CUIImage* pTime = Create<CUIImage>(m_oTimeImage);
	pTime->setScale(pTime->getImageSize() * 2.0f);
	pTime->setLocalPosition((pClock->getScale() / 2.0f) -
		(pTime->getScale() / 2.0f));
	pTime->setTag(TAG_TIME);

	pClock->addChildObject(pTime);

	auto pHPBar = Create<CUIProgressBar>(CUIProgressBar::EProgressBarType::HPBar, 100);
	pHPBar->setLocalPosition(D3DXVECTOR2(160.0f, stWindowSize.y - 130.0f));
	pHPBar->setScale(D3DXVECTOR2(2.0f, 2.0f));
	pHPBar->setTag(TAG_HP);

	this->addChildObject(pHPBar);

	m_oProgressBarList.push_back(pHPBar);

	auto pStamina = Create<CUIProgressBar>(CUIProgressBar::EProgressBarType::StaminaBar, 100);
	pStamina->setLocalPosition(D3DXVECTOR2(184.0f, stWindowSize.y - 154.0f));
	pStamina->setScale(D3DXVECTOR2(2.0f, 2.0f));
	pStamina->setTag(TAG_STAMINA);

	this->addChildObject(pStamina);

	m_oProgressBarList.push_back(pStamina);

	m_pItemShutCut = Create<CUIItemShutCut>();
	m_pItemShutCut->setLocalPosition(D3DXVECTOR2(stWindowSize.x - 300.0f, 40.0f));
	m_pItemShutCut->setTag(TAG_ITEMSHOTCUT);

	this->addChildObject(m_pItemShutCut);
}

CUIWindow::~CUIWindow(void)
{
	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthDisabledStencilState);
}

void CUIWindow::render(void)
{
	GET_CONTEXT()->OMSetDepthStencilState(m_pDepthDisabledStencilState, 0xFF);

	auto pBlendState = GET_BLEND_STATE(KEY_ALPHA_BLEND_ENABLE_STATE);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);

	CUIObject::render();
	
	GET_CONTEXT()->OMSetDepthStencilState(m_pDepthStencilState, 0xFF);

	pBlendState = GET_BLEND_STATE(KEY_ALPHA_BLEND_DISABLE_STATE);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);

}

void CUIWindow::openItemWindow(void)
{
	CUIImage* pOpenUI = Create<CUIImage>("Resources/Textures/UI/a_hou_back1.png");
	pOpenUI->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));
	pOpenUI->setScale(pOpenUI->getImageSize());
	pOpenUI->setTag(TAG_OPENWINDOW);

	this->addChildObject(pOpenUI);
}