#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"
#include "../Base/CUIObject.h"
#include "CUIButton.h"

class CUIProgressBar;
class CUIItemShutCut;

class CUIWindow 
	: public CUIObject
{
public:

	enum EMENUTAG
	{
		TAG_NONE = 0,
		TAG_CLOCK,
		TAG_TIME,
		TAG_HP,
		TAG_STAMINA,
		TAG_SHARP,
		TAG_ID,
		TAG_ITEMSHOTCUT,
		TAG_OPENWINDOW,
	};

public:

	virtual void render(void) override;

	void openItemWindow(void);

public:
	// is - a
	CUIWindow(void);
	virtual ~CUIWindow(void);

protected:
	CC_SYNTHESIZE(std::string, m_oClockImage, ClockImage);
	CC_SYNTHESIZE(std::string, m_oTimeImage, TimeImage);
	CC_SYNTHESIZE(std::string, m_oSharpImage, SharpImage);
	CC_SYNTHESIZE(std::string, m_oIDImage, IDImage);
	CC_SYNTHESIZE(std::string, m_oHPImage, HPImage);
	CC_SYNTHESIZE(std::string, m_oStaminaImage, StaminaImage);
	CC_SYNTHESIZE(std::string, m_oItemShotCutImage, ItemShotCutImage);
	CC_SYNTHESIZE_READONLY(CUIItemShutCut*, m_pItemShutCut, ItemShutCut);
	CC_SYNTHESIZE_READONLY(std::vector<CUIProgressBar *>, m_oProgressBarList, ProgressBarList);

private:

	ID3D11DepthStencilState* m_pDepthStencilState;
	ID3D11DepthStencilState* m_pDepthDisabledStencilState;
};
