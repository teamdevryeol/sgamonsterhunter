#include "CUIItemShutCut.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CWindowManager.h"
#include "CUIImage.h"


CUIItemShutCut::CUIItemShutCut(void)
	:
	CUIObject()
{
	CUIImage* pItemShotCut = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut.png");
	pItemShotCut->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));
	pItemShotCut->setScale(pItemShotCut->getImageSize() * 2.0f);
	pItemShotCut->setTag(TAG_SHUTCUTMAIN);

	this->addChildObject(pItemShotCut);

	CUIImage* pItemShotCutArrowL = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut_Arrow.png");
	pItemShotCutArrowL->setLocalPosition(D3DXVECTOR2(-25.0f, 63.0f));
	pItemShotCutArrowL->setScale(pItemShotCutArrowL->getImageSize() * 2.0f);
	pItemShotCutArrowL->setTag(TAG_SHUTCUTLARROW);

	pItemShotCut->addChildObject(pItemShotCutArrowL);

	CUIImage* pItemShotCutArrowR = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut_Arrow.png");
	pItemShotCutArrowR->setAngle(180.0f);
	pItemShotCutArrowR->setLocalPosition(D3DXVECTOR2(125.0f, 63.0f));
	pItemShotCutArrowR->setScale(pItemShotCutArrowR->getImageSize() * 2.0f);
	pItemShotCutArrowR->setTag(TAG_SHUTCUTRARROW);

	pItemShotCut->addChildObject(pItemShotCutArrowR);
}

CUIItemShutCut::~CUIItemShutCut(void)
{
}

void CUIItemShutCut::update(void)
{
	CUIObject::update();
}
