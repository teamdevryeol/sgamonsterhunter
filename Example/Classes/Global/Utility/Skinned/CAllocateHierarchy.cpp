#include "CAllocateHierarchy.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CResourceManager.h"
#include "../Render/CMesh.h"

CAllocateHierarchy::CAllocateHierarchy(void)
{
	// Do Nothing
}

CAllocateHierarchy::CAllocateHierarchy(const STParameters & a_rstParameters)
:
m_stParameters(a_rstParameters)
{
	// Do Nothing
}

HRESULT CAllocateHierarchy::CreateFrame(LPCSTR a_pszName, LPD3DXFRAME * a_pstOutFrame)
{
	auto pstBone = new STBone();
	ZeroMemory(pstBone, sizeof(STBone));

	if (a_pszName != nullptr) {
		pstBone->Name = (char *)malloc(sizeof(char) * (strlen(a_pszName) + 1));
		strcpy(pstBone->Name, a_pszName);
	}

	D3DXMatrixIdentity(&pstBone->m_stCombineMatrix);
	D3DXMatrixIdentity(&pstBone->TransformationMatrix);

	*a_pstOutFrame = pstBone;
	return S_OK;
}

HRESULT CAllocateHierarchy::CreateMeshContainer(LPCSTR a_pszName, const D3DXMESHDATA * a_pstMeshData, const D3DXMATERIAL * a_pstXMaterials, const D3DXEFFECTINSTANCE * a_pstEffectInstances, DWORD a_nNumMaterials, const DWORD * a_pnAdjacency, LPD3DXSKININFO a_pSkinInfo, LPD3DXMESHCONTAINER * a_pstOutMeshContainer)
{
	auto pstMeshContainer = new STMeshContainer();
	ZeroMemory(pstMeshContainer, sizeof(D3DXMESHCONTAINER));

	// 일반 메시 일 경우
	if (a_pSkinInfo != nullptr && 
		a_pstMeshData->Type == D3DXMESHTYPE_MESH) 
	{
		if (a_pszName != nullptr) {
			pstMeshContainer->Name = (char *)malloc(sizeof(char) * (strlen(a_pszName) + 1));
			strcpy(pstMeshContainer->Name, a_pszName);
		}

		// 메시 정보를 설정한다
		// {
		pstMeshContainer->MeshData = *a_pstMeshData;
		pstMeshContainer->MeshData.pMesh->AddRef();

		pstMeshContainer->pAdjacency = (DWORD *)malloc(sizeof(DWORD) * (a_pstMeshData->pMesh->GetNumFaces() * 3));
		CopyMemory(pstMeshContainer->pAdjacency, a_pnAdjacency, sizeof(DWORD) * (a_pstMeshData->pMesh->GetNumFaces() * 3));
		// }

		// 재질 정보를 설정한다
		// {
		pstMeshContainer->NumMaterials = a_nNumMaterials;

		pstMeshContainer->pMaterials = (LPD3DXMATERIAL)malloc(sizeof(D3DXMATERIAL) * a_nNumMaterials);
		CopyMemory(pstMeshContainer->pMaterials, a_pstXMaterials, sizeof(D3DXMATERIAL) * a_nNumMaterials);

		for (int i = 0; i < a_nNumMaterials; ++i) {
			ID3D11ShaderResourceView *pDiffuseMap = nullptr;

			if (a_pstXMaterials[i].pTextureFilename != nullptr) {
				std::string oFilepath = GetFormatString("%s%s", m_stParameters.m_oBasePath.c_str(),
					a_pstXMaterials[i].pTextureFilename);

				pDiffuseMap = GET_SHADER_RESOURCE_VIEW(oFilepath);
			}

			pstMeshContainer->m_oDiffuseMapList.push_back(pDiffuseMap);
		}
		// }

		// 스킨 정보를 설정한다
		// {
		pstMeshContainer->pSkinInfo = a_pSkinInfo;
		pstMeshContainer->pSkinInfo->AddRef();

		for (int i = 0; i < a_pSkinInfo->GetNumBones(); ++i) {
			auto pstBoneMatrix = a_pSkinInfo->GetBoneOffsetMatrix(i);
			pstMeshContainer->m_oBoneMatrixList.push_back(*pstBoneMatrix);
		}
		// }

		m_nNumMeshContainers += 1;
		pstMeshContainer->m_pSkinnedMesh = m_stParameters.m_oSkinnedMeshCreator(pstMeshContainer, m_nNumMeshContainers);
	}

	*a_pstOutMeshContainer = pstMeshContainer;
	return S_OK;
}

HRESULT CAllocateHierarchy::DestroyFrame(LPD3DXFRAME a_pstFrame)
{
	auto pstBone = (STBone *)a_pstFrame;

	SAFE_FREE(pstBone->Name);
	SAFE_DELETE(pstBone);

	return S_OK;
}

HRESULT CAllocateHierarchy::DestroyMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer)
{
	auto pstMeshContainer = (STMeshContainer *)a_pstMeshContainer;

	SAFE_FREE(pstMeshContainer->Name);
	SAFE_FREE(pstMeshContainer->pAdjacency);
	SAFE_FREE(pstMeshContainer->pMaterials);

	SAFE_RELEASE(pstMeshContainer->pSkinInfo);
	SAFE_RELEASE(pstMeshContainer->MeshData.pMesh);

	SAFE_DELETE(pstMeshContainer);
	return S_OK;
}
