#include "CGrid.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"

CGrid::CGrid(int a_nSize)
:
CRenderObject(CRenderObject::STParameters{
	sizeof(STColorVertex),
	DXGI_FORMAT_UNKNOWN,

	KEY_COLOR_SHADER,
	GetFormatString("GridVertexBuffer%d", (a_nSize / 5) * 5),
	"",
	KEY_TRANSFORM_BUFFER,
	KEY_COLOR_RENDER_BUFFER,
	KEY_COLOR_INPUT_LAYOUT
}),

m_nSize((a_nSize / 5) * 5)
{
	m_pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);
	
	if (m_pVertexBuffer == nullptr) {
		m_pVertexBuffer = this->createVertexBuffer();

		GET_RESOURCE_MANAGER()->addBuffer(m_stParameters.m_oVertexBufferKey,
			m_pVertexBuffer);
	}
}

void CGrid::preRender(void)
{
	CRenderObject::preRender();
	GET_CONTEXT()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	// 렌더 정보를 설정한다
	// {
	auto pRenderBuffer = GET_BUFFER(m_stParameters.m_oRenderBufferKey);
	
	if (pRenderBuffer != nullptr) {
		auto oRenderBufferPtr = GetBufferPointer<STColorRender>(pRenderBuffer);
		auto pstRender = oRenderBufferPtr.get();

		pstRender->m_stColor = m_stColor;
	}
	// }
}

void CGrid::doRender(void)
{
	CRenderObject::doRender();
	GET_CONTEXT()->Draw((m_nSize + 1) * 4, 0);
}

ID3D11Buffer * CGrid::createVertexBuffer(void)
{
	// 정점 버퍼를 생성한다
	auto pVertexBuffer = CreateBuffer(sizeof(STColorVertex) * (m_nSize + 1) * 4,
		D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	// 정점 정보를 설정한다
	// {
	auto oVertexBufferPtr = GetBufferPointer<STColorVertex>(pVertexBuffer);
	auto pstVertices = oVertexBufferPtr.get();

	D3DXVECTOR3 stBasePosition = {
		m_nSize / -2.0f,
		0.0f,
		m_nSize / 2.0f
	};

	for (int i = 0; i < m_nSize + 1; ++i) {
		int nIndex = i * 4;

		D3DXCOLOR stColor = (i % 5 == 0) ? D3DXCOLOR(0.25f, 0.25f, 0.25f, 1.0f)
			: D3DXCOLOR(0.25f, 0.25f, 0.25f, 1.0f);

		// 가로선
		// {
		pstVertices[nIndex + 0].m_stPosition = stBasePosition + 
			D3DXVECTOR3(0.0f, 0.0f, -i);

		pstVertices[nIndex + 1].m_stPosition = stBasePosition + 
			D3DXVECTOR3(m_nSize, 0.0f, -i);
		// }

		// 세로선
		// {
		pstVertices[nIndex + 2].m_stPosition = stBasePosition +
			D3DXVECTOR3(i, 0.0f, 0.0f);

		pstVertices[nIndex + 3].m_stPosition = stBasePosition +
			D3DXVECTOR3(i, 0.0f, -m_nSize);
		// }

		// 색상
		for (int j = 0; j < 4; ++j) {
			pstVertices[nIndex + j].m_stColor = stColor;
		}
	}
	// }

	return pVertexBuffer;
}
