#include "CLine.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"

CLine::CLine(const D3DXVECTOR3& stP1, const D3DXVECTOR3& stP2)
	:
	CRenderObject(CRenderObject::STParameters{
	sizeof(STColorVertex),
	DXGI_FORMAT_UNKNOWN,

	KEY_COLOR_SHADER,
	ToString(stP1) + ToString(stP2),
	"",
	KEY_TRANSFORM_BUFFER,
	KEY_COLOR_RENDER_BUFFER,
	KEY_COLOR_INPUT_LAYOUT
})
{
	m_pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);

	if (m_pVertexBuffer == nullptr) {
		m_pVertexBuffer = this->createVertexBuffer(stP1, stP2);

		GET_RESOURCE_MANAGER()->addBuffer(m_stParameters.m_oVertexBufferKey,
			m_pVertexBuffer);
	}
}

CLine::~CLine()
{
}

void CLine::update(const D3DXVECTOR3 & stP1, const D3DXVECTOR3 & stP2)
{
	D3D11_MAPPED_SUBRESOURCE stSubResource;
	ZeroMemory(&stSubResource, sizeof(stSubResource));

	GET_CONTEXT()->Map(m_pVertexBuffer,
		0, D3D11_MAP_WRITE_DISCARD, 0, &stSubResource);

	auto pstVertices = (STColorVertex *)stSubResource.pData;

	pstVertices[0].m_stPosition = stP1;
	pstVertices[0].m_stPosition.y += 0.01f;
	pstVertices[1].m_stPosition = stP2;
	pstVertices[1].m_stPosition.y += 0.01f;
	pstVertices[0].m_stColor = D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f);
	pstVertices[1].m_stColor = D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f);

	GET_CONTEXT()->Unmap(m_pVertexBuffer, 0);
}

void CLine::preRender(void)
{
	CRenderObject::preRender();
	GET_CONTEXT()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	// 렌더 정보를 설정한다
	// {
	auto pRenderBuffer = GET_BUFFER(m_stParameters.m_oRenderBufferKey);

	if (pRenderBuffer != nullptr) {
		auto oRenderBufferPtr = GetBufferPointer<STColorRender>(pRenderBuffer);
		auto pstRender = oRenderBufferPtr.get();

		pstRender->m_stColor = m_stColor;
	}
	// }
}

void CLine::doRender(void)
{
	CRenderObject::doRender();
	GET_CONTEXT()->Draw(2, 0);
}

ID3D11Buffer * CLine::createVertexBuffer(const D3DXVECTOR3& stP1, const D3DXVECTOR3& stP2)
{
	// 정점 버퍼를 생성한다
	auto pVertexBuffer = CreateBuffer(sizeof(STColorVertex) * 2,
		D3D11_BIND_VERTEX_BUFFER,
		D3D11_CPU_ACCESS_WRITE, 
		D3D11_USAGE_DYNAMIC);

	// 정점 정보를 설정한다
	// {
	auto oVertexBufferPtr = GetBufferPointer<STColorVertex>(pVertexBuffer);
	auto pstVertices = oVertexBufferPtr.get();
	pstVertices[0].m_stPosition = stP1;
	pstVertices[0].m_stPosition.y += 0.01f;
	pstVertices[1].m_stPosition = stP2;
	pstVertices[1].m_stPosition.y += 0.01f;
	pstVertices[0].m_stColor = D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f);
	pstVertices[1].m_stColor = D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f);

	return pVertexBuffer;
}
