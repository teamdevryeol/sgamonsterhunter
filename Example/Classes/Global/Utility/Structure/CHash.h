#pragma once

#include "../../Define/KGlobalDefine.h"
#include "CArrayList.h"
#include "CLinkedList.h"

//! 해시
template <typename K, typename V>
class CHash
{
public:

	enum
	{
		DEFAULT_TABLE_SIZE = 30
	};

public:			// public 함수

	//! 값을 추가한다
	void addValue(K a_tKey, V a_tValue);

	//! 값을 제거한다
	void removeValue(K a_tKey, V a_tValue);

	//! 해시 테이블을 순회한다
	void enumerateHashTable(const std::function<void (int, const CLinkedList<V> &)> &a_rCallback);

public:			// 생성자, 소멸자

	//! 생성자
	CHash(void);

	//! 소멸자
	virtual ~CHash(void);

private:			// private 함수

	//! 해시 값을 생성한다
	int createHashValue(K a_tKey);

private:			// private 변수

	CArrayList<CLinkedList<V>> m_oValueTable;
};

#include "CHash.inl"
