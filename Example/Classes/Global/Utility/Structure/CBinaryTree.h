#pragma once

#include "../../Define/KGlobalDefine.h"

//! 이진 트리
template <typename K, typename V>
class CBinaryTree
{
public:

	//! 순회 타입
	enum EEnumerateType
	{
		PRE_ORDER,
		IN_ORDER,
		POST_ORDER,
		LEVEL_ORDER,
		NONE
	};

	//! 노드
	struct STNode
	{
		K m_tKey;
		V m_tValue;

		STNode *m_pstLChildNode;
		STNode *m_pstRChildNode;
	};

public:			// operator

	//! operator =
	void operator=(const CBinaryTree &a_rBinaryTree);

public:			// public 함수

	//! 데이터를 제거한다
	void clear(void);

	//! 값을 추가한다
	void addValue(K a_tKey, V a_tValue);

	//! 값을 제거한다
	void removeValue(K a_tKey);

	//! 트리를 순회한다
	void enumerateBinaryTree(EEnumerateType a_eEnumerateType,
		const std::function<void(STNode *)> &a_rCallback);

public:			// 생성자, 소멸자

	//! 생성자
	CBinaryTree(void);

	//! 복사 생성자
	CBinaryTree(const CBinaryTree &a_rBinaryTree);

	//! 소멸자
	virtual ~CBinaryTree(void);

private:			// private 함수

	//! 노드를 생성한다
	STNode * createNode(K a_tKey, V a_tValue);

	//! 전위 순회를 수행한다
	void enumerateByPreOrder(STNode *a_pstNode,
		const std::function<void(STNode *)> &a_rCallback);

	//! 중위 순회를 수행한다
	void enumerateByInOrder(STNode *a_pstNode,
		const std::function<void(STNode *)> &a_rCallback);

	//! 후위 순회를 수행한다
	void enumerateByPostOrder(STNode *a_pstNode,
		const std::function<void(STNode *)> &a_rCallback);

	//! 레벨 순회를 수행한다
	void enumerateByLevelOrder(STNode *a_pstNode,
		const std::function<void(STNode *)> &a_rCallback);

private:			// private 변수

	STNode *m_pstRootNode = nullptr;
};

#include "CBinaryTree.inl"
