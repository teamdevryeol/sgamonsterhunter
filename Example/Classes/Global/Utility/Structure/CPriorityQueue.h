#pragma once

#include "../../Define/KGlobalDefine.h"
#include "CArrayList.h"

//! 우선 순위 큐
template <typename T>
class CPriorityQueue
{
public:			// public 함수

	//! 데이터를 추가한다
	void enqueueValue(T a_tValue);

	//! 데이터를 제거한다
	T dequeueValue(void);

	//! 비어있는 유무를 검사한다
	bool isEmpty(void);

public:			// 생성자, 소멸자

	//! 생성자
	CPriorityQueue(void);

	//! 소멸자
	virtual ~CPriorityQueue(void);

private:			// private 함수

	//! 힙을 재정렬한다
	void reheapUp(void);

	//! 힙을 재정렬한다
	void reheapDown(void);

private:			// private 변수

	CArrayList<T> m_oValueList;
};

#include "CPriorityQueue.inl"
