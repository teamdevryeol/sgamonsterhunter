#include "CPriorityQueue.h"

template<typename T>
inline CPriorityQueue<T>::CPriorityQueue(void)
{
	// Do Nothing
}

template<typename T>
inline CPriorityQueue<T>::~CPriorityQueue(void)
{
	// Do Nothing
}

template<typename T>
inline void CPriorityQueue<T>::enqueueValue(T a_tValue)
{
	m_oValueList.addValue(a_tValue);
	this->reheapUp();
}

template<typename T>
inline T CPriorityQueue<T>::dequeueValue(void)
{
	assert(m_oValueList.getCount() >= 1);
	
	T tValue = m_oValueList[0];
	int nLastIndex = m_oValueList.getCount() - 1;

	m_oValueList.replaceValue(0, m_oValueList[nLastIndex]);
	m_oValueList.removeValueByIndex(nLastIndex);

	this->reheapDown();
	return tValue;
}

template<typename T>
inline bool CPriorityQueue<T>::isEmpty(void)
{
	return m_oValueList.getCount() <= 0;
}

template<typename T>
inline void CPriorityQueue<T>::reheapUp(void)
{
	int nChildIndex = m_oValueList.getCount() - 1;

	while (nChildIndex >= 1) {
		int nParentIndex = (nChildIndex - 1) / 2;

		if (m_oValueList[nParentIndex] <= m_oValueList[nChildIndex]) {
			break;
		}

		std::swap(m_oValueList[nParentIndex], m_oValueList[nChildIndex]);
		nChildIndex = nParentIndex;
	}
}

template<typename T>
inline void CPriorityQueue<T>::reheapDown(void)
{
	int nParentIndex = 0;

	while (nParentIndex < m_oValueList.getCount() / 2) {
		int nCompareIndex = (nParentIndex * 2) + 1;

		// 오른쪽 자식이 있을 경우
		if (nCompareIndex + 1 < m_oValueList.getCount()) {
			int nRChildIndex = nCompareIndex + 1;

			T tLChildValue = m_oValueList[nCompareIndex];
			T tRChildValue = m_oValueList[nRChildIndex];

			nCompareIndex = (tLChildValue <= tRChildValue) ? nCompareIndex : nRChildIndex;
		}

		// 부모의 값이 작을 경우
		if (m_oValueList[nParentIndex] <= m_oValueList[nCompareIndex]) {
			break;
		}

		std::swap(m_oValueList[nParentIndex], m_oValueList[nCompareIndex]);
		nParentIndex = nCompareIndex;
	}
}
