#include "CHash.h"

template<typename K, typename V>
inline CHash<K, V>::CHash(void)
:
m_oValueTable(DEFAULT_TABLE_SIZE)
{
	m_oValueTable.reserve(DEFAULT_TABLE_SIZE);
}

template<typename K, typename V>
inline CHash<K, V>::~CHash(void)
{
	// Do Nothing
}

template<typename K, typename V>
inline void CHash<K, V>::addValue(K a_tKey, V a_tValue)
{
	int nHashValue = this->createHashValue(a_tKey);
	m_oValueTable[nHashValue].addValue(a_tValue);
}

template<typename K, typename V>
inline void CHash<K, V>::removeValue(K a_tKey, V a_tValue)
{
	int nHashValue = this->createHashValue(a_tKey);

	if (nHashValue < m_oValueTable.getCount()) {
		m_oValueTable[nHashValue]->removeValue(a_tValue);
	}
}

template<typename K, typename V>
inline void CHash<K, V>::enumerateHashTable(const std::function<void(int, const CLinkedList<V>&)>& a_rCallback)
{
	for (int i = 0; i < m_oValueTable.getCount(); ++i) {
		a_rCallback(i, m_oValueTable[i]);
	}
}

template<typename K, typename V>
inline int CHash<K, V>::createHashValue(K a_tKey)
{
	std::hash<K> oHash;
	return oHash(a_tKey) % m_oValueTable.getSize();
}
