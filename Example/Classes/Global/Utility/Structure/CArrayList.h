#pragma once

#include "../../Define/KGlobalDefine.h"

//! 배열 리스트
template <typename T>
class CArrayList
{
public:
	
	enum
	{
		DEFAULT_ARRAY_SIZE = 5
	};

public:			// operator

	//! operator =
	void operator=(const CArrayList &a_rArrayList);

	//! operator []
	T & operator[](int a_nIndex);

	//! operator []
	T operator[](int a_nIndex) const;

public:			// getter

	//! 크기를 반환한다
	int getSize(void) const;

	//! 개수를 반환한다
	int getCount(void) const;

	//! 값을 반환한다
	T getValue(int a_nIndex) const;

public:			// public 함수

	//! 사이즈를 변경한다
	void resize(int a_nSize);

	//! 공간을 예약한다
	void reserve(int a_nSize);

	//! 값을 추가한다
	void addValue(T a_tValue);

	//! 값을 추가한다
	void insertValue(int a_nIndex, T a_tValue);

	//! 값을 대치한다
	void replaceValue(int a_nIndex, T a_tValue);

	//! 값을 제거한다
	void removeValue(T a_tValue);

	//! 값을 제거한다
	void removeValueByIndex(int a_nIndex);

public:			// 생성자, 소멸자

	//! 생성자
	CArrayList(int a_nSize = DEFAULT_ARRAY_SIZE);

	/*
	기본적으로 C++ 컴파일러에 의해서 구현되는 복사 생성자는 내부적으로
	얕은 복사를 수행하기 때문에 클래스의 소멸자에서 특정 리소스를 정리
	할 경우 잘못된 댕글링 포인터를 만들어 낼 수 있다.
	*/
	//! 복사 생성자
	CArrayList(const CArrayList &a_rArrayList);

	/*
	C++ 클래스에 가상 소멸자가 선언되어있을 경우 해당 클래스를 상속받는
	자식 객체를 생성했을 때 안전하게 자식 객체의 소멸자를 호출해주는 역할을
	한다.
	*/
	//! 소멸자
	virtual ~CArrayList(void);

private:			// private 함수

	//! 유효한 인덱스 여부를 검사한다
	bool isValidIndex(int a_nIndex);

private:			// private 변수

	int m_nSize = 0;
	int m_nCount = 0;

	T *m_ptValues = nullptr;
};

#include "CArrayList.inl"
