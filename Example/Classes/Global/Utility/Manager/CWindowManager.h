#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IWindowMessageHandler.h"

//! 윈도우 관리자
class CWindowManager : public IWindowMessageHandler
{
public:			// 인터페이스 구현

	//! 윈도우 메세지를 처리한다
	virtual LRESULT handleWindowMessage(HWND a_hWindow,
		UINT a_nMessage, WPARAM a_wParam, LPARAM a_lParam) override;

public:			// getter , setter

	//! 윈도우 크기를 반환한다
	SIZE getWindowSize(void);

	//! 윈도우 핸들을 반환한다
	HWND getWindowHandle(void);

	//! 인스턴스 핸들을 반환한다
	HINSTANCE getInstanceHandle(void);


public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CWindowManager);

	//! 초기화
	virtual void init(HINSTANCE a_hInstance,
		const SIZE &a_rstWindowSize, 
		IWindowMessageHandler *a_pMessageHandler);

private:			// private 함수

	//! 윈도우를 생성한다
	HWND createWindow(void);


private:			// private 변수
	SIZE m_stWindowSize;
	WNDCLASS m_stWindowClass;

	HWND m_hWindow = nullptr;
	HINSTANCE m_hInstance = nullptr;
	IWindowMessageHandler *m_pMessageHandler = nullptr;
};
