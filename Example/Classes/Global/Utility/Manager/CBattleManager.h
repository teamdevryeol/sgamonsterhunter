#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../../../Player/Player.h"

class Player;
class Monster;
class CStaticMesh;

class CBattleManager
{
public:

	virtual void init(Player* a_pMesh1, Monster* a_pMesh2);
	virtual void update();
	virtual void render();

public:
	bool getCollision();

private:
	void createMesh();
	void meshUpdate();
	void updatePosition();
	bool IntersectPMBoundingBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh);

	void meshRender();

	void setDebugMode();
	void player_MonsterAttack();
	void pmBoudningBoxCollision();
	
private:

	Player* m_pPlayer;
	Monster* m_pMonster;

	bool m_bIsDebug = true;
	bool m_bIsCollision = false;
	
	CStaticMesh * m_pPAtkMesh = nullptr;
	CStaticMesh * m_pPSAtkMesh = nullptr;
	CStaticMesh * m_pPHitMesh = nullptr;

	CStaticMesh * m_pMHeadMesh = nullptr;
	CStaticMesh * m_pMTailMesh = nullptr;
	CStaticMesh * m_pMBodyMesh = nullptr;
	CStaticMesh * m_pMForwardMesh = nullptr;
	CStaticMesh * m_pMBackMesh = nullptr;

	Player::EAttackType m_ePrevAtkType = Player::EAttackType::NONE;
	D3DXVECTOR3 m_stPrevPos;
	std::vector<D3DXVECTOR3> m_oPrevPos;
};