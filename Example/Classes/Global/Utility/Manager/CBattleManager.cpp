#include "CBattleManager.h"
#include "../../../Monster/Monster.h"
#include "../../Function/GlobalFunction.h"
#include "../../Utility/Object/CStaticMesh.h"
#include "../../Utility/Manager/CInputManager.h"

void CBattleManager::init(Player* a_pPlayerMesh, Monster* a_pMonsterMesh)
{
	m_pPlayer = a_pPlayerMesh;
	m_pMonster = a_pMonsterMesh;

	m_pPlayer->setDamage(5.0f);
	this->createMesh();
}

void CBattleManager::update()
{
	this->meshUpdate();
	this->updatePosition();
	this->setDebugMode();
	this->player_MonsterAttack();
	this->pmBoudningBoxCollision();
}

void CBattleManager::render()
{
	if (m_bIsDebug)
	{
		this->meshRender();
	}
}

bool CBattleManager::getCollision()
{
	return m_bIsCollision;
}

void CBattleManager::createMesh()
{
	m_pPAtkMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	m_pPAtkMesh->setScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	m_pPSAtkMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pPSAtkMesh->setScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	m_pPHitMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pPHitMesh->setScale(D3DXVECTOR3(1.0f, 2.5f, 1.5f));

	m_pMHeadMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMHeadMesh->setScale(D3DXVECTOR3(3.0f, 3.0f, 3.0f));

	m_pMTailMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMTailMesh->setScale(D3DXVECTOR3(3.0f, 3.0f, 3.0f));

	m_pMBodyMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMBodyMesh->setScale(D3DXVECTOR3(7.0f, 7.0f, 7.0f));

	m_pMForwardMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMForwardMesh->setScale(D3DXVECTOR3(5.0f, 5.0f, 5.0f));

	m_pMBackMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMBackMesh->setScale(D3DXVECTOR3(5.0f, 5.0f, 5.0f));

}

void CBattleManager::meshUpdate()
{
	m_pMTailMesh->update();
	m_pMHeadMesh->update();
	m_pMBodyMesh->update();
	m_pPAtkMesh->update();
	m_pPSAtkMesh->update();
	m_pPHitMesh->update();
}

void CBattleManager::meshRender()
{
	//몬스터 박스
	m_pMHeadMesh->render();
	m_pMTailMesh->render();
	m_pMBodyMesh->render();
	m_pMForwardMesh->render();
	m_pMBackMesh->render();

	m_pPAtkMesh->render();
	m_pPSAtkMesh->render();
	m_pPHitMesh->render();
}

void CBattleManager::setDebugMode()
{
	if (IS_KEY_PRESSED(DIK_F1))
	{
		if (m_bIsDebug)
		{
			m_bIsDebug = false;
		}
		else
		{
			m_bIsDebug = true;
		}
	}
}

void CBattleManager::updatePosition()
{
	// 몬스터 바디 위치
	auto pBodyPos = m_pMonster->getBodyObjectBox().m_stCenter;
	m_pMHeadMesh->setPosition(pBodyPos);

	// 몬스터 꼬리 위치
	auto pTailPos = m_pMonster->getTailObjectBox().m_stCenter;
	m_pMTailMesh->setPosition(pTailPos);

	// 몬스터 힙 위치
	auto pHipPos = m_pMonster->getHipObjectBox().m_stCenter;
	m_pMBodyMesh->setPosition(pHipPos);

	// 플레이어 검 위치
	auto pAttackPos = m_pPlayer->getAtkObjectBox().m_stCenter;
	m_pPAtkMesh->setPosition(pAttackPos);

	// 플레이어 방패 위치
	auto pShieldPos = m_pPlayer->getSAtkObjectBox().m_stCenter;
	m_pPSAtkMesh->setPosition(pShieldPos);

	// 플레이어 히트 박스 위치
	auto pHitPos = m_pPlayer->getHitObjectBox().m_stCenter;
	m_pPHitMesh->setPosition(pHitPos);
}

bool CBattleManager::IntersectPMBoundingBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh)
{
	if (IsIntersectObjectBox(a_pPMesh->getFinalObjectBoxList()[0], a_pMMesh->getFinalObjectBoxList()[0]))
	{
		return true;
	}

	return false;
}

void CBattleManager::player_MonsterAttack()
{
	if (m_pPlayer->getState() == Player::EPlayerState::ATTACK)
	{
		auto pAType = m_pPlayer->getAttack();
		if (m_ePrevAtkType != pAType)
		{
			if (this->IntersectPMBoundingBox(m_pPAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMBoundingBox(m_pPAtkMesh, m_pMBodyMesh) ||
				this->IntersectPMBoundingBox(m_pPAtkMesh, m_pMTailMesh) ||
				this->IntersectPMBoundingBox(m_pPSAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMBoundingBox(m_pPSAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMBoundingBox(m_pPSAtkMesh, m_pMHeadMesh))
			{
				m_ePrevAtkType = pAType;
				printf("%.2f\n", m_pMonster->getHP());
				//1.2에 플레이어 공격력
				m_pMonster->setHP(m_pMonster->getHP() - m_pPlayer->getDamage());
				if (this->IntersectPMBoundingBox(m_pPAtkMesh, m_pMTailMesh))
				{
					m_pMonster->setTailHP(m_pMonster->getTailHP() - m_pPlayer->getDamage());
				}

			}
		}
	}
	else
	{
		m_ePrevAtkType = Player::EAttackType::NONE;
	}
}

void CBattleManager::pmBoudningBoxCollision()
{
	//auto stCurrPos = m_pPlayer->getPosition();
	if(this->IntersectPMBoundingBox(m_pPHitMesh, m_pMBodyMesh) || this->IntersectPMBoundingBox(m_pPHitMesh, m_pMTailMesh) || this->IntersectPMBoundingBox(m_pPHitMesh, m_pMHeadMesh))
	{
		m_bIsCollision = true;
		m_pPlayer->setMoveSpd(0.0f);
		m_pPlayer->setJumpSpd(0.0f);
		//auto gapPos = m_stPrevPos - stCurrPos;
		if (m_oPrevPos.size() != 0)
		{
			m_pPlayer->setPosition(m_oPrevPos[m_oPrevPos.size() - 1]);
			m_pPlayer->setPlayerControl(Player::EPlayerControl::FORCE);
			m_oPrevPos.pop_back();
		}
	}
	else
	{
		m_bIsCollision = false;
		if(m_pPlayer->getState() == Player::EPlayerState::MOVE || m_pPlayer->getState() == Player::EPlayerState::ATTACK)
		{
			m_oPrevPos.push_back(m_pPlayer->getPosition());
			if(m_oPrevPos.size() == 501)
			{
				m_oPrevPos.erase(m_oPrevPos.begin());		// 추후 최적화 해야할것
			}
		
		}
		m_pPlayer->setPlayerControl(Player::EPlayerControl::NONE);
		m_pPlayer->setMoveSpd(5.0f);
		m_pPlayer->setJumpSpd(5.0f);
	}
}
