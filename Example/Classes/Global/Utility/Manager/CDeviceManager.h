#pragma once

#include "../../Define/KGlobalDefine.h"

//! 디바이스 관리자
class CDeviceManager
{
public:			// getter

	//! 다이렉트 3D 를 반환한다
	LPDIRECT3D9 getDirect3D(void);

	//! 9 버전 디바이스를 반환한다
	LPDIRECT3DDEVICE9 get9VersionDevice(void);

	//! 팩토리를 반환한다
	IDXGIFactory * getFactory(void);

	//! 스왑 체인을 반환한다
	IDXGISwapChain * getSwapChain(void);

	//! 디바이스를 반환한다
	ID3D11Device * getDevice(void);

	//! 컨텍스트를 반환한다
	ID3D11DeviceContext * getContext(void);

	//! 렌더 타겟 뷰를 반환한다
	ID3D11RenderTargetView * getRenderTargetView(void);

	//! 깊이 스텐실 뷰를 반환한다
	ID3D11DepthStencilView * getDepthStencilView(void);

public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CDeviceManager);

	//! 초기화
	virtual void init(void);

	//! 스왑 체인 버퍼 크기를 변경한다
	void resizeSwapChainBuffer(void);

private:			// private 함수

	//! 다이렉트 3D 를 생성한다
	LPDIRECT3D9 createDirect3D(void);

	//! 9 버전 디바이스를 생성한다
	LPDIRECT3DDEVICE9 create9VersionDevice(void);

	//! 팩토리를 생성한다
	IDXGIFactory * createFactory(void);

	//! 스왑 체인을 생성한다
	IDXGISwapChain * createSwapChain(void);

	//! 디바이스를 생성한다
	ID3D11Device * createDevice(ID3D11DeviceContext **a_pOutContext);

	//! 렌더 타겟 뷰를 생성한다
	ID3D11RenderTargetView * createRenderTargetView(void);

	//! 깊이 스텐실 뷰를 생성한다
	ID3D11DepthStencilView * createDepthStencilView(void);

private:			// private 변수

	DXGI_MODE_DESC m_stBufferDesc;
	DXGI_SAMPLE_DESC m_stSampleDesc;

	LPDIRECT3D9 m_pDirect3D = nullptr;
	LPDIRECT3DDEVICE9 m_p9VersionDevice = nullptr;

	IDXGIFactory *m_pFactory = nullptr;
	IDXGISwapChain *m_pSwapChain = nullptr;

	ID3D11Device *m_pDevice = nullptr;
	ID3D11DeviceContext *m_pContext = nullptr;
	ID3D11RenderTargetView *m_pRenderTargetView = nullptr;
	ID3D11DepthStencilView *m_pDepthStencilView = nullptr;
};
