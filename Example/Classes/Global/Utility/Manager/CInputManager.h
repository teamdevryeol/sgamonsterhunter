#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

//! 입력 관리자
class CInputManager : public IUpdateable
{
public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;

public:			// getter

	//! 마우스 휠을 반환한다
	float getMouseWheel(void);

	//! 마우스 위치를 반환한다
	POINT getMousePosition(void);

	//! 다이렉트 인풋을 반환한다
	LPDIRECTINPUT8 getDirectInput(void);

	//! 키보드 디바이스를 반환한다
	LPDIRECTINPUTDEVICE8 getKeyboardDevice(void);

	//! 마우스 디바이스를 반환한다
	LPDIRECTINPUTDEVICE8 getMouseDevice(void);

public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CInputManager);

	//! 초기화
	virtual void init(void);

	//! 키보드 눌림 여부를 검사한다
	bool isKeyDown(BYTE a_nKeyCode);

	//! 키보드 눌림 시작 여부를 검사한다
	bool isKeyPressed(BYTE a_nKeyCode);

	//! 키보드 눌림 종료 여부를 검사한다
	bool isKeyReleased(BYTE a_nKeyCode);

	//! 마우스 버튼 눌림 여부를 검사한다
	bool isMouseButtonDown(EMouseButton a_eMouseButton);

	//! 마우스 버튼 눌림 시작 여부를 검사한다
	bool isMouseButtonPressed(EMouseButton a_eMouseButton);

	//! 마우스 버튼 눌림 종료 여부를 검사한다
	bool isMouseButtonReleased(EMouseButton a_eMouseButton);

private:			// private 함수

	//! 다이렉트 인풋을 생성한다
	LPDIRECTINPUT8 createDirectInput(void);

	//! 키보드 디바이스를 생성한다
	LPDIRECTINPUTDEVICE8 createKeyboardDevice(void);

	//! 마우스 디바이스를 생성한다
	LPDIRECTINPUTDEVICE8 createMouseDevice(void);

private:			// private 변수

	BYTE m_anKeyboardState[UCHAR_MAX + 1];
	BYTE m_anPrevKeyboardState[UCHAR_MAX + 1];

	DIMOUSESTATE m_stMouseState;
	DIMOUSESTATE m_stPrevMouseState;

	LPDIRECTINPUT8 m_pDirectInput = nullptr;
	LPDIRECTINPUTDEVICE8 m_pKeyboardDevice = nullptr;
	LPDIRECTINPUTDEVICE8 m_pMouseDevice = nullptr;
};
