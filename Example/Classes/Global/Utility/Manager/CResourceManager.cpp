#include "CResourceManager.h"
#include "../../Function/GlobalFunction.h"
#include "../Render/CMesh.h"
#include "../Render/CShader.h"

CResourceManager::CResourceManager(void)
{
	// Do Nothing
}

CResourceManager::~CResourceManager(void)
{
	for (auto &rValueType : m_oWaveSoundList) {
		SAFE_FREE(rValueType.second.m_pnBytes);
	}

	for (auto &rValueType : m_oMeshList) {
		SAFE_DELETE(rValueType.second);
	}

	for (auto &rValueType : m_oShaderList) {
		SAFE_DELETE(rValueType.second);
	}

	for (auto &rValueType : m_oBufferList) {
		SAFE_RELEASE(rValueType.second);
	}

	for (auto &rValueType : m_oInputLayoutList) {
		SAFE_RELEASE(rValueType.second);
	}

	for (auto &rValueType : m_oRasterizerStateList) {
		SAFE_RELEASE(rValueType.second);
	}

	for (auto &rValueType : m_oBlendStateList) {
		SAFE_RELEASE(rValueType.second);
	}

	for (auto &rValueType : m_oShaderResourceViewList) {
		SAFE_RELEASE(rValueType.second);
	}
}

void CResourceManager::init(void)
{
	// Do Nothing
}

STMaterial CResourceManager::getMaterial(const std::string & a_rKey, bool a_bIsAutoCreate)
{
	auto oIterator = m_oMaterialList.find(a_rKey);
	assert(a_bIsAutoCreate || oIterator != m_oMaterialList.end());

	if (a_bIsAutoCreate && oIterator == m_oMaterialList.end()) {
		auto stMaterial = GetMaterial(a_rKey);
		this->addMaterial(a_rKey, stMaterial);
	}

	return m_oMaterialList[a_rKey];
}

STWaveSound CResourceManager::getWaveSound(const std::string & a_rKey, bool a_bIsAutoCreate)
{
	auto oIterator = m_oWaveSoundList.find(a_rKey);
	assert(a_bIsAutoCreate || oIterator != m_oWaveSoundList.end());

	if (a_bIsAutoCreate && oIterator == m_oWaveSoundList.end()) {
		auto stWaveSound = GetWaveSound(a_rKey);
		this->addWaveSound(a_rKey, stWaveSound);
	}

	return m_oWaveSoundList[a_rKey];
}

CMesh * CResourceManager::getMesh(const std::string & a_rKey)
{
	auto oIterator = m_oMeshList.find(a_rKey);
	return (oIterator == m_oMeshList.end()) ? nullptr : oIterator->second;
}

CShader * CResourceManager::getShader(const std::string & a_rKey)
{
	auto oIterator = m_oShaderList.find(a_rKey);
	return (oIterator == m_oShaderList.end()) ? nullptr : oIterator->second;
}

ID3D11Buffer * CResourceManager::getBuffer(const std::string & a_rKey)
{
	auto oIterator = m_oBufferList.find(a_rKey);
	return (oIterator == m_oBufferList.end()) ? nullptr : oIterator->second;
}

ID3D11InputLayout * CResourceManager::getInputLayout(const std::string & a_rKey)
{
	auto oIterator = m_oInputLayoutList.find(a_rKey);
	return (oIterator == m_oInputLayoutList.end()) ? nullptr : oIterator->second;
}

ID3D11RasterizerState * CResourceManager::getRasterizerState(const std::string & a_rKey)
{
	auto oIterator = m_oRasterizerStateList.find(a_rKey);
	return (oIterator == m_oRasterizerStateList.end()) ? nullptr : oIterator->second;
}

ID3D11BlendState * CResourceManager::getBlendState(const std::string & a_rKey)
{
	auto oIterator = m_oBlendStateList.find(a_rKey);
	return (oIterator == m_oBlendStateList.end()) ? nullptr : oIterator->second;
}

ID3D11ShaderResourceView * CResourceManager::getShaderResourceView(const std::string & a_rKey, bool a_bIsAutoCreate)
{
	auto oIterator = m_oShaderResourceViewList.find(a_rKey);

	if (a_bIsAutoCreate && oIterator == m_oShaderResourceViewList.end()) {
		auto pShaderResourceView = CreateShaderResourceView(a_rKey);
		this->addShaderResourceView(a_rKey, pShaderResourceView);
	}

	return m_oShaderResourceViewList[a_rKey];
}

void CResourceManager::addMaterial(const std::string & a_rKey, const STMaterial & a_rstMaterial)
{
	m_oMaterialList.insert(decltype(m_oMaterialList)::value_type(a_rKey, a_rstMaterial));
}

void CResourceManager::addWaveSound(const std::string & a_rKey, const STWaveSound & a_rstWaveSound)
{
	m_oWaveSoundList.insert(decltype(m_oWaveSoundList)::value_type(a_rKey, a_rstWaveSound));
}

void CResourceManager::addMesh(const std::string & a_rKey, CMesh * a_pMesh)
{
	m_oMeshList.insert(decltype(m_oMeshList)::value_type(a_rKey, a_pMesh));
}

void CResourceManager::addShader(const std::string & a_rKey, CShader * a_pShader)
{
	m_oShaderList.insert(decltype(m_oShaderList)::value_type(a_rKey, a_pShader));
}

void CResourceManager::addBuffer(const std::string & a_rKey, ID3D11Buffer * a_pBuffer)
{
	m_oBufferList.insert(decltype(m_oBufferList)::value_type(a_rKey, a_pBuffer));
}

void CResourceManager::addInputLayout(const std::string & a_rKey, ID3D11InputLayout * a_pInputLayout)
{
	m_oInputLayoutList.insert(decltype(m_oInputLayoutList)::value_type(a_rKey, a_pInputLayout));
}

void CResourceManager::addRasterizerState(const std::string & a_rKey, ID3D11RasterizerState * a_pRasterizerStae)
{
	m_oRasterizerStateList.insert(decltype(m_oRasterizerStateList)::value_type(a_rKey, a_pRasterizerStae));
}

void CResourceManager::addBlendState(const std::string & a_rKey, ID3D11BlendState * a_pBlendState)
{
	m_oBlendStateList.insert(decltype(m_oBlendStateList)::value_type(a_rKey, a_pBlendState));
}

void CResourceManager::addShaderResourceView(const std::string & a_rKey, ID3D11ShaderResourceView * a_pShaderResourceView)
{
	m_oShaderResourceViewList.insert(decltype(m_oShaderResourceViewList)::value_type(a_rKey, a_pShaderResourceView));
}
