#include "CTimeManager.h"
#include "../../Function/GlobalFunction.h"

CTimeManager::CTimeManager(void)
{
	// Do Nothing
}

CTimeManager::~CTimeManager(void)
{
	// Do Nothing
}

void CTimeManager::init(void)
{
	m_oPrevTimePoint = std::chrono::system_clock::now();
	m_oStartTimePoint = std::chrono::system_clock::now();
}

void CTimeManager::update(void)
{
	// 시간 간격을 계산한다
	// {
	auto oCurrentTimePoint = std::chrono::system_clock::now();

	m_fDeltaTime = CalculateDeltaTime(m_oPrevTimePoint, 
		oCurrentTimePoint);

	m_fRunningTime = CalculateDeltaTime(m_oStartTimePoint,
		oCurrentTimePoint);
	// }

	m_oPrevTimePoint = oCurrentTimePoint;
}

float CTimeManager::getDeltaTime(void)
{
	return m_fDeltaTime * m_fTimeScale;
}

float CTimeManager::getRunningTime(void)
{
	return m_fRunningTime;
}

void CTimeManager::setTimeScale(float a_fTimeScale)
{
	m_fTimeScale = a_fTimeScale;
}
