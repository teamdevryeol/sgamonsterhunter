#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CObject.h"

//! 광원
class CLight : public CObject
{
public:			// getter

	//! 난반사 색상을 반환한다
	D3DXCOLOR & getDiffuseColor(void);

	//! 정반사 색상을 반환한다
	D3DXCOLOR & getSpecularColor(void);

	//! 환경 색상을 반환한다
	D3DXCOLOR & getAmbientColor(void);

public:			// 생성자

	//! 생성자
	CLight(const D3DXCOLOR &a_rstDiffuseColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f),
		const D3DXCOLOR &a_rstSpecularColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f),
		const D3DXCOLOR &a_rstAmbientColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

protected:			// protected 함수

	//! 상태를 갱신한다
	virtual void doUpdate(void) override;

private:			// private 변수

	D3DXCOLOR m_stDiffuseColor;
	D3DXCOLOR m_stSpecularColor;
	D3DXCOLOR m_stAmbientColor;
	
	D3DXMATRIXA16 m_stLightViewMatrix;
	D3DXMATRIXA16 m_stLightProjectionMatrix;
};
