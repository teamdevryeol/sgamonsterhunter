#include "CStaticMesh.h"
#include "../../Function/GlobalFunction.h"
#include "../Base/CDirect3DApplication.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Object/CLight.h"
#include "../Object/CCamera.h"
#include "../Render/CMesh.h"

void CStaticMesh::setMaterial(const STMaterial & a_rstMaterial)
{
	m_stMaterial = a_rstMaterial;
}

CStaticMesh::CStaticMesh(const STParameters & a_rstParameters)
:
CRenderObject(),
m_stStaticParameters(a_rstParameters)
{
	// 일반 메시를 생성했을 경우
	if (a_rstParameters.m_eStaticMeshType != EStaticMeshType::XMESH) {
		m_stMaterial.m_nNumMaterials = 0;
	}
	else {
		m_stMaterial = GET_MATERIAL(a_rstParameters.m_oMeshKey);
	}

	m_stParameters.m_oShaderKey = a_rstParameters.m_oShaderKey;
	m_stParameters.m_oTransformBufferKey = a_rstParameters.m_oTransformBufferKey;
	m_stParameters.m_oRenderBufferKey = a_rstParameters.m_oRenderBufferKey;
	m_stParameters.m_oInputLayoutKey = a_rstParameters.m_oInputLayoutKey;

	if (GET_MESH(a_rstParameters.m_oMeshKey) == nullptr) {
		auto pMesh = this->createMesh();
		GET_RESOURCE_MANAGER()->addMesh(a_rstParameters.m_oMeshKey, pMesh);
	}

	// 경계 상자, 경계 구를 설정한다
	// {
	auto stBoundingBox = GetBoundingBox(GET_MESH(a_rstParameters.m_oMeshKey));
	auto stBoundingSphere = GetBoundingSphere(GET_MESH(a_rstParameters.m_oMeshKey));

	this->addBoundingBox(stBoundingBox);
	this->addBoundingSphere(stBoundingSphere);
	// }
}

void CStaticMesh::preRender(void)
{
	CRenderObject::preRender();
	if (m_stStaticParameters.m_eStaticMeshType == EStaticMeshType::BOX ||
		m_stStaticParameters.m_eStaticMeshType == EStaticMeshType::SPHERE)
	{
		auto rasterizerState = GET_RASTERIZER_STATE(KEY_WIREFRAME_RASTERIZER_STATE);
		GET_CONTEXT()->RSSetState(rasterizerState);
	}
	// 렌더 정보를 설정한다
	// {
	auto pRenderBuffer = GET_BUFFER(m_stStaticParameters.m_oRenderBufferKey);

	if (pRenderBuffer != nullptr &&
		m_stStaticParameters.m_oRenderBufferKey == KEY_COLOR_LIGHT_RENDER_BUFFER) 
	{
		auto oRenderBufferPtr = GetBufferPointer<STColorLightRender>(pRenderBuffer);
		auto pstRender = oRenderBufferPtr.get();

		pstRender->m_stColor = m_stColor;
		pstRender->m_stViewPosition = D3DXVECTOR4(GET_CAMERA()->getPosition(), 1.0f);
		pstRender->m_stLightDirection = D3DXVECTOR4(GET_LIGHT()->getForwardDirection(), 0.0f);
	}

	
	// }

	// 버퍼를 설정한다
	// {
	auto pMesh = GET_MESH(m_stStaticParameters.m_oMeshKey);

	if (pMesh != nullptr) {
		pMesh->setupBuffers();
	}

	
	
	// }
}

void CStaticMesh::doRender(void)
{
	CRenderObject::doRender();
	auto pMesh = GET_MESH(m_stStaticParameters.m_oMeshKey);

	if (pMesh != nullptr) {
		DWORD nNumAttributes = pMesh->getNumAttributes();

		for (int i = 0; i < nNumAttributes; ++i) {
			auto stAttribute = pMesh->getAttribute(i);

			// 텍스처가 존재 할 경우
			if (i < m_stMaterial.m_nNumMaterials) {
				auto pNormalMap = m_stMaterial.m_oNormalMapList[i];
				auto pDiffuseMap = m_stMaterial.m_oDiffuseMapList[i];

				GET_CONTEXT()->PSSetShaderResources(0, 1, &pNormalMap);
				GET_CONTEXT()->PSSetShaderResources(1, 1, &pDiffuseMap);
			}

			GET_CONTEXT()->DrawIndexed(stAttribute.FaceCount * 3,
				stAttribute.FaceStart * 3, 0);
		}
 
	}
		
	auto rasterizerState = GET_RASTERIZER_STATE(KEY_SOLID_RASTERIZER_STATE);
	GET_CONTEXT()->RSSetState(rasterizerState);
	
}

CMesh * CStaticMesh::createMesh(void)
{
	switch (m_stStaticParameters.m_eStaticMeshType) {
	case EStaticMeshType::BOX: return CreateBoxMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::SPHERE: return CreateSphereMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::CYLINDER: return CreateCylinderMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::TEAPOT: return CreateTeapotMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::TORUS: return CreateTorusMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::PLANE: return CreatePlaneMesh(m_stStaticParameters.m_oElementDescList);
	case EStaticMeshType::XMESH: return CreateXMesh(m_stStaticParameters.m_oMeshKey, m_stStaticParameters.m_oElementDescList);
	}

	return nullptr;
}
