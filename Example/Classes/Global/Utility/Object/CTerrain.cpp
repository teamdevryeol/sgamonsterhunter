#include "CTerrain.h"
#include "../../Function/GlobalFunction.h"
#include "../Base/CDirect3DApplication.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Object/CLight.h"
#include "../Object/CCamera.h"
#include "../Render/CMesh.h"

CTerrain::CTerrain(const STParameters & a_rstParameters)
:
m_stTerrainParameters(a_rstParameters)
{
	m_stParameters.m_oShaderKey = a_rstParameters.m_oShaderKey;
	m_stParameters.m_oTransformBufferKey = a_rstParameters.m_oTransformBufferKey;
	m_stParameters.m_oRenderBufferKey = a_rstParameters.m_oRenderBufferKey;
	m_stParameters.m_oInputLayoutKey = a_rstParameters.m_oInputLayoutKey;

	// 지형 정보를 설정한다
	this->setupTerrainInfo();

	// 메시가 존재하지 않을 경우
	if (GET_MESH(a_rstParameters.m_oMeshKey) == nullptr) {
		auto pMesh = this->createTerrainMesh();
		GET_RESOURCE_MANAGER()->addMesh(a_rstParameters.m_oMeshKey, pMesh);
	}
}

float CTerrain::getHeightAtPosition(const D3DXVECTOR3 & a_rstPosition)
{
	float fMapPositionX = ((m_stTotalSize.cx / 2.0f) + a_rstPosition.x) / m_stTerrainParameters.m_stTileSize.x;
	float fMapPositionZ = ((m_stTotalSize.cy / 2.0f) - a_rstPosition.z) / m_stTerrainParameters.m_stTileSize.y;

	int nMapIndexX = (int)fMapPositionX;
	int nMapIndexZ = (int)fMapPositionZ;
	int nBaseIndex = (nMapIndexZ * m_stTerrainParameters.m_stMapSize.cx) + nMapIndexX;

	float fLTHeight = m_oHeightList[nBaseIndex];
	float fRTHeight = m_oHeightList[nBaseIndex + 1];
	float fLBHeight = m_oHeightList[nBaseIndex + m_stTerrainParameters.m_stMapSize.cx];
	float fRBHeight = m_oHeightList[nBaseIndex + m_stTerrainParameters.m_stMapSize.cx + 1];

	float fDeltaX = fMapPositionX - nMapIndexX;
	float fDeltaZ = fMapPositionZ - nMapIndexZ;

	// 왼쪽 삼각형 위에 있을 경우
	if (1.0f - fDeltaX > fDeltaZ) {
		float fDeltaHeightX = (fRTHeight - fLTHeight) * fDeltaX;
		float fDeltaHeightZ = (fLBHeight - fLTHeight) * fDeltaZ;

		return fLTHeight + (fDeltaHeightX + fDeltaHeightZ);
	}

	float fDeltaHeightX = (fLBHeight - fRBHeight) * (1.0f - fDeltaX);
	float fDeltaHeightZ = (fRTHeight - fRBHeight) * (1.0f - fDeltaZ);

	return fRBHeight + (fDeltaHeightX + fDeltaHeightZ);
}

void CTerrain::preRender(void)
{
	CRenderObject::preRender();

	// 렌더 버퍼를 설정한다
	// {
	auto pRenderBuffer = GET_BUFFER(m_stTerrainParameters.m_oRenderBufferKey);

	if (pRenderBuffer != nullptr) {
		auto oRenderBufferPtr = GetBufferPointer<STColorLightRender>(pRenderBuffer);
		auto pstRender = oRenderBufferPtr.get();

		pstRender->m_stColor = m_stColor;
		pstRender->m_stViewPosition = D3DXVECTOR4(GET_CAMERA()->getPosition(), 1.0f);
		pstRender->m_stLightDirection = D3DXVECTOR4(GET_LIGHT()->getForwardDirection(), 0.0f);
	}
	// }
	
	// 버퍼를 설정한다
	// {
	auto pMesh = GET_MESH(m_stTerrainParameters.m_oMeshKey);

	if (pMesh != nullptr) {
		pMesh->setupBuffers();
	}
	// }
}

void CTerrain::doRender(void)
{
	CRenderObject::doRender();
	auto pMesh = GET_MESH(m_stTerrainParameters.m_oMeshKey);

	if (pMesh != nullptr) {
		DWORD nNumAttributes = pMesh->getNumAttributes();

		for (int i = 0; i < nNumAttributes; ++i) {
			auto stAttribute = pMesh->getAttribute(i);

			// 텍스처를 설정한다
			// {
			for (int j = 0; j < m_stTerrainParameters.m_oDiffuseMapFilepathList.size(); ++j) {
				auto pDiffuseMap = GET_SHADER_RESOURCE_VIEW(m_stTerrainParameters.m_oDiffuseMapFilepathList[j]);
				GET_CONTEXT()->PSSetShaderResources(j, 1, &pDiffuseMap);
			}

			auto pSplatMap = GET_SHADER_RESOURCE_VIEW(m_stTerrainParameters.m_oSplatMapFilepath);
			GET_CONTEXT()->PSSetShaderResources(4, 1, &pSplatMap);
			// }

			GET_CONTEXT()->DrawIndexed(stAttribute.FaceCount * 3,
				stAttribute.FaceStart * 3, 0);
		}
	}
}

void CTerrain::smoothTerrain(void)
{
	for (int i = 0; i < m_stTerrainParameters.m_nSmoothLevel; ++i) {
		std::vector<float> oSmoothHeightList;
		std::vector<float> oAdjacencyHeightList;

		for (int j = 0; j < m_stTerrainParameters.m_stMapSize.cy; ++j) {
			for (int k = 0; k < m_stTerrainParameters.m_stMapSize.cx; ++k) {
				int nIndex = (j * m_stTerrainParameters.m_stMapSize.cx) + k;
				oAdjacencyHeightList.clear();

				// 왼쪽 정점
				if (k - 1 >= 0) {
					float fHeight = m_oHeightList[nIndex - 1];
					oAdjacencyHeightList.push_back(fHeight);
				}

				// 오른쪽 정점
				if (k + 1 < m_stTerrainParameters.m_stMapSize.cx) {
					float fHeight = m_oHeightList[nIndex + 1];
					oAdjacencyHeightList.push_back(fHeight);
				}

				// 위쪽 정점
				if (j - 1 >= 0) {
					float fHeight = m_oHeightList[nIndex - m_stTerrainParameters.m_stMapSize.cx];
					oAdjacencyHeightList.push_back(fHeight);

					// 왼쪽 정점
					if (k - 1 >= 0) {
						float fHeight = m_oHeightList[nIndex - m_stTerrainParameters.m_stMapSize.cx - 1];
						oAdjacencyHeightList.push_back(fHeight);
					}

					// 오른쪽 정점
					if (k + 1 < m_stTerrainParameters.m_stMapSize.cx) {
						float fHeight = m_oHeightList[nIndex - m_stTerrainParameters.m_stMapSize.cx + 1];
						oAdjacencyHeightList.push_back(fHeight);
					}
				}

				// 아래쪽 정점
				if (j + 1 < m_stTerrainParameters.m_stMapSize.cy) {
					float fHeight = m_oHeightList[nIndex + m_stTerrainParameters.m_stMapSize.cx];
					oAdjacencyHeightList.push_back(fHeight);

					// 왼쪽 정점
					if (k - 1 >= 0) {
						float fHeight = m_oHeightList[nIndex + m_stTerrainParameters.m_stMapSize.cx - 1];
						oAdjacencyHeightList.push_back(fHeight);
					}

					// 오른쪽 정점
					if (k + 1 < m_stTerrainParameters.m_stMapSize.cx) {
						float fHeight = m_oHeightList[nIndex + m_stTerrainParameters.m_stMapSize.cx + 1];
						oAdjacencyHeightList.push_back(fHeight);
					}
				}

				float fHeight = m_oHeightList[nIndex];
				float fTotalHeight = std::accumulate(oAdjacencyHeightList.begin(), oAdjacencyHeightList.end(), 0.0f);

				oSmoothHeightList.push_back((fHeight + (fTotalHeight / oAdjacencyHeightList.size())) * 0.5f);
			}
		}

		m_oHeightList.assign(oSmoothHeightList.begin(), oSmoothHeightList.end());
	}
}

void CTerrain::setupTerrainInfo(void)
{
	// 크기 정보를 설정한다
	m_stTotalSize.cx = m_stTerrainParameters.m_stMapSize.cx * m_stTerrainParameters.m_stTileSize.x;
	m_stTotalSize.cy = m_stTerrainParameters.m_stMapSize.cy * m_stTerrainParameters.m_stTileSize.y;

	// 높이 정보를 설정한다
	// {
	FILE* pstReadStream = fopen(m_stTerrainParameters.m_oHeightMapFilepath.c_str(), "rb");

	if (pstReadStream != nullptr) {
		for (int i = 0; i < m_stTerrainParameters.m_stMapSize.cy; ++i) {
			std::vector<float> oHeightList;

			for (int j = 0; j < m_stTerrainParameters.m_stMapSize.cx; ++j) {
				USHORT nHeight = 0;
				fread(&nHeight, sizeof(nHeight), 1, pstReadStream);

				oHeightList.push_back(nHeight * m_stTerrainParameters.m_fHeightScale);
			}

			while (true)
			{
				m_oHeightList.push_back(oHeightList.back());
				oHeightList.pop_back();

				if (oHeightList.size() == 0)
				{
					break;
				}
			}
		}

		fclose(pstReadStream);
	}
	// }

	// 지형을 부드럽게 만든다
	this->smoothTerrain();
}

CMesh * CTerrain::createTerrainMesh(void)
{
	// 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	auto oElementList = InputLayoutToDeclaration(m_stTerrainParameters.m_oElementDescList);

	D3DXCreateMesh((m_stTerrainParameters.m_stMapSize.cx * m_stTerrainParameters.m_stMapSize.cy) * 2,
		m_stTerrainParameters.m_stMapSize.cx * m_stTerrainParameters.m_stMapSize.cy,
		D3DXMESH_MANAGED | D3DXMESH_32BIT,
		&oElementList[0],
		GET_9VERSION_DEVICE(),
		&pMesh);
	// }

	// 정점 정보를 설정한다
	// {
	BYTE *pnVertices = nullptr;

	if (SUCCEEDED(pMesh->LockVertexBuffer(0, (void **)&pnVertices))) {
		auto oPositionIterator = std::find_if(oElementList.begin(),
			oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
		{
			return a_stElement.Usage == D3DDECLUSAGE_POSITION;
		});

		auto oUVIterator = std::find_if(oElementList.begin(),
			oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
		{
			return a_stElement.Usage == D3DDECLUSAGE_TEXCOORD;
		});

		D3DXVECTOR3 stBasePosition = {
			(m_stTotalSize.cx / -2.0f) + (m_stTerrainParameters.m_stTileSize.x / 2.0f),
			0.0f,
			m_stTotalSize.cy / 2.0f - (m_stTerrainParameters.m_stTileSize.y / 2.0f)
		};

		for (int i = 0; i < m_stTerrainParameters.m_stMapSize.cy; ++i) {
			for (int j = 0; j < m_stTerrainParameters.m_stMapSize.cx; ++j) {
				int nIndex = (i * m_stTerrainParameters.m_stMapSize.cx) + j;
				int nVertexIndex = nIndex * pMesh->GetNumBytesPerVertex();

				if (oPositionIterator != oElementList.end()) {
					int nOffset = oPositionIterator->Offset;

					*reinterpret_cast<D3DXVECTOR3 *>(pnVertices + (nVertexIndex + nOffset)) = stBasePosition + D3DXVECTOR3(j * m_stTerrainParameters.m_stTileSize.x,
						m_oHeightList[nIndex],
						i * -m_stTerrainParameters.m_stTileSize.y);
				}

				if (oUVIterator != oElementList.end()) {
					int nOffset = oUVIterator->Offset;

					*reinterpret_cast<D3DXVECTOR2 *>(pnVertices + (nVertexIndex + nOffset)) = D3DXVECTOR2(j / (float)(m_stTerrainParameters.m_stMapSize.cx - 1),
						i / (float)(m_stTerrainParameters.m_stMapSize.cy - 1));
				}
			}
		}

		pMesh->UnlockVertexBuffer();
	}
	// }

	// 인덱스 정보를 설정한다
	// {
	DWORD *pnIndices = nullptr;

	if (SUCCEEDED(pMesh->LockIndexBuffer(0, (void **)&pnIndices))) {
		for (int i = 0; i < m_stTerrainParameters.m_stMapSize.cy - 1; ++i) {
			for (int j = 0; j < m_stTerrainParameters.m_stMapSize.cx - 1; ++j) {
				int nVertexIndex = (i * m_stTerrainParameters.m_stMapSize.cx) + j;
				int nIndex = nVertexIndex * 6;

				// 왼쪽 삼각형
				pnIndices[nIndex + 0] = nVertexIndex;
				pnIndices[nIndex + 1] = nVertexIndex + 1;
				pnIndices[nIndex + 2] = nVertexIndex + m_stTerrainParameters.m_stMapSize.cx;

				// 오른쪽 삼각형
				pnIndices[nIndex + 3] = nVertexIndex + m_stTerrainParameters.m_stMapSize.cx;
				pnIndices[nIndex + 4] = nVertexIndex + 1;
				pnIndices[nIndex + 5] = nVertexIndex + m_stTerrainParameters.m_stMapSize.cx + 1;
			}
		}

		pMesh->UnlockIndexBuffer();
	}
	// }

	// 인접 정보를 설정한다
	// {
	LPD3DXBUFFER pAdjacency = nullptr;
	D3DXCreateBuffer(sizeof(DWORD) * (pMesh->GetNumFaces() * 3), &pAdjacency);

	pMesh->GenerateAdjacency(FLT_EPSILON, (DWORD *)pAdjacency->GetBufferPointer());
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, m_stTerrainParameters.m_oElementDescList
	});
}
