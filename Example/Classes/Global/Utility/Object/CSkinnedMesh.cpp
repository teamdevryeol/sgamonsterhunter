#include "CSkinnedMesh.h"
#include "../../Function/GlobalFunction.h"
#include "../Base/CDirect3DApplication.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Skinned/CAnimationController.h"
#include "../Object/CLight.h"
#include "../Object/CCamera.h"
#include "../Render/CMesh.h"

CSkinnedMesh::CSkinnedMesh(const STParameters & a_rstParameters)
:
CRenderObject(),
m_stSkinnedParameters(a_rstParameters)
{
	m_stParameters.m_oShaderKey = a_rstParameters.m_oShaderKey;
	m_stParameters.m_oTransformBufferKey = a_rstParameters.m_oTransformBufferKey;
	m_stParameters.m_oRenderBufferKey = a_rstParameters.m_oRenderBufferKey;
	m_stParameters.m_oInputLayoutKey = a_rstParameters.m_oInputLayoutKey;

	m_pSkinnedMesh = this->createSkinnedMeshFromX();

	// 경계 상자, 경계 구를 설정한다
	// {
	for (int i = 0; i < m_oMeshContainerList.size(); ++i) {
		auto pSkinnedMesh = m_oMeshContainerList[i]->m_pSkinnedMesh;

		auto stBoundingBox = GetBoundingBox(pSkinnedMesh);
		auto stBoundingSphere = GetBoundingSphere(pSkinnedMesh);

		this->addBoundingBox(stBoundingBox);
		this->addBoundingSphere(stBoundingSphere);
	}
	// }

}

CSkinnedMesh::~CSkinnedMesh(void)
{
	SAFE_DELETE(m_pAnimationController);

	CAllocateHierarchy oAllocateHierarchy;
	D3DXFrameDestroy(m_pstRootBone, &oAllocateHierarchy);
}

D3DXMATRIX CSkinnedMesh::getBoneMatrix(std::string a_rstBoneName)
{
	auto bone = this->getBone(a_rstBoneName);

	return bone->m_stCombineMatrix;
}

CAllocateHierarchy::STBone * CSkinnedMesh::getBone(std::string a_rstBoneName)
{
	CAllocateHierarchy::STBone* bone = NULL;
	if (m_pstRootBone != NULL)
	{
		bone = this->serchBone(m_pstRootBone, a_rstBoneName);
	}

	if (bone != NULL)
	{
		return bone;
	}

	printf("본을 찾을 수 없습니다.\n");

	return NULL;
}

CAllocateHierarchy::STBone * CSkinnedMesh::getTailBone()
{
	return m_pTailBone;
}

D3DXMATRIX & CSkinnedMesh::getTailBone2Matrix()
{
	return m_pTailBone2->TransformationMatrix;
}

CAllocateHierarchy::STBone * CSkinnedMesh::getRootBone()
{
	return m_pstRootBone;
}

LPD3DXFRAME CSkinnedMesh::getNullBone()
{
	return m_pNullBone;
}

std::vector<std::string>& CSkinnedMesh::getAnimationNameList(void)
{
	return m_pAnimationController->getAnimationNameList();
}

void CSkinnedMesh::setTimeScale(float a_fTimeScale)
{
	m_pAnimationController->setTimeScale(a_fTimeScale);
}

LPD3DXANIMATIONSET CSkinnedMesh::getAnimationSet(void)
{
	return m_pAnimationController->getAnimationSet();
}

double CSkinnedMesh::getDblPercent(void)
{
	return m_pAnimationController->getDblPercent();
}

void CSkinnedMesh::setTailBone2Matrix(const D3DXMATRIXA16 &matrix)
{
	m_pTailBone2->TransformationMatrix = matrix;
}

void CSkinnedMesh::clonAnimation(LPD3DXANIMATIONSET a_pAnimationSet)
{
	m_pAnimationController->clonAnimation(a_pAnimationSet);
}

void CSkinnedMesh::playAnimation(const std::string & a_rAnimationName, bool a_bIsLoop)
{
	m_pAnimationController->playAnimation(a_rAnimationName, a_bIsLoop);
}

void CSkinnedMesh::playAnimationWithFrame(int a_nStartFrame, int a_nEndFrame, const std::string& a_rAnimationName, bool a_bIsLoop)
{
	m_pAnimationController->playAnimationWithFrame(a_nStartFrame, a_nEndFrame, a_rAnimationName, a_bIsLoop);
}

void CSkinnedMesh::stopAnimation(void)
{
	m_pAnimationController->stopAnimation();
}

void CSkinnedMesh::doUpdate(void)
{
	CRenderObject::doUpdate();
	m_pAnimationController->update();

	auto &rstWorldMatrix = this->getFinalWorldMatrix();
	this->updateBone(m_pstRootBone, rstWorldMatrix);
}

void CSkinnedMesh::doRender(void)
{
	CRenderObject::doRender();
	this->drawBone(m_pstRootBone);
}

void CSkinnedMesh::updateBone(LPD3DXFRAME a_pstFrame, const D3DXMATRIXA16 & a_rstMatrix)
{
	auto pstBone = (CAllocateHierarchy::STBone *)a_pstFrame;
	pstBone->m_stCombineMatrix = pstBone->TransformationMatrix * a_rstMatrix;

	if (pstBone->Name != NULL)
	{
		if (strcmp(pstBone->Name, "TL-03") == 0)
		{
			m_pBone = pstBone;
		}
		if (strcmp(pstBone->Name, "TL-03_t") == 0)
		{
			pstBone->m_stCombineMatrix = m_pBone->m_stCombineMatrix;
		}

		if (strcmp(pstBone->Name, "NULL") == 0)
		{
			m_pNullBone = pstBone;
		}
	}
	
	// 형제 본이 존재 할 경우
	if (a_pstFrame->pFrameSibling != nullptr) {
		this->updateBone(a_pstFrame->pFrameSibling, a_rstMatrix);
	}

	// 자식 본이 존재 할 경우
	if (a_pstFrame->pFrameFirstChild != nullptr) {
		this->updateBone(a_pstFrame->pFrameFirstChild,
			pstBone->m_stCombineMatrix);
	}
}

void CSkinnedMesh::drawBone(LPD3DXFRAME a_pstFrame)
{
	auto pstMeshContainer = a_pstFrame->pMeshContainer;

	while (pstMeshContainer != nullptr) {
		this->drawMeshContainer(pstMeshContainer);
		pstMeshContainer = pstMeshContainer->pNextMeshContainer;
	}

	// 형제 본이 존재 할 경우
	if (a_pstFrame->pFrameSibling != nullptr) {
		this->drawBone(a_pstFrame->pFrameSibling);
	}

	// 자식 본이 존재 할 경우
	if (a_pstFrame->pFrameFirstChild != nullptr) {
		this->drawBone(a_pstFrame->pFrameFirstChild);
	}
}

void CSkinnedMesh::drawMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer)
{
	auto pstMeshContainer = (CAllocateHierarchy::STMeshContainer *)a_pstMeshContainer;

	for (int i = 0; i < pstMeshContainer->m_oBoneCombinationList.size(); ++i) {
		int nNumBlends = 0;
		auto stBoneCombination = pstMeshContainer->m_oBoneCombinationList[i];

		// 최대 블렌딩 인덱스를 탐색한다
		for (int j = 0; j < pstMeshContainer->m_nNumBlends; ++j) {
			if (stBoneCombination.BoneId[j] != UINT_MAX) {
				nNumBlends = j;
			}
		}

		// 행렬 정보를 설정한다
		// {
		D3DXMATRIXA16 astBoneMatrices[MAX_NUM_BLENDS];

		for (int j = 0; j < pstMeshContainer->m_nNumBlends; ++j) {
			if (stBoneCombination.BoneId[j] == UINT_MAX) {
				D3DXMatrixIdentity(&astBoneMatrices[j]);
			}
			else {
				int nIndex = stBoneCombination.BoneId[j];
				auto pstBone = pstMeshContainer->m_oBoneList[nIndex];
				auto stBoneMatrix = pstMeshContainer->m_oBoneMatrixList[nIndex];

				astBoneMatrices[j] = stBoneMatrix * pstBone->m_stCombineMatrix;
			}
		}
		// }

		// 렌더 정보를 설정한다
		// {
		auto pRenderBuffer = GET_BUFFER(m_stSkinnedParameters.m_oRenderBufferKey);

		if (pRenderBuffer != nullptr) {
			auto oRenderBufferPtr = GetBufferPointer<STSkinnedMeshRender>(pRenderBuffer);
			auto pstRender = oRenderBufferPtr.get();

			pstRender->m_stColor = m_stColor;
			pstRender->m_stViewPosition = D3DXVECTOR4(GET_CAMERA()->getPosition(), 1.0f);
			pstRender->m_stLightDirection = D3DXVECTOR4(GET_LIGHT()->getForwardDirection(), 0.0f);
			pstRender->m_stNumBlends = D3DXVECTOR4((float)(nNumBlends + 1), 0.0f, 0.0f, 0.0f);

			CopyMemory(pstRender->m_astBoneMatrices,
				astBoneMatrices, sizeof(astBoneMatrices));
		}
		// }

		// 텍스처를 설정한다
		auto pDiffuseMap = pstMeshContainer->m_oDiffuseMapList[stBoneCombination.AttribId];
		GET_CONTEXT()->PSSetShaderResources(0, 1, &pDiffuseMap);
		
		// 버퍼를 설정한다
		pstMeshContainer->m_pSkinnedMesh->setupBuffers();

		// 물체를 그린다
		GET_CONTEXT()->DrawIndexed(stBoneCombination.FaceCount * 3,
			stBoneCombination.FaceStart * 3, 0);
	}
}

void CSkinnedMesh::setupBone(LPD3DXFRAME a_pstFrame)
{
	if (a_pstFrame->Name != NULL)
	{
		if (strcmp(a_pstFrame->Name, "NULL") == 0)
		{
			m_pNullBone = a_pstFrame;
		}
		if (strcmp(a_pstFrame->Name, "TL-04") == 0)
		{
			m_pTailBone = (CAllocateHierarchy::STBone*)a_pstFrame;
		}
	}

	if (a_pstFrame->pMeshContainer != nullptr) {
		this->setupBoneOnMeshContainer(a_pstFrame->pMeshContainer);
	}

	// 형제 본이 존재 할 경우
	if (a_pstFrame->pFrameSibling != nullptr) {
		this->setupBone(a_pstFrame->pFrameSibling);
	}

	// 자식 본이 존재 할 경우
	if (a_pstFrame->pFrameFirstChild != nullptr) {
		this->setupBone(a_pstFrame->pFrameFirstChild);
	}
}

void CSkinnedMesh::setupBoneOnMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer)
{
	auto pstMeshContainer = (CAllocateHierarchy::STMeshContainer *)a_pstMeshContainer;

	for (int i = 0; i < pstMeshContainer->pSkinInfo->GetNumBones(); ++i) {
		auto pszName = pstMeshContainer->pSkinInfo->GetBoneName(i);
		auto pstFrame = D3DXFrameFind(m_pstRootBone, pszName);
		pstMeshContainer->m_oBoneList.push_back((CAllocateHierarchy::STBone *)pstFrame);
	
	}
}

CMesh * CSkinnedMesh::createSkinnedMesh(LPD3DXMESHCONTAINER a_pstMeshContainer, int a_nMeshContainerNumber)
{
	auto pstMeshContainer = (CAllocateHierarchy::STMeshContainer *)a_pstMeshContainer;

	// 스킨드 메시를 생성한다
	// {
	LPD3DXMESH pSkinnedMesh = nullptr;
	LPD3DXBUFFER pBoneCombination = nullptr;

	pstMeshContainer->pSkinInfo->ConvertToBlendedMesh(pstMeshContainer->MeshData.pMesh,
		pstMeshContainer->MeshData.pMesh->GetOptions() | D3DXMESHOPT_VERTEXCACHE,
		pstMeshContainer->pAdjacency,
		pstMeshContainer->pAdjacency,
		NULL,
		NULL,
		&pstMeshContainer->m_nNumBlends,
		&pstMeshContainer->m_nNumBoneCombinations,
		&pBoneCombination,
		&pSkinnedMesh);
	// }

	// 본 조합 정보를 설정한다
	// {
	for (int i = 0; i < pstMeshContainer->m_nNumBoneCombinations; ++i) {
		LPD3DXBONECOMBINATION pstBoneCombinations = (LPD3DXBONECOMBINATION)pBoneCombination->GetBufferPointer();
		
		D3DXBONECOMBINATION stBoneCombination = pstBoneCombinations[i];
		stBoneCombination.BoneId = (DWORD *)malloc(sizeof(DWORD) * pstMeshContainer->m_nNumBlends);

		CopyMemory(stBoneCombination.BoneId, 
			pstBoneCombinations[i].BoneId, sizeof(DWORD) * pstMeshContainer->m_nNumBlends);

		pstMeshContainer->m_oBoneCombinationList.push_back(stBoneCombination);
	}

	SAFE_RELEASE(pBoneCombination);
	// }

	// 인접 정보를 설정한다
	// {
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateBuffer(sizeof(DWORD) * (pSkinnedMesh->GetNumFaces() * 3),
		&pAdjacency);

	CopyMemory(pAdjacency->GetBufferPointer(),
		pstMeshContainer->pAdjacency, sizeof(DWORD) * (pSkinnedMesh->GetNumFaces() * 3));
	// }

	// 메시 컨테이너를 설정한다
	m_oMeshContainerList.push_back(pstMeshContainer);

	return Create<CMesh>(CMesh::STParameters{
		pSkinnedMesh, pAdjacency, m_stSkinnedParameters.m_oElementDescList
	});
}

CMesh * CSkinnedMesh::createSkinnedMeshFromX(void)
{
	// 스킨드 메시를 생성한다
	// {
	CAllocateHierarchy oAllocateHierarchy(CAllocateHierarchy::STParameters{
		GetBasePath(m_stSkinnedParameters.m_oMeshKey),
		std::bind(&CSkinnedMesh::createSkinnedMesh, this, std::placeholders::_1, std::placeholders::_2)
	});

	LPD3DXANIMATIONCONTROLLER pAnimationController = nullptr;

	D3DXLoadMeshHierarchyFromXA(m_stSkinnedParameters.m_oMeshKey.c_str(),
		D3DXMESH_MANAGED,
		GET_9VERSION_DEVICE(),
		&oAllocateHierarchy,
		NULL,
		(LPD3DXFRAME *)&m_pstRootBone,
		&pAnimationController);
	// }

	// 본을 설정한다
	this->setupBone(m_pstRootBone);

	// 애니메이션 컨트롤러를 설정한다
	m_pAnimationController = Create<CAnimationController>(pAnimationController);

	return m_oMeshContainerList.front()->m_pSkinnedMesh;
}

CAllocateHierarchy::STBone * CSkinnedMesh::serchBone(LPD3DXFRAME a_pstFrame, std::string a_rstBoneName)
{
	if (a_pstFrame->Name != NULL)
	{
		if (strcmp(a_pstFrame->Name, a_rstBoneName.c_str()) == 0)
		{
			return (CAllocateHierarchy::STBone *)a_pstFrame;
		}
	}

	// 형제 본이 존재 할 경우
	if (a_pstFrame->pFrameSibling != nullptr) {
		auto bone = this->serchBone(a_pstFrame->pFrameSibling, a_rstBoneName);
		if (bone != NULL)
			return bone;
	}

	// 자식 본이 존재 할 경우
	if (a_pstFrame->pFrameFirstChild != nullptr) {
		auto bone = this->serchBone(a_pstFrame->pFrameFirstChild, a_rstBoneName);
		if (bone != NULL)
			return bone;
	}

	return NULL;
}
