#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"
#include "../Interface/IRenderable.h"
#include "CWindowApplication.h"

class CGrid;
class CGizmo;
class CLight;
class CCamera;
class CRenderObject;

//! 다이렉트 3D 어플리케이션
class CDirect3DApplication : public CWindowApplication,
	public IUpdateable, public IRenderable
{
public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;
	
	//! 물체를 그린다
	virtual void render(void) override;

public:			// getter, setter

	//! 광원을 반환한다
	CLight * getLight(void);

	//! 카메라를 반환한다
	CCamera * getCamera(void);

	//! 렌더 객체를 반환한다
	CRenderObject * getRenderObject(void);

	//! 렌더 객체를 변경한다
	void setRenderObject(CRenderObject *a_pRenderObject);

	//! 윈도우 크기를 변경한다
	virtual void setWindowSize(const SIZE &a_rstWindowSize) override;

protected:			// protected 함수

	//! 초기화
	virtual void init(void) override;

	//! 메세지 루프를 구동한다
	virtual int runMessageLoop(void) override;

	//! 기본 리소스를 설정한다
	virtual void setupDefaultResources(void);

	//! 색상 리소스를 설정한다
	virtual void setupColorResources(void);

	//! 색상 광원 리소스를 설정한다
	virtual void setupColorLightResources(void);

	//! 색상 텍스처 리소를 설정한다
	virtual void setupColorTextureResources(void);

	//! 텍스처 리소스를 설정한다
	virtual void setupTextureResources(void);

	//! 텍스처 광원 리소스를 설정한다
	virtual void setupTextureLightResources(void);

	//! 정적 메시 리소스를 설정한다
	virtual void setupStaticMeshResources(void);

	//! 스킨드 메시 리소스를 설정한다
	virtual void setupSkinnedMeshResources(void);

	//! 지형 리소스를 설정한다
	virtual void setupTerrainResources(void);

protected:			// 생성자, 소멸자

	//! 생성자
	CDirect3DApplication(HINSTANCE a_hInstance,
		int a_nShowOptions, const SIZE &a_rstWindowSize);

	//! 소멸자
	virtual ~CDirect3DApplication(void) = 0;

protected:			// protected 변수

	POINT m_stPrevMousePosition;
	D3DXCOLOR m_stClearColor;

	CGizmo *m_pGridGizmo = nullptr;
	CGizmo *m_pLightGizmo = nullptr;

	CGrid *m_pGrid = nullptr;
	CLight *m_pLight = nullptr;
	CCamera *m_pCamera = nullptr;
	CRenderObject *m_pRenderObject = nullptr;
	CRenderObject *m_pNextRenderObject = nullptr;

	ID3D11SamplerState *m_pSamplerState = nullptr;
};
