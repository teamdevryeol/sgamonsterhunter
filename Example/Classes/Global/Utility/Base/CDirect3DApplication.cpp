#include "CDirect3DApplication.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CWindowManager.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CTimeManager.h"
#include "../Manager/CInputManager.h"
#include "../Manager/CSoundManager.h"
#include "../Manager/CResourceManager.h"
#include "../Base/CRenderObject.h"
#include "../Object/CLight.h"
#include "../Object/CCamera.h"
#include "../Debug/CGrid.h"
#include "../Debug/CGizmo.h"
#include "../Render/CShader.h"

CDirect3DApplication::CDirect3DApplication(HINSTANCE a_hInstance, int a_nShowOptions, const SIZE & a_rstWindowSize)
:
CWindowApplication(a_hInstance, a_nShowOptions, a_rstWindowSize),
m_stClearColor(0.0f, 0.008f, 0.094f, 1.0f)
{
	ZeroMemory(&m_stPrevMousePosition, sizeof(m_stPrevMousePosition));
}

CDirect3DApplication::~CDirect3DApplication(void)
{
	SAFE_DELETE(m_pGridGizmo);

	SAFE_DELETE(m_pGrid);
	SAFE_DELETE(m_pLight);
	SAFE_DELETE(m_pCamera);
	SAFE_DELETE(m_pRenderObject);
	SAFE_DELETE(m_pNextRenderObject);

	SAFE_RELEASE(m_pSamplerState);
}

void CDirect3DApplication::init(void)
{
	CWindowApplication::init();
	GET_DEVICE_MANAGER()->init();

	// 기본 리소스를 설정한다
	this->setupDefaultResources();

	// 그리드를 설정한다
	m_pGrid = Create<CGrid>(30);
	m_pGrid->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// 그리드 기즈모를 설정한다
	m_pGridGizmo = Create<CGizmo>(15);
	m_pGridGizmo->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// 광원 기즈모를 생성한다
	m_pLightGizmo = Create<CGizmo>(3);
	m_pLightGizmo->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// 광원을 생성한다
	// {
	m_pLight = Create<CLight>();
	m_pLight->setPosition(D3DXVECTOR3(0.0f, 0.0f, -5.0f));

	m_pLight->addChildObject(m_pLightGizmo);
	// }

	// 카메라를 설정한다
	// {
	m_pCamera = Create<CCamera>(50.0f, 
		GET_WINDOW_SIZE().cx / (float)GET_WINDOW_SIZE().cy);

	m_pCamera->setPosition(D3DXVECTOR3(0.0f, 0.0f, -5.0f));
	// }

	// 렌더 객체를 설정한다
	m_pRenderObject = Create<CRenderObject>();
	m_pRenderObject->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
}

void CDirect3DApplication::update(void)
{
	m_pGrid->update();
	m_pGridGizmo->update();
	m_pLight->update();
	m_pCamera->update();
	m_pRenderObject->update();

	if (IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT)) {
		// 카메라 이동을 설정한다
		// {
		float fMoveSpeed = IS_KEY_DOWN(DIK_LSHIFT)? 50.0f : 5.0f;

		if (IS_KEY_DOWN(DIK_W)) {
			m_pCamera->moveByForwardDirection(fMoveSpeed * GET_DELTA_TIME());
		}
		else if (IS_KEY_DOWN(DIK_S)) {
			m_pCamera->moveByForwardDirection(-fMoveSpeed * GET_DELTA_TIME());
		}

		if (IS_KEY_DOWN(DIK_A)) {
			m_pCamera->moveByRightDirection(-fMoveSpeed * GET_DELTA_TIME());
		}
		else if (IS_KEY_DOWN(DIK_D)) {
			m_pCamera->moveByRightDirection(fMoveSpeed * GET_DELTA_TIME());
		}

		if (IS_KEY_DOWN(DIK_Z)) {
			m_pCamera->moveByUpDirection(fMoveSpeed * GET_DELTA_TIME());
		}
		else if (IS_KEY_DOWN(DIK_C)) {
			m_pCamera->moveByUpDirection(-fMoveSpeed * GET_DELTA_TIME());
		}
		// }

		// 카메라 회전을 설정한다
		// {
		if (IS_MOUSE_BUTTON_PRESSED(EMouseButton::MIDDLE)) {
			m_stPrevMousePosition = GET_MOUSE_POSITION();
		}

		auto stMousePosition = GET_MOUSE_POSITION();

		float fDeltaX = stMousePosition.x - m_stPrevMousePosition.x;
		float fDeltaY = stMousePosition.y - m_stPrevMousePosition.y;

		m_pCamera->rotateByUpDirection(fDeltaX, false);
		m_pCamera->rotateByRightDirection(fDeltaY, true);

		m_stPrevMousePosition = stMousePosition;

		//if (IS_KEY_DOWN(DIK_Q)) {
		//	m_pCamera->rotateByUpDirection(-90.0f * /GET_DELTA_TIME/(), false);
		//}
		//else if (IS_KEY_DOWN(DIK_E)) {
		//	m_pCamera->rotateByUpDirection(90.0f * GET_DELTA_TIME//(), false);
		//}
		// }
	}
}

void CDirect3DApplication::render(void)
{
	// 샘플러 상태를 설정한다
	GET_CONTEXT()->PSSetSamplers(0, 1, &m_pSamplerState);

	// 래스라이저 상태를 설정한다
	auto pRasterizerState = GET_RASTERIZER_STATE(KEY_SOLID_RASTERIZER_STATE);
	GET_CONTEXT()->RSSetState(pRasterizerState);

	//m_pGrid->render();
	//m_pGridGizmo->render();
	//m_pLightGizmo->render();
	m_pRenderObject->render();
}

CLight * CDirect3DApplication::getLight(void)
{
	return m_pLight;
}

CCamera * CDirect3DApplication::getCamera(void)
{
	return m_pCamera;
}

CRenderObject * CDirect3DApplication::getRenderObject(void)
{
	return m_pRenderObject;
}

void CDirect3DApplication::setRenderObject(CRenderObject * a_pRenderObject)
{
	m_pNextRenderObject = a_pRenderObject;
}

void CDirect3DApplication::setWindowSize(const SIZE & a_rstWindowSize)
{
	CWindowApplication::setWindowSize(a_rstWindowSize);

	GET_DEVICE_MANAGER()->resizeSwapChainBuffer();
	m_pCamera->setAspect(a_rstWindowSize.cx / (float)a_rstWindowSize.cy);
}

int CDirect3DApplication::runMessageLoop(void)
{
	// 관리자를 초기화한다
	GET_TIME_MANAGER()->init();
	GET_INPUT_MANAGER()->init();
	GET_RESOURCE_MANAGER()->init();

#ifdef SOUND_MANAGER_ENABLE
	GET_SOUND_MANAGER()->init();
#endif			// #ifdef SOUND_MANAGER_ENABLE

	MSG stMessage;
	ZeroMemory(&stMessage, sizeof(stMessage));

	while (stMessage.message != WM_QUIT) {
		if (PeekMessage(&stMessage, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&stMessage);
			DispatchMessage(&stMessage);
		}

		// 관리자 상태를 갱신한다
		GET_TIME_MANAGER()->update();
		GET_INPUT_MANAGER()->update();

		// 렌더 타겟 뷰, 깊이 스텐실 뷰를 지운다
		// {
		GET_CONTEXT()->ClearRenderTargetView(GET_RENDER_TARGET_VIEW(),
			m_stClearColor);

		GET_CONTEXT()->ClearDepthStencilView(GET_DEPTH_STENCIL_VIEW(),
			D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		// }

		this->update();
		this->render();

		if (m_pNextRenderObject != nullptr) {
			auto pPrevRenderObject = m_pRenderObject;
			m_pRenderObject = m_pNextRenderObject;

			m_pNextRenderObject = nullptr;
			SAFE_DELETE(pPrevRenderObject);
		}

		// 페이지를 플리핑한다
		GET_SWAP_CHAIN()->Present(0, 0);
	}

	return stMessage.wParam;
}

void CDirect3DApplication::setupDefaultResources(void)
{
	// 변환 버퍼를 설정한다
	// {
	auto pTransformBuffer = CreateBuffer(sizeof(STTransform),
		D3D11_BIND_CONSTANT_BUFFER);

	GET_RESOURCE_MANAGER()->addBuffer(KEY_TRANSFORM_BUFFER,
		pTransformBuffer);
	// }

	// 샘플러 상태를 설정한다
	m_pSamplerState = CreateSamplerState(D3D11_FILTER_MIN_MAG_MIP_LINEAR);

	// 래스터라이저 상태를 설정한다
	// {
	auto pSolidRasterizerState = CreateRasterizerState(D3D11_FILL_SOLID, D3D11_CULL_NONE);
	auto pWireframeRasterizerState = CreateRasterizerState(D3D11_FILL_WIREFRAME, D3D11_CULL_NONE);

	GET_RESOURCE_MANAGER()->addRasterizerState(KEY_SOLID_RASTERIZER_STATE,
		pSolidRasterizerState);

	GET_RESOURCE_MANAGER()->addRasterizerState(KEY_WIREFRAME_RASTERIZER_STATE,
		pWireframeRasterizerState);
	// }

	// 블렌드 상태를 설정한다
	// {
	auto pAlphaBlendEnableState = CreateBlendState(true);
	auto pAlphaBlendDisableState = CreateBlendState(false);

	GET_RESOURCE_MANAGER()->addBlendState(KEY_ALPHA_BLEND_ENABLE_STATE, 
		pAlphaBlendEnableState);

	GET_RESOURCE_MANAGER()->addBlendState(KEY_ALPHA_BLEND_DISABLE_STATE,
		pAlphaBlendDisableState);
	// }

	// 리소스를 설정한다
	// {
	this->setupColorResources();
	this->setupColorLightResources();
	this->setupColorTextureResources();

	this->setupTextureResources();
	this->setupTextureLightResources();

	this->setupStaticMeshResources();
	this->setupSkinnedMeshResources();

	this->setupTerrainResources();
	// }
}

void CDirect3DApplication::setupColorResources(void)
{
	// 색상 쉐이더를 설정한다
	// {
	auto pColorShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/ColorVertexShader.vsh",
		"Resources/Shaders/Default/ColorPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_COLOR_SHADER,
		pColorShader);
	// }

	// 색상 입력 레이아웃을 설정한다
	// {
	auto pColorInputLayout = CreateInputLayout(INPUT_COLOR_ELEMENT_DESCS, 
		GET_VERTEX_SHADER_CODE(KEY_COLOR_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_COLOR_INPUT_LAYOUT,
		pColorInputLayout);
	// }

	// 색상 렌더 버퍼를 설정한다
	// {
	auto pColorRenderBuffer = CreateBuffer(sizeof(STColorRender),
		D3D11_BIND_CONSTANT_BUFFER);

	GET_RESOURCE_MANAGER()->addBuffer(KEY_COLOR_RENDER_BUFFER,
		pColorRenderBuffer);
	// }
}

void CDirect3DApplication::setupColorLightResources(void)
{
	// 색상 광원 쉐이더를 설정한다
	// {
	auto pColorLightShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/ColorLightVertexShader.vsh",
		"Resources/Shaders/Default/ColorLightPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_COLOR_LIGHT_SHADER,
		pColorLightShader);
	// }

	// 색상 광원 입력 레이아웃을 설정한다
	// {
	auto pColorLightInputLayout = CreateInputLayout(INPUT_COLOR_LIGHT_ELEMENT_DESCS, 
		GET_VERTEX_SHADER_CODE(KEY_COLOR_LIGHT_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_COLOR_LIGHT_INPUT_LAYOUT,
		pColorLightInputLayout);
	// }

	// 색상 광원 렌더 버퍼를 설정한다
	// {
	auto pColorLightRenderBuffer = CreateBuffer(sizeof(STColorLightRender),
		D3D11_BIND_CONSTANT_BUFFER);

	GET_RESOURCE_MANAGER()->addBuffer(KEY_COLOR_LIGHT_RENDER_BUFFER,
		pColorLightRenderBuffer);
	// }
}

void CDirect3DApplication::setupColorTextureResources(void)
{
	// 색상 텍스처 쉐이더를 설정한다
	// {
	auto pColorTextureShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/ColorTextureVertexShader.vsh",
		"Resources/Shaders/Default/ColorTexturePixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_COLOR_TEXTURE_SHADER,
		pColorTextureShader);
	// }

	// 색상 텍스처 입력 레이아웃을 설정한다
	// {
	auto pColorTextureInputLayout = CreateInputLayout(INPUT_COLOR_TEXTURE_ELEMENT_DESCS,
		GET_VERTEX_SHADER_CODE(KEY_COLOR_TEXTURE_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_COLOR_TEXTURE_INPUT_LAYOUT,
		pColorTextureInputLayout);
	// }
}

void CDirect3DApplication::setupTextureResources(void)
{
	// 텍스처 쉐이더를 설정한다
	// {
	auto pTextureShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/TextureVertexShader.vsh",
		"Resources/Shaders/Default/TexturePixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_TEXTURE_SHADER,
		pTextureShader);
	// }

	// 텍스처 입력 레이아웃을 설정한다
	// {
	auto pTextureInputLayout = CreateInputLayout(INPUT_TEXTURE_ELEMENT_DESCS,
		GET_VERTEX_SHADER_CODE(KEY_TEXTURE_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_TEXTURE_INPUT_LAYOUT,
		pTextureInputLayout);
	// }
}

void CDirect3DApplication::setupTextureLightResources(void)
{
	// 텍스처 광원 쉐이더를 설정한다
	// {
	auto pTextureLightShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/TextureLightVertexShader.vsh",
		"Resources/Shaders/Default/TextureLightPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_TEXTURE_LIGHT_SHADER,
		pTextureLightShader);
	// }

	// 텍스처 광원 입력 레이아웃을 설정한다
	// {
	auto pTextureLightInputLayout = CreateInputLayout(INPUT_TEXTURE_LIGHT_ELEMENT_DESCS,
		GET_VERTEX_SHADER_CODE(KEY_TEXTURE_LIGHT_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_TEXTURE_LIGHT_INPUT_LAYOUT,
		pTextureLightInputLayout);
	// }
}

void CDirect3DApplication::setupStaticMeshResources(void)
{
	// 정적 메시 쉐이더를 설정한다
	// {
	auto pStaticMeshShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/StaticMeshVertexShader.vsh",
		"Resources/Shaders/Default/StaticMeshPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_STATIC_MESH_SHADER,
		pStaticMeshShader);
	// }

	// 정적 메시 입력 레이아웃을 설정한다
	// {
	auto pStaticMeshInputLayout = CreateInputLayout(INPUT_STATIC_MESH_ELEMENT_DESCS,
		GET_VERTEX_SHADER_CODE(KEY_STATIC_MESH_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_STATIC_MESH_INPUT_LAYOUT,
		pStaticMeshInputLayout);
	// }
}

void CDirect3DApplication::setupSkinnedMeshResources(void)
{
	// 스킨드 메시 쉐이더를 설정한다
	// {
	auto pSkinnedMeshShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/SkinnedMeshVertexShader.vsh",
		"Resources/Shaders/Default/SkinnedMeshPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_SKINNED_MESH_SHADER,
		pSkinnedMeshShader);
	// }

	// 스킨드 메시 입력 레이아웃을 설정한다
	// {
	auto pSkinnedMeshInputLayout = CreateInputLayout(INPUT_SKINNED_MESH_ELEMENT_DESCS,
		GET_VERTEX_SHADER_CODE(KEY_SKINNED_MESH_SHADER));

	GET_RESOURCE_MANAGER()->addInputLayout(KEY_SKINNED_MESH_INPUT_LAYOUT,
		pSkinnedMeshInputLayout);
	// }

	// 스킨드 메시 렌더 버퍼를 설정한다
	// {
	auto pSkinnedMeshRenderBuffer = CreateBuffer(sizeof(STSkinnedMeshRender),
		D3D11_BIND_CONSTANT_BUFFER);

	GET_RESOURCE_MANAGER()->addBuffer(KEY_SKINNED_MESH_RENDER_BUFFER,
		pSkinnedMeshRenderBuffer);
	// }
}

void CDirect3DApplication::setupTerrainResources(void)
{
	auto pTerrainShader = Create<CShader>(CShader::STParameters{
		"Resources/Shaders/Default/TerrainVertexShader.vsh",
		"Resources/Shaders/Default/TerrainPixelShader.psh"
	});

	GET_RESOURCE_MANAGER()->addShader(KEY_TERRAIN_SHADER,
		pTerrainShader);
}
