#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

class CUIObject : public IUpdateable
{
public:
	virtual void update(void) override;
	virtual void render(void);

public:
	CC_SYNTHESIZE_READONLY(EObjectType, m_eObjectType, ObjectType);
	CC_SYNTHESIZE(D3DXVECTOR2, m_stLocalPosition, LocalPosition);
	CC_SYNTHESIZE(D3DXVECTOR2, m_stScale, Scale);
	CC_SYNTHESIZE(CUIObject*, m_pParent, Parent);
	CC_SYNTHESIZE(int, m_nTag, Tag);

public:

	void addChildObject(CUIObject *a_pChildObject);
	CUIObject* findChildObjectbyTag(int a_nTag);
	void removeChildObject(CUIObject *a_pChildObject);
	void removeChildObjectbyTag(int a_nTag);

public:
	CUIObject(void);
	virtual ~CUIObject(void);

protected:

	std::vector<CUIObject*>	m_vecChildren;
	D3DXVECTOR2				m_stWorldPosition;
	bool					m_isHide = false;
};
