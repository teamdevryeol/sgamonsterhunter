#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

//! 최상위 객체
class CObject : public IUpdateable
{
public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override final;

public:			// getter, setter

	//! 객체 타입을 반환한다
	EObjectType getObjectType(void);

	//! 위치를 반환한다
	D3DXVECTOR3 & getPosition(void);

	//! 척도를 반환한다
	D3DXVECTOR3 & getScale(void);

	//! 회전을 반환한다
	D3DXVECTOR3 & getRotation(void);

	//! X 축 방향을 반환한다
	D3DXVECTOR3 & getRightDirection(void);

	//! Y 축 방향을 반환한다
	D3DXVECTOR3 & getUpDirection(void);

	//! Z 축 방향을 반한한다
	D3DXVECTOR3 & getForwardDirection(void);

	//! 월드 행렬을 반환한다
	D3DXMATRIXA16 & getWorldMatrix(void);

	//! 최종 월드 행렬을 반환한다
	D3DXMATRIXA16 & getFinalWorldMatrix(void);

	//! 부모 객체를 반환한다
	CObject * getParentObject(void);

	//! 간격을 변경한다
	void setOffset(const D3DXVECTOR3 &a_rstOffset);

	//! 위치를 변경한다
	void setPosition(const D3DXVECTOR3 &a_rstPosition);

	//! 척도를 변경한다
	void setScale(const D3DXVECTOR3 &a_rstScale);

	//! 회전을 변경한다
	void setRotation(const D3DXVECTOR3 &a_rstRotation);

	//! X 축 방향을 변경한다
	void setRightDirection(const D3DXVECTOR3 &a_rstRightDirection);

	//! Y 축 방향을 변경한다
	void setUpDirection(const D3DXVECTOR3 &a_rstUpDirection);

	//! Z 축 방향을 변경한다
	void setForwardDirection(const D3DXVECTOR3 &a_rstForwardDirection);

	//! 행렬을 변경한다.
	void setWorldMatrix(const D3DXMATRIXA16& a_rstWorldMatrix);

	//! 부모 객체를 변경한다
	void setParentObject(CObject *a_pParentObject);

public:			// public 함수

	//! 방향 벡터를 초기화한다
	void resetDirections(void);

	//! 방향 벡터를 정규화한다
	void normalizeDirections(void);

	//! X 축을 기준으로 이동한다
	void moveByRightDirection(float a_fOffset, bool a_bIsLocal = true);

	//! Y 축을 기준으로 이동한다
	void moveByUpDirection(float a_fOffset, bool a_bIsLocal = true);

	//! Z 축을 기준으로 이동한다
	void moveByForwardDirection(float a_fOffset, bool a_bIsLocal = true);

	//! 해당 방향으로 이동한다
	void moveByDirection(const D3DXVECTOR3 &a_rstDirection);

	//! X 축을 기준으로 회전한다
	void rotateByRightDirection(float a_fAngle, bool a_bIsLocal = true);

	//! Y 축을 기준으로 회전한다
	void rotateByUpDirection(float a_fAngle, bool a_bIsLocal = true);

	//! Z 축을 기준으로 회전한다
	void rotateByForwardDirection(float a_fAngle, bool a_bIsLocal = true);

	//! 해당 위치로 회전한다
	void rotateByPosition(const D3DXVECTOR3 &a_rstPosition,
		bool a_bIsShpere = true);

	//! 해당 행렬을 기준으로 회전한다
	void rotateByMatrix(const D3DXMATRIXA16 &a_rstMatrix);

	//! 자식 객체를 추가한다
	void addChildObject(CObject *a_pChildObject);

	//! 자식 객체를 제거한다
	void removeChildObject(CObject *a_pChildObject, bool a_bIsRemove = true);

public:			// 생성자, 소멸자

	//! 생성자
	CObject(void);

	//! 소멸자
	virtual ~CObject(void);

protected:			// protected 함수

	//! 상태를 갱신한다
	virtual void doUpdate(void);

protected:			// protected 변수

	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR3 m_stScale;
	D3DXVECTOR3 m_stRotation;

	D3DXVECTOR3 m_stRightDirection;
	D3DXVECTOR3 m_stUpDirection;
	D3DXVECTOR3 m_stForwardDirection;
	
	D3DXMATRIXA16 m_stOffsetMatrix;
	D3DXMATRIXA16 m_stWorldMatrix;
	D3DXMATRIXA16 m_stFinalWorldMatrix;

	bool m_bIsUpdateEnable = true;
	EObjectType m_eObjectType = EObjectType::NONE;

	CObject *m_pParentObject = nullptr;
	std::vector<CObject *> m_oChildObjectList;
};
