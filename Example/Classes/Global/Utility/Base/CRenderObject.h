#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IRenderable.h"
#include "CObject.h"

class CDebugRenderer;

//! 렌더 객체
class CRenderObject : public CObject, 
	public IRenderable
{
public:

	//! 매개 변수
	struct STParameters
	{
		UINT m_nVertexSize;
		DXGI_FORMAT m_eIndexFormat;

		std::string m_oShaderKey;
		std::string m_oVertexBufferKey;
		std::string m_oIndexBufferKey;
		std::string m_oTransformBufferKey;
		std::string m_oRenderBufferKey;
		std::string m_oInputLayoutKey;
	};

public:			// getter, setter

	//! 경계 상자 리스트를 반환한다
	std::vector<STBoundingBox> & getBoundingBoxList(void);

	//! 경계 구 리스트를 반환한다
	std::vector<STBoundingSphere> & getBoundingSphereList(void);

	//! 최종 객체 상자 리스트를 반환한다
	std::vector<STObjectBox> & getFinalObjectBoxList(void);

	//! 최종 경계 상자 리스트를 반환한다
	std::vector<STBoundingBox> & getFinalBoundingBoxList(void);

	//! 최종 경계 구 리스트를 반환한다
	std::vector<STBoundingSphere> & getFinalBoundingSphereList(void);

	//! 디버그 여부를 변경한다
	void setDebugEnable(bool a_bIsDebugEnable,
		EDebugRenderType a_eDebugRenderType = EDebugRenderType::BOX);

	//! 그리기 여부를 변경한다
	void setRenderEnable(bool a_bIsRenderEnable);

	//! 알파 블렌드 여부를 변경한다
	void setAlphaBlendEnable(bool a_bIsAlphaBlendEnable);

	//! 색상을 변경한다
	void setColor(const D3DXCOLOR &a_rstColor);

	//! 경계 상자 리스트를 변경한다
	void setBoundingBoxList(const std::vector<STBoundingBox> &a_rBoundingBoxList);

	//! 경계 구 리스트를 변경한다
	void setBoundingSphereList(const std::vector<STBoundingSphere> &a_rBoundingSphereList);

public:			// public 함수

	//! 물체를 그린다
	virtual void render(void) override final;

	//! 경계 상자를 추가한다
	void addBoundingBox(const STBoundingBox &a_rstBoundingBox);

	//! 경계 구를 추가한다
	void addBoundingSphere(const STBoundingSphere &a_rstBoundingSphere);

public:			// 생성자, 소멸자

	//! 생성자
	CRenderObject(void);

	//! 생성자
	CRenderObject(const STParameters &a_rstParameters);

	//! 소멸자
	virtual ~CRenderObject(void);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void);

	//! 물체를 그린다
	virtual void doRender(void);

	//! 물체를 그린 후
	virtual void postRender(void);

protected:			// protected 변수

	bool m_bIsDebugEnable = false;
	bool m_bIsRenderEnable = true;
	bool m_bIsAlphaBlendEnable = false;

	D3DXCOLOR m_stColor;
	STParameters m_stParameters;
	CDebugRenderer *m_pDebugRenderer = nullptr;

	std::vector<STBoundingBox> m_oBoundingBoxList;
	std::vector<STBoundingSphere> m_oBoundingSphereList;

	std::vector<STObjectBox> m_oFinalObjectBoxList;
	std::vector<STBoundingBox> m_oFinalBoundingBoxList;
	std::vector<STBoundingSphere> m_oFinalBoundingSphereList;
};
