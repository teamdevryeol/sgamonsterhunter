#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

//! 파티클 발생자
class CParticleEmitter : public IUpdateable
{
public:

	//! 파티클
	struct STParticle
	{
		bool m_bIsActive;

		float m_fActiveTime;
		float m_fMaxActiveTime;
		
		float m_fScale;
		float m_fStartScale;
		float m_fEndScale;

		D3DXCOLOR m_stColor;
		D3DXCOLOR m_stStartColor;
		D3DXCOLOR m_stEndColor;

		D3DXVECTOR3 m_stPosition;
		D3DXVECTOR3 m_stVelocity;

		D3DXVECTOR3 m_stRotation;
		D3DXVECTOR3 m_stAngleVelocity;
	};

	//! 매개 변수
	struct STParameters
	{
		int m_nMaxNumParticles;
		int m_nMaxNumParticlesPerSecond;
	};

public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;

public:			// getter, setter

	//! 파티클 리스트를 반환한다
	std::vector<STParticle> & getParticleList(void);

	//! 생성 여부를 변경한다
	void setEmitEnable(bool a_bIsEmitEnable);

public:			// 생성자

	//! 생성자
	CParticleEmitter(const STParameters &a_rstParameters);

private:			// private 함수

	//! 파티클을 생성한다
	void emitParticle(void);

	//! 파티클을 갱신한다
	void updateParticle(void);

	//! 파티클을 제거한다
	void removeParticle(void);

private:			// private 변수

	bool m_bIsEmitEnable = false;
	float m_fSkipTime = 0.0f;

	STParameters m_stParameters;
	std::vector<STParticle> m_oParticleList;
};
