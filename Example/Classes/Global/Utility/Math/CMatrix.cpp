#include "CMatrix.h"

CMatrix::CMatrix(void)
{
	ZeroMemory(m_afValues, sizeof(m_afValues));
}

float & CMatrix::operator()(int a_nRow, int a_nCol)
{
	return m_afValues[a_nRow][a_nCol];
}

float CMatrix::operator()(int a_nRow, int a_nCol) const
{
	return const_cast<CMatrix &>(*this)(a_nRow, a_nCol);
}

CMatrix CMatrix::getTransposeMatrix(void)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			oMatrix(j, i) = (*this)(i, j);
		}
	}

	return oMatrix;
}

CMatrix CMatrix::getAddMatrix(const CMatrix & a_rMatrix)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			oMatrix(i, j) = (*this)(i, j) + a_rMatrix(i, j);
		}
	}

	return oMatrix;
}

CMatrix CMatrix::getSubMatrix(const CMatrix & a_rMatrix)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			oMatrix(i, j) = (*this)(i, j) - a_rMatrix(i, j);
		}
	}

	return oMatrix;
}

CMatrix CMatrix::getMultiplyMatrix(const CMatrix & a_rMatrix)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			for (int k = 0; k < MATRIX_SIZE; ++k) {
				oMatrix(i, j) += (*this)(i, k) * a_rMatrix(k, j);
			}
		}
	}

	return oMatrix;
}

CMatrix CMatrix::getMultiplyScalar(float a_fScalar)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			oMatrix(i, j) = (*this)(i, j) * a_fScalar;
		}
	}

	return oMatrix;
}

CMatrix CMatrix::getIdentityMatrix(void)
{
	CMatrix oMatrix;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		oMatrix(i, i) = 1;
	}

	return oMatrix;
}

CMatrix CMatrix::getTranslateMatrix(float a_fX, float a_fY, float a_fZ)
{
	CMatrix oMatrix = CMatrix::getIdentityMatrix();
	oMatrix(3, 0) = a_fX;
	oMatrix(3, 1) = a_fY;
	oMatrix(3, 2) = a_fZ;

	return oMatrix;
}

CMatrix CMatrix::getScaleMatrix(float a_fX, float a_fY, float a_fZ)
{
	CMatrix oMatrix = CMatrix::getIdentityMatrix();
	oMatrix(0, 0) = a_fX;
	oMatrix(1, 1) = a_fY;
	oMatrix(2, 2) = a_fZ;

	return oMatrix;
}

CMatrix CMatrix::getRotateMatrix(float a_fX, float a_fY, float a_fZ)
{
	CMatrix oPitch = CMatrix::getIdentityMatrix();
	oPitch(1, 1) = cosf(D3DXToRadian(a_fX));
	oPitch(1, 2) = sinf(D3DXToRadian(a_fX));
	oPitch(2, 1) = -sinf(D3DXToRadian(a_fX));
	oPitch(2, 2) = cosf(D3DXToRadian(a_fX));

	CMatrix oYaw = CMatrix::getIdentityMatrix();
	oYaw(0, 0) = cosf(D3DXToRadian(a_fY));
	oYaw(0, 2) = -sinf(D3DXToRadian(a_fY));
	oYaw(2, 0) = sinf(D3DXToRadian(a_fY));
	oYaw(2, 2) = cosf(D3DXToRadian(a_fY));

	CMatrix oRoll = CMatrix::getIdentityMatrix();
	oRoll(0, 0) = cosf(D3DXToRadian(a_fZ));
	oRoll(0, 1) = sinf(D3DXToRadian(a_fZ));
	oRoll(1, 0) = -sinf(D3DXToRadian(a_fZ));
	oRoll(1, 1) = cosf(D3DXToRadian(a_fZ));

	CMatrix oMatrix = oYaw.getMultiplyMatrix(oPitch);
	return oMatrix.getMultiplyMatrix(oRoll);
}
