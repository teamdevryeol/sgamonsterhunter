#include "CVector.h"
#include "CMatrix.h"

CVector::CVector(float a_fX, float a_fY, float a_fZ, float a_fW)
:
m_fX(a_fX),
m_fY(a_fY),
m_fZ(a_fZ),
m_fW(a_fW)
{
	// Do Nothing
}

float CVector::getLength(void)
{
	return sqrtf(powf(m_fX, 2.0f) + 
		powf(m_fY, 2.0f) + powf(m_fZ, 2.0f));
}

CVector CVector::getNormalizeVector(void)
{
	float fLength = this->getLength();

	return CVector(m_fX / fLength,
		m_fY / fLength, m_fZ / fLength, m_fW);
}

CVector CVector::getAddVector(const CVector & a_rVector)
{
	return CVector(m_fX + a_rVector.m_fX,
		m_fY + a_rVector.m_fY, m_fZ + a_rVector.m_fZ, m_fW);
}

CVector CVector::getSubVector(const CVector & a_rVector)
{
	return CVector(m_fX - a_rVector.m_fX,
		m_fY - a_rVector.m_fY, m_fZ - a_rVector.m_fZ, m_fW);
}

CVector CVector::getMultiplyScalar(float a_fScalar)
{
	return CVector(m_fX * a_fScalar,
		m_fY * a_fScalar, m_fZ * a_fScalar, m_fW);
}

float CVector::getDotVector(const CVector & a_rVector)
{
	return (m_fX * a_rVector.m_fX) +
		(m_fY * a_rVector.m_fY) + (m_fZ * a_rVector.m_fZ);
}

CVector CVector::getCrossVector(const CVector & a_rVector)
{
	return CVector((m_fY * a_rVector.m_fZ) - (m_fZ * a_rVector.m_fY),
		(m_fZ * a_rVector.m_fX) - (m_fX * a_rVector.m_fZ),
		(m_fX * a_rVector.m_fY) - (m_fY * a_rVector.m_fX), m_fW);
}

CVector CVector::getTransformationVector(const CMatrix & a_rMatrix)
{
	float fDotValueA = (m_fX * a_rMatrix(0, 0)) + (m_fY * a_rMatrix(1, 0)) + (m_fZ * a_rMatrix(2, 0)) + (m_fW * a_rMatrix(3, 0));
	float fDotValueB = (m_fX * a_rMatrix(0, 1)) + (m_fY * a_rMatrix(1, 1)) + (m_fZ * a_rMatrix(2, 1)) + (m_fW * a_rMatrix(3, 1));
	float fDotValueC = (m_fX * a_rMatrix(0, 2)) + (m_fY * a_rMatrix(1, 2)) + (m_fZ * a_rMatrix(2, 2)) + (m_fW * a_rMatrix(3, 2));
	float fDotValueD = (m_fX * a_rMatrix(0, 3)) + (m_fY * a_rMatrix(1, 3)) + (m_fZ * a_rMatrix(2, 3)) + (m_fW * a_rMatrix(3, 3));

	return CVector(fDotValueA, fDotValueB, fDotValueC, fDotValueD);
}
