#pragma once

#include "../../Define/KGlobalDefine.h"

//! 행렬
class CMatrix
{
public:

	enum
	{
		MATRIX_SIZE = 4
	};

public:			// operator

	//! operator ()
	float & operator()(int a_nRow, int a_nCol);

	//! operator ()
	float operator()(int a_nRow, int a_nCol) const;

public:			// getter

	//! 전치 행렬을 반환한다
	CMatrix getTransposeMatrix(void);

	//! 행렬의 덧셈을 반환한다
	CMatrix getAddMatrix(const CMatrix &a_rMatrix);

	//! 행렬의 뺄셈을 반환한다
	CMatrix getSubMatrix(const CMatrix &a_rMatrix);

	//! 행렬의 곱셈을 반환한다
	CMatrix getMultiplyMatrix(const CMatrix &a_rMatrix);

	//! 행렬의 스칼라 곱셈을 반환한다
	CMatrix getMultiplyScalar(float a_fScalar);

	//! 단위 행렬을 반환한다
	static CMatrix getIdentityMatrix(void);

	/*
	이동 행렬 요소
	:
	1행 :  1  0  0  0
	2행 :  0  1  0  0
	3행 :  0  0  1  0
	4행 : pX pY pZ  1
	*/
	//! 이동 행렬을 반환한다
	static CMatrix getTranslateMatrix(float a_fX, float a_fY, float a_fZ);

	/*
	척도 행렬 요소
	:
	1행 : sX  0  0  0
	2행 :  0 sY  0  0
	3행 :  0  0 sZ  0
	4행 :  0  0  0  1
	*/
	//! 척도 행렬을 반환한다
	static CMatrix getScaleMatrix(float a_fX, float a_fY, float a_fZ);

	/*
	일반적인 오일러 회전 행렬 결합 순서 (Y * P * R)

	X 축 기준 회전 행렬 요소 (Pitch)
	:
	1행 : 1     0    0  0
	2행 : 0  cosΘ sinΘ  0
	3행 : 0 -sinΘ cosΘ  0
	4행 : 0     0    0  1

	Y 축 기준 회전 행렬 요소 (Yaw)
	:
	1행 : cosΘ  0 -sinΘ  0
	2행 : 0     1     0  0
	3행 : sinΘ  0  cosΘ  0
	4행 : 0     0     0  1

	Z 축 기준 회전 행렬 요소 (Roll)
	:
	1행 :  cosΘ sinΘ  0  0
	2행 : -sinΘ cosΘ  0  0
	3행 :     0    0  1  0
	4행 :     0    0  0  1
	*/
	//! 회전 행렬을 반환한다
	static CMatrix getRotateMatrix(float a_fX, float a_fY, float a_fZ);

public:			// 생성자

	//! 생성자
	CMatrix(void);

private:			// private 변수

	float m_afValues[MATRIX_SIZE][MATRIX_SIZE];
};
