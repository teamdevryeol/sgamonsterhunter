#include "GlobalFunction.h"
#include "../Utility/Base/CDirect3DApplication.h"
#include "../Utility/Manager/CDeviceManager.h"
#include "../Utility/Manager/CResourceManager.h"
#include "../Utility/Object/CCamera.h"
#include "../Utility/Render/CMesh.h"

std::vector<D3DVERTEXELEMENT9> InputLayoutToDeclaration(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	std::unordered_map<std::string, D3DVERTEXELEMENT9> oDeclarationList{
		decltype(oDeclarationList)::value_type("POSITION", VERTEX_ELEMENT(D3DDECLUSAGE_POSITION, 0, D3DDECLTYPE_FLOAT3, 0)),
		decltype(oDeclarationList)::value_type("NORMAL", VERTEX_ELEMENT(D3DDECLUSAGE_NORMAL, 0, D3DDECLTYPE_FLOAT3, 0)),
		decltype(oDeclarationList)::value_type("BINORMAL", VERTEX_ELEMENT(D3DDECLUSAGE_BINORMAL, 0, D3DDECLTYPE_FLOAT3, 0)),
		decltype(oDeclarationList)::value_type("TANGENT", VERTEX_ELEMENT(D3DDECLUSAGE_TANGENT, 0, D3DDECLTYPE_FLOAT3, 0)),
		decltype(oDeclarationList)::value_type("BLENDWEIGHT", VERTEX_ELEMENT(D3DDECLUSAGE_BLENDWEIGHT, 0, D3DDECLTYPE_FLOAT4, 0)),
		decltype(oDeclarationList)::value_type("TEXCOORD", VERTEX_ELEMENT(D3DDECLUSAGE_TEXCOORD, 0, D3DDECLTYPE_FLOAT2, 0)),
		decltype(oDeclarationList)::value_type("COLOR", VERTEX_ELEMENT(D3DDECLUSAGE_COLOR, 0, D3DDECLTYPE_FLOAT4, 0))
	};

	int nOffset = 0;
	std::vector<D3DVERTEXELEMENT9> oElementList;

	for (int i = 0; i < a_rElementDescList.size(); ++i) {
		auto stElement = oDeclarationList[a_rElementDescList[i].SemanticName];
		stElement.UsageIndex = a_rElementDescList[i].SemanticIndex;
		stElement.Offset = nOffset;

		switch (a_rElementDescList[i].Format) {
		case DXGI_FORMAT_R32_FLOAT: nOffset += 4; break;
		case DXGI_FORMAT_R32G32_FLOAT: nOffset += 8; break;
		case DXGI_FORMAT_R32G32B32_FLOAT: nOffset += 12; break;
		case DXGI_FORMAT_R32G32B32A32_FLOAT: nOffset += 16; break;
		}

		oElementList.push_back(stElement);
	}

	oElementList.push_back(D3DDECL_END());
	return oElementList;
}

std::vector<D3D11_INPUT_ELEMENT_DESC> DeclarationToInputLayout(const std::vector<D3DVERTEXELEMENT9>& a_rElementList)
{
	std::unordered_map<BYTE, D3D11_INPUT_ELEMENT_DESC> oInputLayoutList{
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_POSITION, INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_NORMAL, INPUT_ELEMENT_DESC("NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_BINORMAL, INPUT_ELEMENT_DESC("BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_TANGENT, INPUT_ELEMENT_DESC("TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_BLENDWEIGHT, INPUT_ELEMENT_DESC("BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_TEXCOORD, INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)),
		decltype(oInputLayoutList)::value_type(D3DDECLUSAGE_COLOR, INPUT_ELEMENT_DESC("COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT))
	};

	std::vector<D3D11_INPUT_ELEMENT_DESC> oElementDescList;

	for (int i = 0; i < a_rElementList.size(); ++i) {
		auto stElementDesc = oInputLayoutList[a_rElementList[i].Usage];
		stElementDesc.SemanticIndex = a_rElementList[i].UsageIndex;

		oElementDescList.push_back(stElementDesc);
	}

	return oElementDescList;
}

std::string GetBasePath(const std::string & a_rFilepath)
{
	std::string oBasePath = "";

	// 파일 경로가 포함되어있을 경우
	if (a_rFilepath.rfind("/") != std::string::npos) {
		oBasePath = a_rFilepath.substr(0, a_rFilepath.rfind("/") + 1);
	}

	return oBasePath;
}

std::string GetReplaceString(const std::string & a_rString, const std::string & a_rFindString, const std::string & a_rReplaceString)
{
	std::string oString = a_rString;

	while (oString.find(a_rFindString) != std::string::npos) {
		oString.replace(oString.find(a_rFindString), 
			a_rFindString.length(), a_rReplaceString);
	}

	return oString;
}

std::string GetFormatString(const char * a_pszFormat, ...)
{
	va_list vaList = nullptr;
	va_start(vaList, a_pszFormat);

	char szString[MAX_PATH] = "";
	vsprintf(szString, a_pszFormat, vaList);

	va_end(vaList);
	return szString;
}

STMaterial GetMaterial(const std::string & a_rFilepath)
{
	// 재질을 설정한다
	// {
	STMaterial stMaterial = { 0 };
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pXMaterial = nullptr;

	D3DXLoadMeshFromXA(a_rFilepath.c_str(),
		D3DXMESH_MANAGED,
		GET_9VERSION_DEVICE(),
		NULL,
		&pXMaterial,
		NULL,
		&stMaterial.m_nNumMaterials,
		&pMesh);

	std::string oBasePath = GetBasePath(a_rFilepath);

	for (int i = 0; i < stMaterial.m_nNumMaterials; ++i) {
		LPD3DXMATERIAL pstXMaterials = (LPD3DXMATERIAL)pXMaterial->GetBufferPointer();

		ID3D11ShaderResourceView *pNormalMap = nullptr;
		ID3D11ShaderResourceView *pDiffuseMap = nullptr;

		// 텍스처 정보가 존재 할 경우
		if (pstXMaterials[i].pTextureFilename != nullptr) {
			std::string oNormalMapFilepath = GetFormatString("%s%s", oBasePath.c_str(),
				GetReplaceString(pstXMaterials[i].pTextureFilename, "_D", "_N").c_str());

			std::string oDiffuseMapFilepath = GetFormatString("%s%s", oBasePath.c_str(),
				pstXMaterials[i].pTextureFilename);

			pNormalMap = GET_SHADER_RESOURCE_VIEW(oNormalMapFilepath);
			pDiffuseMap = GET_SHADER_RESOURCE_VIEW(oDiffuseMapFilepath);
		}

		stMaterial.m_oNormalMapList.push_back(pNormalMap);
		stMaterial.m_oDiffuseMapList.push_back(pDiffuseMap);
	}
	// }

	SAFE_RELEASE(pXMaterial);
	return stMaterial;
}

STWaveSound GetWaveSound(const std::string & a_rFilepath)
{
	STWaveSound stWaveSound;
	ZeroMemory(&stWaveSound, sizeof(stWaveSound));

	// 사운드 정보를 설정한다
	// {
	HMMIO hWaveFile = mmioOpenA((char *)a_rFilepath.c_str(), NULL, MMIO_READ);

	if (hWaveFile != nullptr) {
		MMCKINFO stChunkInfo;
		ZeroMemory(&stChunkInfo, sizeof(stChunkInfo));

		stChunkInfo.fccType = mmioFOURCC('W', 'A', 'V', 'E');
		mmioDescend(hWaveFile, &stChunkInfo, NULL, MMIO_FINDRIFF);

		MMCKINFO stSubChunkInfo;
		ZeroMemory(&stSubChunkInfo, sizeof(stSubChunkInfo));

		stSubChunkInfo.ckid = mmioFOURCC('f', 'm', 't', ' ');
		mmioDescend(hWaveFile, &stSubChunkInfo, &stChunkInfo, MMIO_FINDCHUNK);

		mmioRead(hWaveFile, (char *)&stWaveSound.m_stWaveFormat, sizeof(stWaveSound.m_stWaveFormat));
		mmioAscend(hWaveFile, &stSubChunkInfo, 0);

		stSubChunkInfo.ckid = mmioFOURCC('d', 'a', 't', 'a');
		mmioDescend(hWaveFile, &stSubChunkInfo, &stChunkInfo, MMIO_FINDCHUNK);

		stWaveSound.m_nNumBytes = stSubChunkInfo.cksize;
		stWaveSound.m_pnBytes = (BYTE *)malloc(sizeof(BYTE) * stSubChunkInfo.cksize);

		mmioRead(hWaveFile, (char *)stWaveSound.m_pnBytes, stSubChunkInfo.cksize);
		mmioClose(hWaveFile, 0);
	}
	// }

	return stWaveSound;
}

STBoundingBox GetBoundingBox(CMesh * a_pMesh)
{
	// 경계 상자 정보를 설정한다
	// {
	STBoundingBox stBoundingBox;
	ZeroMemory(&stBoundingBox, sizeof(stBoundingBox));

	auto pMesh = a_pMesh->getMesh();
	D3DXVECTOR3 *pstVertices = nullptr;

	if (SUCCEEDED(pMesh->LockVertexBuffer(0, (void **)&pstVertices))) {
		D3DXComputeBoundingBox(pstVertices,
			pMesh->GetNumVertices(),
			pMesh->GetNumBytesPerVertex(),
			&stBoundingBox.m_stMin,
			&stBoundingBox.m_stMax);

		pMesh->UnlockVertexBuffer();
	}
	// }

	return stBoundingBox;
}

STBoundingSphere GetBoundingSphere(CMesh * a_pMesh)
{
	// 경계 구 정보를 설정한다
	// {
	STBoundingSphere stBoundingSphere;
	ZeroMemory(&stBoundingSphere, sizeof(stBoundingSphere));

	auto pMesh = a_pMesh->getMesh();
	D3DXVECTOR3 *pstVertices = nullptr;

	if (SUCCEEDED(pMesh->LockVertexBuffer(0, (void **)&pstVertices))) {
		D3DXComputeBoundingSphere(pstVertices,
			pMesh->GetNumVertices(),
			pMesh->GetNumBytesPerVertex(),
			&stBoundingSphere.m_stCenter,
			&stBoundingSphere.m_fRadius);

		pMesh->UnlockVertexBuffer();
	}
	// }

	return stBoundingSphere;
}

LPD3DXMESH GetCloneMesh(LPD3DXMESH a_pOriginMesh, LPD3DXBUFFER a_pAdjacency, const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// 사본 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	auto oElementList = InputLayoutToDeclaration(a_rElementDescList);

	a_pOriginMesh->CloneMesh(a_pOriginMesh->GetOptions(),
		&oElementList[0],
		GET_9VERSION_DEVICE(),
		&pMesh);

	DWORD *pnAdjacency = (a_pAdjacency == nullptr) ? nullptr 
		: (DWORD *)a_pAdjacency->GetBufferPointer();

	/*
	D3DXMESHOPT_ATTRSORT 옵션으로 메시를 최적화시켜주어야지만 내부적으로
	속성 테이블이 생성된다.
	*/
	pMesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT,
		pnAdjacency, pnAdjacency, NULL, NULL);
	// }

	// 법선, 접선/종법선을 설정한다
	if (pnAdjacency != nullptr) {
		D3DXComputeNormals(pMesh, pnAdjacency);
		D3DXComputeTangent(pMesh, 0, 0, 0, TRUE, pnAdjacency);
	}

	// 정점 정보를 설정한다
	// {
	auto oIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_COLOR;
	});

	if (oIterator != oElementList.end()) {
		BYTE *pnVertices = nullptr;

		if (SUCCEEDED(pMesh->LockVertexBuffer(0, (void **)&pnVertices))) {
			for (int i = 0; i < pMesh->GetNumVertices(); ++i) {
				BYTE *pnColor = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oIterator->Offset);
				*reinterpret_cast<D3DXCOLOR *>(pnColor) = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
			}

			pMesh->UnlockVertexBuffer();
		}
	}
	// }

	return pMesh;
}

bool IntersectTriangle(const D3DXVECTOR3 & orig, const D3DXVECTOR3 & dir, D3DXVECTOR3 & v0, D3DXVECTOR3 & v1, D3DXVECTOR3 & v2, FLOAT * t, FLOAT * u, FLOAT * v)
{
	// Find vectors for two edges sharing vert0
	D3DXVECTOR3 edge1 = v1 - v0;
	D3DXVECTOR3 edge2 = v2 - v0;

	// Begin calculating determinant - also used to calculate U parameter
	D3DXVECTOR3 pvec;
	D3DXVec3Cross(&pvec, &dir, &edge2);

	// If determinant is near zero, ray lies in plane of triangle
	FLOAT det = D3DXVec3Dot(&edge1, &pvec);

	D3DXVECTOR3 tvec;
	if (det > 0)
	{
		tvec = orig - v0;
	}
	else
	{
		tvec = v0 - orig;
		det = -det;
	}

	if (det < 0.0001f)
		return FALSE;

	// Calculate U parameter and test bounds
	*u = D3DXVec3Dot(&tvec, &pvec);
	if (*u < 0.0f || *u > det)
		return FALSE;

	// Prepare to test V parameter
	D3DXVECTOR3 qvec;
	D3DXVec3Cross(&qvec, &tvec, &edge1);

	// Calculate V parameter and test bounds
	*v = D3DXVec3Dot(&dir, &qvec);
	if (*v < 0.0f || *u + *v > det)
		return FALSE;

	// Calculate t, scale parameters, ray intersects triangle
	*t = D3DXVec3Dot(&edge2, &qvec);
	FLOAT fInvDet = 1.0f / det;
	*t *= fInvDet;
	*u *= fInvDet;
	*v *= fInvDet;

	return TRUE;
}

bool IsIntersectBox(const STBoundingBox & a_rstLhs, const STBoundingBox & a_rstRhs)
{
	// X 축을 검사한다
	if (a_rstLhs.m_stMin.x > a_rstRhs.m_stMax.x ||
		a_rstLhs.m_stMax.x < a_rstRhs.m_stMin.x)
	{
		return false;
	}

	// Y 축을 검사한다
	if (a_rstLhs.m_stMin.y > a_rstRhs.m_stMax.y ||
		a_rstLhs.m_stMax.y < a_rstRhs.m_stMin.y)
	{
		return false;
	}

	// Z 축을 검사한다
	if (a_rstLhs.m_stMin.z > a_rstRhs.m_stMax.z ||
		a_rstLhs.m_stMax.z < a_rstRhs.m_stMin.z)
	{
		return false;
	}

	return true;
}

bool IsIntersectObjectBox(const STObjectBox & a_rstLhs, const STObjectBox & a_rstRhs)
{
	float fCutoffValue = 0.99999f;
	float afDeltaDotValues[NUM_DIRECTIONS] = { 0 };

	float afCosValues[NUM_DIRECTIONS][NUM_DIRECTIONS] = { 0 };
	float afAbsCosValues[NUM_DIRECTIONS][NUM_DIRECTIONS] = { 0 };

	bool bIsParallel = false;
	D3DXVECTOR3 stDelta = a_rstRhs.m_stCenter - a_rstLhs.m_stCenter;

	for (int i = 0; i < NUM_DIRECTIONS; ++i) {
		for (int j = 0; j < NUM_DIRECTIONS; ++j) {
			afCosValues[i][j] = D3DXVec3Dot(&a_rstLhs.m_astDirections[i], &a_rstRhs.m_astDirections[j]);
			afAbsCosValues[i][j] = fabsf(afCosValues[i][j]);

			// 평행 축이 존재 할 경
			if (afAbsCosValues[i][j] > fCutoffValue) {
				bIsParallel = true;
			}
		}

		afDeltaDotValues[i] = D3DXVec3Dot(&stDelta, &a_rstLhs.m_astDirections[i]);
	}

	// OBB A 를 기준으로 검사한다
	// {
	// X 축을 기준으로 검사한다
	float fTotalDotValue = fabsf(afDeltaDotValues[0]);
	float fDotValueA = a_rstLhs.m_afHalfLengths[0];

	float fDotValueB = (afAbsCosValues[0][0] * a_rstRhs.m_afHalfLengths[0]) +
		(afAbsCosValues[0][1] * a_rstRhs.m_afHalfLengths[1]) +
		(afAbsCosValues[0][2] * a_rstRhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Y 축을 기준으로 검사한다
	fTotalDotValue = fabsf(afDeltaDotValues[1]);
	fDotValueA = a_rstLhs.m_afHalfLengths[1];

	fDotValueB = (afAbsCosValues[1][0] * a_rstRhs.m_afHalfLengths[0]) +
		(afAbsCosValues[1][1] * a_rstRhs.m_afHalfLengths[1]) +
		(afAbsCosValues[1][2] * a_rstRhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Z 축을 기준으로 검사한다
	fTotalDotValue = fabsf(afDeltaDotValues[2]);
	fDotValueA = a_rstLhs.m_afHalfLengths[2];

	fDotValueB = (afAbsCosValues[2][0] * a_rstRhs.m_afHalfLengths[0]) +
		(afAbsCosValues[2][1] * a_rstRhs.m_afHalfLengths[1]) +
		(afAbsCosValues[2][2] * a_rstRhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}
	// }

	// OBB B 를 기준으로 검사한다
	// {
	// X 축을 기준으로 검사한다
	fTotalDotValue = fabsf(D3DXVec3Dot(&stDelta, &a_rstRhs.m_astDirections[0]));
	fDotValueB = a_rstRhs.m_afHalfLengths[0];

	fDotValueA = (afAbsCosValues[0][0] * a_rstLhs.m_afHalfLengths[0]) +
		(afAbsCosValues[1][0] * a_rstLhs.m_afHalfLengths[1]) +
		(afAbsCosValues[2][0] * a_rstLhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Y 축을 기준으로 검사한다
	fTotalDotValue = fabsf(D3DXVec3Dot(&stDelta, &a_rstRhs.m_astDirections[1]));
	fDotValueB = a_rstRhs.m_afHalfLengths[1];

	fDotValueA = (afAbsCosValues[0][1] * a_rstLhs.m_afHalfLengths[0]) +
		(afAbsCosValues[1][1] * a_rstLhs.m_afHalfLengths[1]) +
		(afAbsCosValues[2][1] * a_rstLhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Z 축을 기준으로 검사한다
	fTotalDotValue = fabsf(D3DXVec3Dot(&stDelta, &a_rstRhs.m_astDirections[2]));
	fDotValueB = a_rstRhs.m_afHalfLengths[2];

	fDotValueA = (afAbsCosValues[0][2] * a_rstLhs.m_afHalfLengths[0]) +
		(afAbsCosValues[1][2] * a_rstLhs.m_afHalfLengths[1]) +
		(afAbsCosValues[2][2] * a_rstLhs.m_afHalfLengths[2]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}
	// }

	if (bIsParallel) {
		return true;
	}

	// OBB A x OBB B 를 기준으로 검사한다
	// {
	// X x X 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[2] * afCosValues[1][0]) -
		(afDeltaDotValues[1] * afCosValues[2][0]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[2][0]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[1][0]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[0][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[0][1]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// X x Y 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[2] * afCosValues[1][1]) -
		(afDeltaDotValues[1] * afCosValues[2][1]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[2][1]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[1][1]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[0] * afAbsCosValues[0][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[0][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// X x Z 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[2] * afCosValues[1][2]) -
		(afDeltaDotValues[1] * afCosValues[2][2]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[2][2]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[1][2]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[0] * afAbsCosValues[0][1]) +
		(a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[0][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Y x X 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[0] * afCosValues[2][0]) -
		(afDeltaDotValues[2] * afCosValues[0][0]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[2][0]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[0][0]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[1][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[1][1]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Y x Y 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[0] * afCosValues[2][1]) -
		(afDeltaDotValues[2] * afCosValues[0][1]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[2][1]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[0][1]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[1][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[1][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Y x Z 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[0] * afCosValues[2][2]) -
		(afDeltaDotValues[2] * afCosValues[0][2]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[2][2]) +
		(a_rstLhs.m_afHalfLengths[2] * afAbsCosValues[0][2]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[0] * afAbsCosValues[1][1]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[1][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Z x X 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[1] * afCosValues[0][0]) -
		(afDeltaDotValues[0] * afCosValues[1][0]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[1][0]) +
		(a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[0][0]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[2][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[2][1]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Z x Y 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[1] * afCosValues[0][1]) -
		(afDeltaDotValues[0] * afCosValues[1][1]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[1][1]) +
		(a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[0][1]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[0] * afAbsCosValues[2][2]) +
		(a_rstRhs.m_afHalfLengths[2] * afAbsCosValues[2][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}

	// Z x Z 축을 기준으로 검사한다
	fTotalDotValue = fabsf((afDeltaDotValues[1] * afCosValues[0][2]) -
		(afDeltaDotValues[0] * afCosValues[1][2]));

	fDotValueA = (a_rstLhs.m_afHalfLengths[0] * afAbsCosValues[1][2]) +
		(a_rstLhs.m_afHalfLengths[1] * afAbsCosValues[0][2]);

	fDotValueB = (a_rstRhs.m_afHalfLengths[0] * afAbsCosValues[2][1]) +
		(a_rstRhs.m_afHalfLengths[1] * afAbsCosValues[2][0]);

	if (fTotalDotValue > fDotValueA + fDotValueB) {
		return false;
	}
	// }

	return true;
}

bool IsIntersectSphere(const STBoundingSphere & a_rstLhs, const STBoundingSphere & a_rstRhs)
{
	D3DXVECTOR3 stDelta = a_rstLhs.m_stCenter - a_rstRhs.m_stCenter;
	return D3DXVec3Length(&stDelta) <= (a_rstLhs.m_fRadius + a_rstRhs.m_fRadius);
}

bool IsIntersectBoxRay(const STBoundingBox & a_rstLhs, const STRay & a_rstRay)
{
	float fMaxS = -FLT_MAX;
	float fMinT = FLT_MAX;

	// X 축을 검사한다
	if (fabsf(a_rstRay.m_stDirection.x) <= FLT_EPSILON) {
		if (a_rstRay.m_stOrigin.x < a_rstLhs.m_stMin.x ||
			a_rstRay.m_stOrigin.x > a_rstLhs.m_stMax.x)
		{
			return false;
		}
	}
	else {
		float fS = (a_rstLhs.m_stMin.x - a_rstRay.m_stOrigin.x) / a_rstRay.m_stDirection.x;
		float fT = (a_rstLhs.m_stMax.x - a_rstRay.m_stOrigin.x) / a_rstRay.m_stDirection.x;

		if (fT < fS) {
			std::swap(fS, fT);
		}

		if (fS > fMaxS) {
			fMaxS = fS;
		}

		if (fT < fMinT) {
			fMinT = fT;
		}

		if (fMinT < 0.0f || fMaxS > fMinT) {
			return false;
		}
	}

	// Y 축을 검사한다
	if (fabsf(a_rstRay.m_stDirection.y) <= FLT_EPSILON) {
		if (a_rstRay.m_stOrigin.y < a_rstLhs.m_stMin.y ||
			a_rstRay.m_stOrigin.y > a_rstLhs.m_stMax.y)
		{
			return false;
		}
	}
	else {
		float fS = (a_rstLhs.m_stMin.y - a_rstRay.m_stOrigin.y) / a_rstRay.m_stDirection.y;
		float fT = (a_rstLhs.m_stMax.y - a_rstRay.m_stOrigin.y) / a_rstRay.m_stDirection.y;

		if (fT < fS) {
			std::swap(fS, fT);
		}

		if (fS > fMaxS) {
			fMaxS = fS;
		}

		if (fT < fMinT) {
			fMinT = fT;
		}

		if (fMinT < 0.0f || fMaxS > fMinT) {
			return false;
		}
	}

	// Z 축을 검사한다
	if (fabsf(a_rstRay.m_stDirection.z) <= FLT_EPSILON) {
		if (a_rstRay.m_stOrigin.z < a_rstLhs.m_stMin.z ||
			a_rstRay.m_stOrigin.z > a_rstLhs.m_stMax.z)
		{
			return false;
		}
	}
	else {
		float fS = (a_rstLhs.m_stMin.z - a_rstRay.m_stOrigin.z) / a_rstRay.m_stDirection.z;
		float fT = (a_rstLhs.m_stMax.z - a_rstRay.m_stOrigin.z) / a_rstRay.m_stDirection.z;

		if (fT < fS) {
			std::swap(fS, fT);
		}

		if (fS > fMaxS) {
			fMaxS = fS;
		}

		if (fT < fMinT) {
			fMinT = fT;
		}

		if (fMinT < 0.0f || fMaxS > fMinT) {
			return false;
		}
	}

	return true;
}

bool IsIntersectSphereRay(const STBoundingSphere & a_rstLhs, const STRay & a_rstRay)
{
	D3DXVECTOR3 stDelta = a_rstLhs.m_stCenter - a_rstRay.m_stOrigin;
	
	D3DXVECTOR3 stUnitDelta = stDelta;
	D3DXVECTOR3 stDirection = a_rstRay.m_stDirection;

	// 방향 벡터를 정규화한다
	D3DXVec3Normalize(&stUnitDelta, &stUnitDelta);
	D3DXVec3Normalize(&stDirection, &stDirection);

	// 둔각 일 경우
	if (D3DXVec3Dot(&stUnitDelta, &stDirection) < 0.0f) {
		return D3DXVec3Length(&stDelta) <= a_rstLhs.m_fRadius;
	}

	float fDotValue = D3DXVec3Dot(&stDelta, &stDirection);

	D3DXVECTOR3 stComparePoint = a_rstRay.m_stOrigin + (stDirection * fDotValue);
	D3DXVECTOR3 stCompareDelta = stComparePoint - a_rstLhs.m_stCenter;

	return D3DXVec3Length(&stCompareDelta) <= a_rstLhs.m_fRadius;
}

float CalculateDeltaTime(const std::chrono::system_clock::time_point & a_rStartTimePoint, const std::chrono::system_clock::time_point & a_rEndTimePoint)
{
	std::chrono::duration<float> oDeltaTime(a_rEndTimePoint - a_rStartTimePoint);
	return oDeltaTime.count();
}

int CreateIntRandomValue(int a_nMin, int a_nMax)
{
	std::random_device oRandomDevice;
	std::uniform_int_distribution<int> oUniformRandom(a_nMin, a_nMax);

	return oUniformRandom(oRandomDevice);
}

float CreateFloatRandomValue(float a_fMin, float a_fMax)
{
	std::random_device oRandomDevice;
	std::uniform_real_distribution<float> oUniformRandom(a_fMin, a_fMax);

	return oUniformRandom(oRandomDevice);
}

std::string ToString(const D3DXVECTOR3 & stValue)
{
	return GetFormatString("%f %f %f ", stValue.x, stValue.y, stValue.z);;
}

STRay CreateRay(const POINT & a_rstPoint)
{
	// 광선을 생성한다
	// {
	STRay stRay;
	ZeroMemory(&stRay, sizeof(stRay));

	UINT nNumViewports = 1;
	D3D11_VIEWPORT stViewport;

	GET_CONTEXT()->RSGetViewports(&nNumViewports, &stViewport);

	float fDirectionX = ((a_rstPoint.x * 2.0f) / stViewport.Width) - 1.0f;
	float fDirectionY = ((a_rstPoint.y * -2.0f) / stViewport.Height) + 1.0f;

	auto &rstViewMatrix = GET_VIEW_MATRIX();
	auto &rstProjectionMatrix = GET_PROJECTION_MATRIX();

	stRay.m_stDirection.x = fDirectionX / rstProjectionMatrix(0, 0);
	stRay.m_stDirection.y = fDirectionY / rstProjectionMatrix(1, 1);
	stRay.m_stDirection.z = 1.0f;

	D3DXMATRIXA16 stInverseMatrix;
	D3DXMatrixInverse(&stInverseMatrix, NULL, &rstViewMatrix);

	D3DXVec3TransformCoord(&stRay.m_stOrigin, &stRay.m_stOrigin, &stInverseMatrix);
	D3DXVec3TransformNormal(&stRay.m_stDirection, &stRay.m_stDirection, &stInverseMatrix);
	// }

	D3DXVec3Normalize(&stRay.m_stDirection, &stRay.m_stDirection);
	return stRay;
}



ID3D10Blob * CompileShader(const std::string & a_rFilepath, const std::string & a_rFunctionName, const std::string & a_rShaderVersion)
{
	// 쉐이더를 컴파일한다
	// {
	ID3D10Blob *pShaderCode = nullptr;
	ID3D10Blob *pErrorBuffer = nullptr;

	D3DX11CompileFromFileA(a_rFilepath.c_str(),
		NULL,
		NULL,
		a_rFunctionName.c_str(),
		a_rShaderVersion.c_str(),
		D3D10_SHADER_PACK_MATRIX_ROW_MAJOR,
		0,
		NULL,
		&pShaderCode,
		&pErrorBuffer,
		NULL);

	if (pErrorBuffer != nullptr) {
		char *pszMessage = (char *)pErrorBuffer->GetBufferPointer();
		printf("GlobalFunction.CompileShader : %s\n", pszMessage);

		SAFE_RELEASE(pErrorBuffer);
	}
	// }

	return pShaderCode;
}

CMesh * CreateBoxMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList, float a_fWidth, float a_fHeight, float a_fDepth)
{
	// 박스 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateBox(GET_9VERSION_DEVICE(),
		a_fWidth, a_fHeight, a_fDepth, &pMesh, &pAdjacency);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

CMesh * CreateSphereMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList, float a_fRadius)
{
	// 구 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateSphere(GET_9VERSION_DEVICE(),
		a_fRadius, 15, 15, &pMesh, &pAdjacency);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

CMesh * CreateCylinderMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// 원기둥 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateCylinder(GET_9VERSION_DEVICE(),
		0.5f, 0.5f, 1.0f, 15, 15, &pMesh, &pAdjacency);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

CMesh * CreateTeapotMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// 주전자 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateTeapot(GET_9VERSION_DEVICE(), &pMesh, &pAdjacency);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

CMesh * CreateTorusMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// 도넛 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXCreateTorus(GET_9VERSION_DEVICE(),
		0.25f, 0.5f, 15, 15, &pMesh, &pAdjacency);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

CMesh * CreatePlaneMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// 평면 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	auto oElementList = InputLayoutToDeclaration(a_rElementDescList);

	D3DXCreateMesh(2,
		4, D3DXMESH_MANAGED, &oElementList[0], GET_9VERSION_DEVICE(), &pMesh);
	// }

	// 정점 정보를 설정한다
	// {
	BYTE *pnVertices = nullptr;

	auto oPositionIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_POSITION;
	});

	auto oNormalIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_NORMAL;
	});

	auto oBinormalIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_BINORMAL;
	});

	auto oTangentIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_TANGENT;
	});

	auto oUVIterator = std::find_if(oElementList.begin(),
		oElementList.end(), [=](D3DVERTEXELEMENT9 a_stElement) -> bool
	{
		return a_stElement.Usage == D3DDECLUSAGE_TEXCOORD;
	});

	if (SUCCEEDED(pMesh->LockVertexBuffer(0, (void **)&pnVertices))) {
		D3DXVECTOR3 astPositions[] = {
			D3DXVECTOR3(-0.5f, -0.5f, 0.0f),
			D3DXVECTOR3(-0.5f, 0.5f, 0.0f),
			D3DXVECTOR3(0.5f, 0.5f, 0.0f),
			D3DXVECTOR3(0.5f, -0.5f, 0.0f)
		};

		D3DXVECTOR3 astNormals[] = {
			D3DXVECTOR3(0.0f, 0.0f, -1.0f),
			D3DXVECTOR3(0.0f, 0.0f, -1.0f),
			D3DXVECTOR3(0.0f, 0.0f, -1.0f),
			D3DXVECTOR3(0.0f, 0.0f, -1.0f)
		};

		D3DXVECTOR3 astBinormals[] = {
			D3DXVECTOR3(0.0f, -1.0f, 0.0f),
			D3DXVECTOR3(0.0f, -1.0f, 0.0f),
			D3DXVECTOR3(0.0f, -1.0f, 0.0f),
			D3DXVECTOR3(0.0f, -1.0f, 0.0f)
		};

		D3DXVECTOR3 astTangents[] = {
			D3DXVECTOR3(1.0f, 0.0f, 0.0f),
			D3DXVECTOR3(1.0f, 0.0f, 0.0f),
			D3DXVECTOR3(1.0f, 0.0f, 0.0f),
			D3DXVECTOR3(1.0f, 0.0f, 0.0f)
		};

		D3DXVECTOR2 astUVs[] = {
			D3DXVECTOR2(0.0f, 1.0f),
			D3DXVECTOR2(0.0f, 0.0f),
			D3DXVECTOR2(1.0f, 0.0f),
			D3DXVECTOR2(1.0f, 1.0f)
		};

		for (int i = 0; i < pMesh->GetNumVertices(); ++i) {
			if (oPositionIterator != oElementList.end()) {
				BYTE *pnPosition = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oPositionIterator->Offset);
				*reinterpret_cast<D3DXVECTOR3 *>(pnPosition) = astPositions[i];
			}

			if (oNormalIterator != oElementList.end()) {
				BYTE *pnNormal = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oNormalIterator->Offset);
				*reinterpret_cast<D3DXVECTOR3 *>(pnNormal) = astNormals[i];
			}

			if (oBinormalIterator != oElementList.end()) {
				BYTE *pnBinormal = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oBinormalIterator->Offset);
				*reinterpret_cast<D3DXVECTOR3 *>(pnBinormal) = astBinormals[i];
			}

			if (oTangentIterator != oElementList.end()) {
				BYTE *pnTangent = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oTangentIterator->Offset);
				*reinterpret_cast<D3DXVECTOR3 *>(pnTangent) = astTangents[i];
			}

			if (oUVIterator != oElementList.end()) {
				BYTE *pnUV = pnVertices + ((i * pMesh->GetNumBytesPerVertex()) + oUVIterator->Offset);
				*reinterpret_cast<D3DXVECTOR2 *>(pnUV) = astUVs[i];
			}
		}

		pMesh->UnlockVertexBuffer();
	}
	// }

	// 인덱스 정보를 설정한다
	// {
	WORD *pnIndices = nullptr;

	if (SUCCEEDED(pMesh->LockIndexBuffer(0, (void **)&pnIndices))) {
		pnIndices[0] = 0;	pnIndices[1] = 1;	pnIndices[2] = 2;
		pnIndices[3] = 0;	pnIndices[4] = 2;	pnIndices[5] = 3;

		pMesh->UnlockIndexBuffer();
	}
	// }


	return Create<CMesh>(CMesh::STParameters{
		pMesh, nullptr, a_rElementDescList
	});
}

CMesh * CreateXMesh(const std::string & a_rFilepath, const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList)
{
	// X 메시를 생성한다
	// {
	LPD3DXMESH pMesh = nullptr;
	LPD3DXBUFFER pAdjacency = nullptr;

	D3DXLoadMeshFromXA(a_rFilepath.c_str(),
		D3DXMESH_MANAGED,
		GET_9VERSION_DEVICE(),
		&pAdjacency,
		NULL,
		NULL,
		NULL,
		&pMesh);
	// }

	return Create<CMesh>(CMesh::STParameters{
		pMesh, pAdjacency, a_rElementDescList
	});
}

ID3D11Buffer * CreateBuffer(UINT a_nSize, UINT a_nBindFlags, UINT a_nCPUAccessFlags, D3D11_USAGE a_eUsage)
{
	// 버퍼를 생성한다
	// {
	D3D11_BUFFER_DESC stBufferDesc;
	ZeroMemory(&stBufferDesc, sizeof(stBufferDesc));

	stBufferDesc.ByteWidth = a_nSize;
	stBufferDesc.BindFlags = a_nBindFlags;
	stBufferDesc.Usage = a_eUsage;
	stBufferDesc.CPUAccessFlags = a_nCPUAccessFlags;
	stBufferDesc.MiscFlags = 0;
	stBufferDesc.StructureByteStride = 0;

	ID3D11Buffer *pBuffer = nullptr;
	GET_DEVICE()->CreateBuffer(&stBufferDesc, NULL, &pBuffer);
	// }

	return pBuffer;
}

ID3D11VertexShader * CreateVertexShader(const std::string & a_rFilepath, ID3D10Blob ** a_pOutVertexShaderCode, const std::string & a_rFunctionName, const std::string & a_rShaderVersion)
{
	// 정점 쉐이더를 생성한다
	// {
	*a_pOutVertexShaderCode = CompileShader(a_rFilepath,
		a_rFunctionName, a_rShaderVersion);

	ID3D11VertexShader *pVertexShader = nullptr;

	GET_DEVICE()->CreateVertexShader((*a_pOutVertexShaderCode)->GetBufferPointer(),
		(*a_pOutVertexShaderCode)->GetBufferSize(),
		NULL,
		&pVertexShader);
	// }

	return pVertexShader;
}

ID3D11PixelShader * CreatePixelShader(const std::string & a_rFilepath, ID3D10Blob ** a_pOutPixelShaderCode, const std::string & a_rFunctionName, const std::string & a_rShaderVersion)
{
	// 픽셀 쉐이더를 생성한다
	// {
	*a_pOutPixelShaderCode = CompileShader(a_rFilepath,
		a_rFunctionName, a_rShaderVersion);

	ID3D11PixelShader *pPixelShader = nullptr;

	GET_DEVICE()->CreatePixelShader((*a_pOutPixelShaderCode)->GetBufferPointer(),
		(*a_pOutPixelShaderCode)->GetBufferSize(),
		NULL,
		&pPixelShader);
	// }

	return pPixelShader;
}

ID3D11InputLayout * CreateInputLayout(const std::vector<D3D11_INPUT_ELEMENT_DESC>& a_rElementDescList, ID3D10Blob * a_pVertexShaderCode)
{
	// 입력 레이아웃을 생성한다
	// {
	ID3D11InputLayout *pInputLayout = nullptr;

	GET_DEVICE()->CreateInputLayout(&a_rElementDescList[0],
		a_rElementDescList.size(),
		a_pVertexShaderCode->GetBufferPointer(),
		a_pVertexShaderCode->GetBufferSize(),
		&pInputLayout);
	// }

	return pInputLayout;
}

ID3D11SamplerState * CreateSamplerState(D3D11_FILTER a_eFilter,
	D3D11_TEXTURE_ADDRESS_MODE a_eAddressU,
	D3D11_TEXTURE_ADDRESS_MODE a_eAddressV)
{
	/*
	Filter 종류
	:
	- POINT (텍셀의 소수점을 버려서 해당 정수 파트에 있는 데이터 추출)
	- LINEAR (특정 텍셀의 주위의 색상을 혼합해서 최종 데이터 추출)

	Address Mode 종류
	:
	- CLAMP (텍스처의 가장 마지막에 있는 데이터를 추출)
	- WRAP (텍스처의 데이터를 반복시켜서 데이터를 추출)
	- MIRROR (텍스처의 데이터를 반전시켜서 데이터를 추출)
	- BORDER (설정 된 데이터를 추출)

	Address Mode 란?
	:
	- UV 좌표의 범위는 기본적으로 0 ~ 1 의 범위를 지니고 있지만 해당
	범위를 넘어갔을 경우 데이터를 샘플링하는 방식을 지정하는 것을 
	의미합니다.
	*/
	// 샘플러 상태를 생성한다
	// {
	D3D11_SAMPLER_DESC stSamplerDesc;
	ZeroMemory(&stSamplerDesc, sizeof(stSamplerDesc));

	stSamplerDesc.Filter = a_eFilter;
	stSamplerDesc.AddressU = a_eAddressU;
	stSamplerDesc.AddressV = a_eAddressV;
	stSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	stSamplerDesc.ComparisonFunc = D3D11_COMPARISON_EQUAL;
	stSamplerDesc.MaxAnisotropy = 1;
	stSamplerDesc.MinLOD = 0.0f;
	stSamplerDesc.MaxLOD = FLT_MAX;
	stSamplerDesc.MipLODBias = 0;

	// 테두리 색상을 설정한다
	CopyMemory(stSamplerDesc.BorderColor,
		(float *)D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), sizeof(D3DXCOLOR));

	ID3D11SamplerState *pSamplerState = nullptr;
	GET_DEVICE()->CreateSamplerState(&stSamplerDesc, &pSamplerState);
	// }

	return pSamplerState;
}

ID3D11RasterizerState * CreateRasterizerState(D3D11_FILL_MODE a_eFillMode, D3D11_CULL_MODE a_eCullMode)
{
	// 래스터라이저 상태를 생성한다
	// {
	D3D11_RASTERIZER_DESC stRasterizerDesc;
	ZeroMemory(&stRasterizerDesc, sizeof(stRasterizerDesc));

	stRasterizerDesc.FillMode = a_eFillMode;
	stRasterizerDesc.CullMode = a_eCullMode;

	ID3D11RasterizerState *pRasterizerState = nullptr;
	GET_DEVICE()->CreateRasterizerState(&stRasterizerDesc, &pRasterizerState);
	// }

	return pRasterizerState;
}

ID3D11BlendState * CreateBlendState(bool a_bIsBlendEnable, D3D11_BLEND a_eSourceBlend, D3D11_BLEND a_eDestinationBlend, D3D11_BLEND_OP a_eBlendOperation)
{
	// 블렌드 상태를 생성한다
	// {
	D3D11_BLEND_DESC stBlendDesc;
	ZeroMemory(&stBlendDesc, sizeof(stBlendDesc));

	stBlendDesc.RenderTarget[0].BlendEnable = a_bIsBlendEnable;

	stBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	stBlendDesc.RenderTarget[0].BlendOp = a_eBlendOperation;

	stBlendDesc.RenderTarget[0].SrcBlend = a_eSourceBlend;
	stBlendDesc.RenderTarget[0].DestBlend = a_eDestinationBlend;

	stBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	stBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;

	stBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	ID3D11BlendState *pBlendState = nullptr;
	GET_DEVICE()->CreateBlendState(&stBlendDesc, &pBlendState);
	// }

	return pBlendState;
}

ID3D11ShaderResourceView * CreateShaderResourceView(const std::string & a_rFilepath)
{
	// 쉐이더 리소스 뷰를 생성한다
	// {
	ID3D11ShaderResourceView *pShaderResourceView = nullptr;

	if (a_rFilepath.rfind(".tga") == std::string::npos &&
		a_rFilepath.rfind(".dds") == std::string::npos) 
	{
		D3DX11CreateShaderResourceViewFromFileA(GET_DEVICE(),
			a_rFilepath.c_str(),
			NULL,
			NULL,
			&pShaderResourceView,
			NULL);
	}
	else {
		DirectX::TexMetadata stMetadata;
		DirectX::ScratchImage oScratchImage;

		// 유니코드 문자열을 설정한다
		// {
		WCHAR szFilepath[MAX_PATH] = L"";

		MultiByteToWideChar(CP_ACP,
			0, a_rFilepath.c_str(), a_rFilepath.length(), szFilepath, sizeof(szFilepath) / sizeof(WCHAR));
		// }

		// 이미지 정보를 설정한다
		if (a_rFilepath.rfind(".tga") != std::string::npos) {
			DirectX::LoadFromTGAFile(szFilepath, &stMetadata, oScratchImage);
		}
		else {
			DirectX::LoadFromDDSFile(szFilepath,
				DirectX::DDS_FLAGS_NONE, &stMetadata, oScratchImage);
		}

		DirectX::CreateShaderResourceView(GET_DEVICE(),
			oScratchImage.GetImages(),
			oScratchImage.GetImageCount(),
			stMetadata,
			&pShaderResourceView);
	}
	// }

	return pShaderResourceView;
}
