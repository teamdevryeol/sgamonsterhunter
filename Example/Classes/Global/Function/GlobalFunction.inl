#include "GlobalFunction.h"
#include "../Define/KGlobalDefine.h"

template<typename T>
inline T ClampValue(T a_tValue, T a_tMinValue, T a_tMaxValue)
{
	if (a_tMinValue > a_tMaxValue) {
		std::swap(a_tMinValue, a_tMaxValue);
	}

	a_tValue = max(a_tMinValue, a_tValue);
	a_tValue = min(a_tMaxValue, a_tValue);

	return a_tValue;
}

template<typename T>
inline std::unique_ptr<T, std::function<void(T*)>> GetBufferPointer(ID3D11Resource * a_pResource)
{
	auto oBufferPointer = std::unique_ptr<T, std::function<void(T *)>>(nullptr, 
		[=](T *a_pvPointer) -> void 
	{
		if (a_pvPointer != nullptr) {
			GET_CONTEXT()->Unmap(a_pResource, 0);
		}
	});

	D3D11_MAPPED_SUBRESOURCE stSubResource;
	ZeroMemory(&stSubResource, sizeof(stSubResource));

	if (SUCCEEDED(GET_CONTEXT()->Map(a_pResource,
		0, D3D11_MAP_WRITE_DISCARD, 0, &stSubResource)))
	{
		oBufferPointer.reset((T *)stSubResource.pData);
	}

	return oBufferPointer;
}

template<typename T, typename ...ARGS>
inline T * Create(ARGS && ...args)
{
	return new T(args...);
}

