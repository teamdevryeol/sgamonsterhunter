#pragma once
#pragma warning(disable:4005)

#include <iostream>
#include <cassert>
#include <functional>
#include <algorithm>
#include <random>
#include <chrono>
#include <memory>
#include <numeric>
#include <queue>
#include <array>
#include <unordered_map>
#include <Windows.h>
#include <tchar.h>
#include <d3dx9.h>
#include <D3DX11.h>
#include <D3DX10.h>
#include <DXGI.h>
#include <dinput.h>
#include <dsound.h>
#include "DirectXTex.h"


// 기타
#define MAX_NUM_BLENDS			(4)
//#define SOUND_MANAGER_ENABLE

#define CC_SYNTHESIZE(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void) const { return varName; }\
public: virtual void set##funName(varType var){ varName = var; } 

#define CC_SYNTHESIZE_READONLY(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void) const { return varName; }

// 방향
#define NUM_DIRECTIONS					(3)
#define WORLD_RIGHT_DIRECTION			(D3DXVECTOR3(1.0f, 0.0f, 0.0f))
#define WORLD_UP_DIRECTION				(D3DXVECTOR3(0.0f, 1.0f, 0.0f))
#define WORLD_FORWARD_DIRECTION			(D3DXVECTOR3(0.0f, 0.0f, 1.0f))

// 식별자
// {
#define KEY_BOX_MESH				(std::string("BoxMesh"))
#define KEY_SPHERE_MESH				(std::string("SphereMesh"))
#define KEY_CYLINDER_MESH			(std::string("CylinderMesh"))
#define KEY_TEAPOT_MESH				(std::string("TeapotMesh"))
#define KEY_TORUS_MESH				(std::string("TorusMesh"))
#define KEY_PLANE_MESH				(std::string("PlaneMesh"))

#define KEY_COLOR_SHADER				(std::string("ColorShader"))
#define KEY_COLOR_INPUT_LAYOUT			(std::string("ColorInputLayout"))
#define KEY_COLOR_RENDER_BUFFER			(std::string("ColorRenderBuffer"))

#define KEY_COLOR_LIGHT_SHADER					(std::string("ColorLightShader"))
#define KEY_COLOR_LIGHT_INPUT_LAYOUT			(std::string("ColorLightInputLayout"))
#define KEY_COLOR_LIGHT_RENDER_BUFFER			(std::string("ColorLightRenderBuffer"))

#define KEY_COLOR_TEXTURE_SHADER				(std::string("ColorTextureShader"))
#define KEY_COLOR_TEXTURE_INPUT_LAYOUT			(std::string("ColorTextureInputLayout"))
#define KEY_COLOR_TEXTURE_RENDER_BUFFER			(KEY_COLOR_RENDER_BUFFER)

#define KEY_TEXTURE_SHADER					(std::string("TextureShader"))
#define KEY_TEXTURE_INPUT_LAYOUT			(std::string("TextureInputLayout"))
#define KEY_TEXTURE_RENDER_BUFFER			(KEY_COLOR_RENDER_BUFFER)

#define KEY_TEXTURE_LIGHT_SHADER				(std::string("TextureLightShader"))
#define KEY_TEXTURE_LIGHT_INPUT_LAYOUT			(std::string("TextureLightInputLayout"))
#define KEY_TEXTURE_LIGHT_RENDER_BUFFER			(KEY_COLOR_LIGHT_RENDER_BUFFER)

#define KEY_STATIC_MESH_SHADER					(std::string("StaticMeshShader"))
#define KEY_STATIC_MESH_INPUT_LAYOUT			(std::string("StaticMeshInputLayout"))
#define KEY_STATIC_MESH_RENDER_BUFFER			(KEY_COLOR_LIGHT_RENDER_BUFFER)

#define KEY_SKINNED_MESH_SHADER					(std::string("SkinnedMeshShader"))
#define KEY_SKINNED_MESH_INPUT_LAYOUT			(std::string("SkinnedMeshInputLayout"))
#define KEY_SKINNED_MESH_RENDER_BUFFER			(std::string("SkinnedMeshRenderBuffer"))

#define KEY_TERRAIN_SHADER					(std::string("TerrainShader"))
#define KEY_TERRAIN_INPUT_LAYOUT			(KEY_STATIC_MESH_INPUT_LAYOUT)
#define KEY_TERRAIN_RENDER_BUFFER			(KEY_COLOR_LIGHT_RENDER_BUFFER)

#define KEY_TRANSFORM_BUFFER			(std::string("TransformBuffer"))

#define KEY_SOLID_RASTERIZER_STATE				(std::string("SolidRasterizerState"))
#define KEY_WIREFRAME_RASTERIZER_STATE			(std::string("WireframeRasterizerState"))

#define KEY_ALPHA_BLEND_ENABLE_STATE			(std::string("AlphaBlendEnableState"))
#define KEY_ALPHA_BLEND_DISABLE_STATE			(std::string("AlphaBlendDisableState"))
// }

// 입력 레이아웃
// {
#define VERTEX_ELEMENT(USAGE, INDEX, FORMAT, OFFSET)						(D3DVERTEXELEMENT9 { 0, (OFFSET), (FORMAT), D3DDECLMETHOD_DEFAULT, (USAGE), (INDEX) })
#define INPUT_ELEMENT_DESC(SEMANTIC_NAME, INDEX, FORMAT, OFFSET)			(D3D11_INPUT_ELEMENT_DESC { (SEMANTIC_NAME), (INDEX), (FORMAT), 0, (OFFSET), D3D11_INPUT_PER_VERTEX_DATA, 0 })

#define INPUT_COLOR_ELEMENT_DESCS																			\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																		\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),										\
	INPUT_ELEMENT_DESC("COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)			\
}

#define INPUT_COLOR_LIGHT_ELEMENT_DESCS																		\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																		\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),										\
	INPUT_ELEMENT_DESC("NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),				\
	INPUT_ELEMENT_DESC("COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)			\
}

#define INPUT_COLOR_TEXTURE_ELEMENT_DESCS																	\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																		\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),										\
	INPUT_ELEMENT_DESC("COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),			\
	INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),				\
}

#define INPUT_TEXTURE_ELEMENT_DESCS																		\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																	\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),									\
	INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)			\
}

#define INPUT_TEXTURE_LIGHT_ELEMENT_DESCS																\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																	\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),									\
	INPUT_ELEMENT_DESC("NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),			\
	INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)			\
}

#define INPUT_STATIC_MESH_ELEMENT_DESCS																		\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																		\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),										\
	INPUT_ELEMENT_DESC("NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),				\
	INPUT_ELEMENT_DESC("BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),			\
	INPUT_ELEMENT_DESC("TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),			\
	INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)				\
}

#define INPUT_SKINNED_MESH_ELEMENT_DESCS																		\
std::vector<D3D11_INPUT_ELEMENT_DESC> {																			\
	INPUT_ELEMENT_DESC("POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0),											\
	INPUT_ELEMENT_DESC("NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),					\
	INPUT_ELEMENT_DESC("BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),				\
	INPUT_ELEMENT_DESC("TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),				\
	INPUT_ELEMENT_DESC("BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),			\
	INPUT_ELEMENT_DESC("TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)					\
}
// }

// 메모리 관리
// {
#define SAFE_FREE(TARGET)					if((TARGET) != nullptr) { free((TARGET)); (TARGET) = nullptr; }
#define SAFE_DELETE(TARGET)					if((TARGET) != nullptr) { delete (TARGET); (TARGET) = nullptr; }
#define SAFE_DELETE_ARRAY(TARGET)			if((TARGET) != nullptr) { delete[] (TARGET); (TARGET) = nullptr; }

#define SAFE_RELEASE(TARGET)			if((TARGET) != nullptr) { (TARGET)->Release(); (TARGET) = nullptr; }
#define SAFE_UNACQUIRE(TARGET)			if((TARGET) != nullptr) { (TARGET)->Unacquire(); SAFE_RELEASE((TARGET)); }
// }

// 카메라
#define GET_VIEW_MATRIX()				(GET_CAMERA()->getViewMatrix())
#define GET_PROJECTION_MATRIX()			(GET_CAMERA()->getProjectionMatrix())

// 쉐이더
// {
#define GET_VERTEX_SHADER_CODE(KEY)			((GET_SHADER((KEY)) == nullptr)? nullptr : GET_SHADER((KEY))->getVertexShaderCode())
#define GET_PIXEL_SHADER_CODE(KEY)			((GET_SHADER((KEY)) == nullptr)? nullptr : GET_SHADER((KEY))->getPixelShaderCode())

#define GET_VERTEX_SHADER(KEY)			((GET_SHADER((KEY)) == nullptr)? nullptr : GET_SHADER((KEY))->getVertexShader())
#define GET_PIXEL_SHADER(KEY)			((GET_SHADER((KEY)) == nullptr)? nullptr : GET_SHADER((KEY))->getPixelShader())
// }

// 윈도우 관리자
#define GET_WINDOW_SIZE()				(GET_WINDOW_MANAGER()->getWindowSize())
#define GET_WINDOW_HANDLE()				(GET_WINDOW_MANAGER()->getWindowHandle())
#define GET_INSTANCE_HANDLE()			(GET_WINDOW_MANAGER()->getInstanceHandle())

// 디바이스 관리자
// {
#define GET_DIRECTED()					(GET_DEVICE_MANAGER()->getDirect3D())
#define GET_9VERSION_DEVICE()			(GET_DEVICE_MANAGER()->get9VersionDevice())

#define GET_FACTORY()						(GET_DEVICE_MANAGER()->getFactory())
#define GET_SWAP_CHAIN()					(GET_DEVICE_MANAGER()->getSwapChain())
#define GET_DEVICE()						(GET_DEVICE_MANAGER()->getDevice())
#define GET_CONTEXT()						(GET_DEVICE_MANAGER()->getContext())
#define GET_RENDER_TARGET_VIEW()			(GET_DEVICE_MANAGER()->getRenderTargetView())
#define GET_DEPTH_STENCIL_VIEW()			(GET_DEVICE_MANAGER()->getDepthStencilView())
// }

// 시간 관리자
#define GET_DELTA_TIME()			(GET_TIME_MANAGER()->getDeltaTime())
#define GET_RUNNING_TIME()			(GET_TIME_MANAGER()->getRunningTime())

// 입력 관리자
// {
#define IS_KEY_DOWN(KEY_CODE)				(GET_INPUT_MANAGER()->isKeyDown((KEY_CODE)))
#define IS_KEY_PRESSED(KEY_CODE)			(GET_INPUT_MANAGER()->isKeyPressed((KEY_CODE)))
#define IS_KEY_RELEASED(KEY_CODE)			(GET_INPUT_MANAGER()->isKeyReleased((KEY_CODE)))

#define IS_MOUSE_BUTTON_DOWN(MOUSE_BUTTON)				(GET_INPUT_MANAGER()->isMouseButtonDown((MOUSE_BUTTON)))
#define IS_MOUSE_BUTTON_PRESSED(MOUSE_BUTTON)			(GET_INPUT_MANAGER()->isMouseButtonPressed((MOUSE_BUTTON)))
#define IS_MOUSE_BUTTON_RELEASED(MOUSE_BUTTON)			(GET_INPUT_MANAGER()->isMouseButtonReleased((MOUSE_BUTTON)))

#define GET_MOUSE_WHEEL()				(GET_INPUT_MANAGER()->getMouseWheel())
#define GET_MOUSE_POSITION()			(GET_INPUT_MANAGER()->getMousePosition())

#define GET_DIRECT_INPUT()				(GET_INPUT_MANAGER()->getDirectInput())
#define GET_KEYBOARD_DEVICE()			(GET_INPUT_MANAGER()->getKeyboardDevice())
#define GET_MOUSE_DEVICE()				(GET_INPUT_MANAGER()->getMouseDevice())
// }

// 사운드 관리자
// {
#define GET_DIRECT_SOUND()				(GET_SOUND_MANAGER()->getDirectSound())
#define GET_PRIMARY_BUFFER()			(GET_SOUND_MANAGER()->getPrimaryBuffer())

#define PLAY_EFFECT_SOUND(FILE_PATH)				(GET_SOUND_MANAGER()->playEffectSound((FILE_PATH)))
#define PLAY_BACKGROUND_SOUND(FILE_PATH)			(GET_SOUND_MANAGER()->playBackgroundSound((FILE_PATH)))
// }

// 리소스 관리자
// {
#define GET_MATERIAL(KEY)			(GET_RESOURCE_MANAGER()->getMaterial((KEY)))
#define GET_WAVE_SOUND(KEY)			(GET_RESOURCE_MANAGER()->getWaveSound((KEY)))

#define GET_MESH(KEY)							(GET_RESOURCE_MANAGER()->getMesh((KEY)))
#define GET_SHADER(KEY)							(GET_RESOURCE_MANAGER()->getShader((KEY)))
#define GET_BUFFER(KEY)							(GET_RESOURCE_MANAGER()->getBuffer((KEY)))
#define GET_INPUT_LAYOUT(KEY)					(GET_RESOURCE_MANAGER()->getInputLayout((KEY)))
#define GET_RASTERIZER_STATE(KEY)				(GET_RESOURCE_MANAGER()->getRasterizerState((KEY)))
#define GET_BLEND_STATE(KEY)					(GET_RESOURCE_MANAGER()->getBlendState((KEY)))
#define GET_SHADER_RESOURCE_VIEW(KEY)			(GET_RESOURCE_MANAGER()->getShaderResourceView((KEY)))
// }

// 다이렉트 3D 어플리케이션
#define GET_LIGHT()					(GET_DIRECT3D_APPLICATION()->getLight())
#define GET_CAMERA()				(GET_DIRECT3D_APPLICATION()->getCamera())
#define GET_RENDER_OBJECT()			(GET_DIRECT3D_APPLICATION()->getRenderObject())

// 관리자
#define GET_WINDOW_MANAGER()			(CWindowManager::getInstance())
#define GET_DEVICE_MANAGER()			(CDeviceManager::getInstance())
#define GET_TIME_MANAGER()				(CTimeManager::getInstance())
#define GET_INPUT_MANAGER()				(CInputManager::getInstance())
#define GET_SOUND_MANAGER()				(CSoundManager::getInstance())
#define GET_RESOURCE_MANAGER()			(CResourceManager::getInstance())

// 어플리케이션
#define GET_WINDOW_APPLICATION()			(CWindowApplication::getInstance())
#define GET_DIRECT3D_APPLICATION()			((CDirect3DApplication *)GET_WINDOW_APPLICATION())

//! 싱글턴
#define DECLARE_SINGLETON(CLASS_NAME)			\
private:										\
CLASS_NAME(void);								\
~CLASS_NAME(void);								\
public:											\
static CLASS_NAME * getInstance(void) {			\
	static CLASS_NAME oInstance;				\
	return &oInstance;							\
}

//! 주시 타입
enum class EFollowType
{
	LOCK,
	FREE,
	NONE
};


//! 객체 타입
enum class EObjectType
{
	OBJECT,
	RENDER_OBJECT,
	UI_OBJECT,
	NONE
};

//! 마우스 버튼
enum class EMouseButton
{
	LEFT,
	RIGHT,
	MIDDLE,
	NONE
};

//! 정적 메시 타입
enum class EStaticMeshType
{
	BOX,
	SPHERE,
	CYLINDER,
	TEAPOT,
	TORUS,
	PLANE,
	XMESH,
	NONE
};

//! 디버그 렌더 타입
enum class EDebugRenderType
{
	BOX,
	SPHERE,
	NONE
};

//! 색상 정점
struct STColorVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXCOLOR m_stColor;
};

//! 색상 광원 정점
struct STColorLightVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR3 m_stNormal;
	D3DXCOLOR m_stColor;
};

//! 색상 텍스처 정점
struct STColorTextureVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXCOLOR m_stColor;
	D3DXVECTOR2 m_stUV;
};

//! 텍스처 정점
struct STTextureVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR2 m_stUV;
};

//! 텍스처 광원 정점
struct STTextureLightVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR3 m_stNormal;
	D3DXVECTOR2 m_stUV;
};

//! 정적 메시 정점
struct STStaticMeshVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR3 m_stNormal;
	D3DXVECTOR3 m_stBinormal;
	D3DXVECTOR3 m_stTangent;
	D3DXVECTOR2 m_stUV;
};

//! 스킨드 메시 정점
struct STSkinnedMeshVertex
{
	D3DXVECTOR3 m_stPosition;
	D3DXVECTOR3 m_stNormal;
	D3DXVECTOR3 m_stBinormal;
	D3DXVECTOR3 m_stTangent;
	D3DXVECTOR4 m_stBlendWeight;
	D3DXVECTOR2 m_stUV;
};

//! 변환
struct STTransform
{
	D3DXMATRIXA16 m_stWorldMatrix;
	D3DXMATRIXA16 m_stViewMatrix;
	D3DXMATRIXA16 m_stProjectionMatrix;
};

//! 렌더
struct STRender
{
	D3DXVECTOR4 m_stUVOffset;
};

//! 색상 렌더
struct STColorRender
{
	D3DXCOLOR m_stColor;
};

//! 색상 광원 렌더
struct STColorLightRender
{
	D3DXCOLOR m_stColor;

	D3DXVECTOR4 m_stViewPosition;
	D3DXVECTOR4 m_stLightDirection;
};

//! 스킨드 메시 렌더
struct STSkinnedMeshRender
{
	D3DXCOLOR m_stColor;

	D3DXVECTOR4 m_stViewPosition;
	D3DXVECTOR4 m_stLightDirection;

	D3DXVECTOR4 m_stNumBlends;
	D3DXMATRIXA16 m_astBoneMatrices[MAX_NUM_BLENDS];
};

//! 재질
struct STMaterial
{
	DWORD m_nNumMaterials;

	std::vector<ID3D11ShaderResourceView *> m_oNormalMapList;
	std::vector<ID3D11ShaderResourceView *> m_oDiffuseMapList;
};

//! 웨이브 사운드
struct STWaveSound
{
	BYTE *m_pnBytes;

	DWORD m_nNumBytes;
	WAVEFORMATEX m_stWaveFormat;
};

//! 광선
struct STRay
{
	D3DXVECTOR3 m_stOrigin;
	D3DXVECTOR3 m_stDirection;
};

//! 경계 상자
struct STBoundingBox
{
	D3DXVECTOR3 m_stMin;
	D3DXVECTOR3 m_stMax;
};

//! 객체 상자
struct STObjectBox
{
	D3DXVECTOR3 m_stCenter;

	float m_afHalfLengths[NUM_DIRECTIONS];
	D3DXVECTOR3 m_astDirections[NUM_DIRECTIONS];
};

//! 경계 구
struct STBoundingSphere
{
	float m_fRadius;
	D3DXVECTOR3 m_stCenter;
};
