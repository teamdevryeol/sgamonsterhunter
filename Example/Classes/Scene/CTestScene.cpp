#include "CTestScene.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Global/Utility/UI/CUIWindow.h"
#include "../Global/Utility/Object/CCamera.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"
#include "../Global/Utility/Manager/CBattleManager.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CLight.h"
#include "../Player/Player.h"
#include "../Monster/Monster.h"
#include "../Map/CMap.h"


CTestScene::CTestScene(HINSTANCE a_hInstance, int a_nShowOptions, SIZE& a_rstWindowSize)
:CDirect3DApplication(a_hInstance,a_nShowOptions,a_rstWindowSize)
{
	// Do Nothing
}

CTestScene::~CTestScene(void)
{
	// Do Nothing
}

void CTestScene::init(void)
{
	CDirect3DApplication::init();

	// 맵 생성
	m_pMap = Create<CMap>("BattleMap/Map");
	m_pMap->setScale(D3DXVECTOR3(0.5f, 0.5f, 0.5f));

	// UI 생성
	m_pUIRoot = Create<CUIWindow>();
	m_pUIRoot->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));

	// 캐릭터 생성
	m_pPlayer = Create<Player>();
	m_pPlayer->init();

	// 몬스터 생성
	m_pMonster = Create<Monster>();
	m_pMonster->init();

	// 카메라 설정
	GET_CAMERA()->setTargetObject(m_pPlayer->m_pBaseMesh,
		D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(0.0f, 30.0f, -15.0f),
		EFollowType::FREE);
	

	// 배틀매니저 생성
	m_pBTManager = Create<CBattleManager>();
	m_pBTManager->init(m_pPlayer, m_pMonster);

	// 밝기 설정
	GET_LIGHT()->setPosition(D3DXVECTOR3(0.0f, 5.0f, 0.0f));
	GET_LIGHT()->rotateByRightDirection(90.0f, true);
}

void CTestScene::update(void)
{
	CDirect3DApplication::update();
	m_pPlayer->update();
	m_pPlayer->setHeight(m_pMap->getHeightAtPosition(m_pPlayer->getPosition()));
	m_pMonster->update();
	m_pMap->update();
	m_pUIRoot->update();

	m_pBTManager->update();

	//if(!m_pBTManager->getCollision())
	//{
	//	GET_CAMERA()->setForwardDirection(D3DXVECTOR3(m_pPlayer->m_pBaseMesh->getRightDirection/().x, /m_pPlayer->m_pBaseMesh->getRightDirection().y - 0.3f, m_pPlayer->m_pBaseMesh-//>getRightDirection().z));
	//}
	if (!IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT) && !m_pBTManager->getCollision())
	{
		GET_CAMERA()->setForwardDirection(D3DXVECTOR3(m_pPlayer->m_pBaseMesh->getRightDirection().x, m_pPlayer->m_pBaseMesh->getRightDirection().y - 0.3f, m_pPlayer->m_pBaseMesh->getRightDirection().z));
	}
}

void CTestScene::render(void)
{
	CDirect3DApplication::render();
	m_pPlayer->render();
	m_pMonster->render();
	m_pMap->render();
	m_pUIRoot->render();

	m_pBTManager->render();
}
