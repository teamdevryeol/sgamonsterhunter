#pragma once
#include "../Global/Define/KGlobalDefine.h"
#include "../Global/Utility/Base/CDirect3DApplication.h"

class Player;
class Monster;
class CMap;
class CUIWindow;
class CBattleManager;

class CTestScene : public CDirect3DApplication
{

public:				// 생성자, 소멸자

	//! 생성자
	CTestScene(HINSTANCE a_hInstance, int a_nShowOptions, SIZE& a_rstWindowSize);

	//! 소멸자
	virtual ~CTestScene(void) override;

protected:			// protected 함수
	virtual void init(void) override;

public:				// public 함수
	virtual void update(void) override;
	virtual void render(void) override;


private:			// private 함수

private:			// private 변수

	CBattleManager* m_pBTManager = nullptr;
	Player* m_pPlayer = nullptr;
	Monster* m_pMonster = nullptr;
	CMap *m_pMap = nullptr;
	CUIWindow *m_pUIRoot = nullptr;
};