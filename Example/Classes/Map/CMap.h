#pragma once

#include "../Global/Define/KGlobalDefine.h"
#include "../Global/Utility/Object/CStaticMesh.h"

class CTerrain;

class CMap : public CStaticMesh
{
public:			// getter

	//! 높이를 반환한다
	float getHeightAtPosition(const D3DXVECTOR3 &a_rstPosition);
	
	void setScale(const D3DXVECTOR3 &a_rstScale);

public:

	CC_SYNTHESIZE_READONLY(std::string, m_oFilepath, Filepath);

public:			// 생성자

	//! 생성자
	CMap(const std::string a_oFilepath, float a_fHeightCorrectionValue = 80.0f);

private:			// private 변수

	float m_fHeightCorrectionValue = 0.0f;

	CTerrain *m_pTerrain = nullptr;

	STMaterial m_stMaterial;
	STParameters m_stStaticParameters;
};