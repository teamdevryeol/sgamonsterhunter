#include "Monster.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CStaticMesh.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"
#include "../Global/Function/GlobalFunction.h"

Monster::Monster()
{
}

Monster::~Monster()
{
}

void Monster::init()
{
	this->createMesh();

	backStatic->setPosition(D3DXVECTOR3(-800.0f, 0.0f, 0.0f));
	backStatic->setScale(D3DXVECTOR3(600.0f, 300.0f, 600.0f));
	backStatic->setRenderEnable(false);
	

	frontStatic->setPosition(D3DXVECTOR3(500.0f, 0.0f, 0.0f));
	frontStatic->setScale(D3DXVECTOR3(600.0f, 300.0f, 600.0f));
	frontStatic->setRenderEnable(false);


	bodyStatic->setScale(D3DXVECTOR3(300.0f, 150.0f, 150.0f));
	bodyStatic->setRenderEnable(false);
	

	tailStatic->setScale(D3DXVECTOR3(300.0f, 300.0f, 300.0f));
	tailStatic->setRenderEnable(false);


	hipStatic->setScale(D3DXVECTOR3(300.0f, 300.0f, 200.0f));


	tailPosition->addChildObject(tailStatic);
	bodyPosition->addChildObject(bodyStatic);
	hipPosition->addChildObject(hipStatic);

	monster->setPosition(D3DXVECTOR3(0.0f, 0.0f, -20.0f));
	monster->setScale(D3DXVECTOR3(0.02f, 0.02f, 0.02f));
	monster->rotateByUpDirection(-90.0f);
	monster->setTimeScale(1.5f);
	monster->addChildObject(backStatic);
	monster->addChildObject(frontStatic);

	auto& rAnimationNameList = monster->getAnimationNameList();

	monster->playAnimation(rAnimationNameList[2], true);

	monsterDirection = m_stGoalPosition - monster->getPosition();
	D3DXVec3Normalize(&monsterDirection, &monsterDirection);

	this->m_fHP = 200.0f;
	this->m_fTailHP = 100.0f;
}

void Monster::update()
{
	monster->update();
	tailPosition->update();
	bodyPosition->update();
	hipPosition->update();

	this->setHeightMap(monster);

	tailPosition->setWorldMatrix(monster->getBoneMatrix("TL-05"));
	bodyPosition->setWorldMatrix(monster->getBoneMatrix("HD-00"));
	hipPosition->setWorldMatrix(monster->getBoneMatrix("WST-00"));

	auto &rBoundingBoxList = tailStatic->getFinalObjectBoxList();
	auto &rBoundingBoxList2 = backStatic->getFinalObjectBoxList();

	auto& rAnimationNameList = monster->getAnimationNameList();

	if (!tailAttack && !m_bIsGoal && !idle && !runBiteAttack && !biteAttack)
	{
		if (prevAc == EPrevAc::TAIL_ATTACK)
		{
			monster->rotateByUpDirection(-90.0f);
			prevAc = EPrevAc::NONE;
		}
		else if (prevAc == EPrevAc::RUN_BITE)
		{
			prevAc = EPrevAc::NONE;
		}
		else if (prevAc == EPrevAc::BITE)
		{
			prevAc = EPrevAc::NONE;
		}
		monster->playAnimation(rAnimationNameList[2], true);
		idle = true;
	}

	this->tailAttackPattern();
	this->runBitePattern();
	this->bitePattern();

	if (IS_KEY_PRESSED(DIK_4))
	{
		m_bIsGoal = true;
		tailAttack = false;
		monster->playAnimation(rAnimationNameList[0], true);
	}

	if (IS_KEY_PRESSED(DIK_1))
	{
		tailAttack = true;
	}
	else if (IS_KEY_PRESSED(DIK_2))
	{
		runBiteAttack = true;
	}
	else if (IS_KEY_PRESSED(DIK_3))
	{
		biteAttack = true;
	}

	if (m_bIsGoal)
	{
		D3DXVECTOR3 monsterRightDirection;
		D3DXVec3Cross(&monsterRightDirection, &monster->getUpDirection(), &monsterDirection);
		float dlength = D3DXVec3Length(&(monsterRightDirection - monster->getForwardDirection()));
		if (dlength > 0.1f)
		{
			monster->rotateByUpDirection(-50.0f * GET_DELTA_TIME(), true);
		}
	
		monster->moveByRightDirection(5.0f * GET_DELTA_TIME());
		D3DXVECTOR3 monsterDirection = m_stGoalPosition - monster->getPosition();
		float length = D3DXVec3Length(&monsterDirection);
	
		if (length < 10.0f)
		{
			m_bIsGoal = false;
			monster->stopAnimation();
		}
	}
}

void Monster::render()
{
	monster->render();
}

void Monster::tailAttackPattern()
{
	if (tailAttack)
	{
		idle = false;
		auto& rAnimationNameList = monster->getAnimationNameList();
		if (!aAnimationRun && (monster->getDblPercent() > 0.95f || prevAc == EPrevAc::NONE))
		{
			prevAc = EPrevAc::TAIL_ATTACK;
			aAnimationRun = true;
			if (attackVecIndex == 1)
			{
				tailAttack = false;
				attackVecIndex = 0;
				return;
			}
			monster->playAnimation(rAnimationNameList[1], false);
			attackVecIndex++;
		}
		else if (monster->getDblPercent() < 0.95f)
		{
			aAnimationRun = false;
		}
	}
}

void Monster::runBitePattern()
{
	if (runBiteAttack)
	{
		idle = false;
		auto& rAnimationNameList = monster->getAnimationNameList();
		if (!aAnimationRun)
		{
			monster->playAnimation(rAnimationNameList[5], false);
			aAnimationRun = true;
		}
		prevAc = EPrevAc::RUN_BITE;
		if (monster->getDblPercent() > 0.95f)
		{
			runBiteAttack = false;
			aAnimationRun = false;
		}
	}
}

void Monster::bitePattern()
{
	if (biteAttack)
	{
		idle = false;
		auto& rAnimationNameList = monster->getAnimationNameList();
		if (!biteAnimationRun)
		{
			monster->playAnimation(rAnimationNameList[4], false);
			biteAnimationRun = true;
		}
		prevAc = EPrevAc::BITE;
		if (monster->getDblPercent() > 0.95f)
		{
			biteAttack = false;
			biteAnimationRun = false;
		}
	}
}

void Monster::setHeight(float a_fHeight)
{
	auto prevPos = monster->getPosition();
	monster->setPosition(D3DXVECTOR3(prevPos.x, a_fHeight, prevPos.z));
}

float Monster::getHP()
{
	return m_fHP;
}

float Monster::getTailHP()
{
	return m_fTailHP;
}

std::vector<STBoundingBox> Monster::getTailBoundingBoxList()
{
	return tailStatic->getFinalBoundingBoxList();
}

float Monster::getHeight()
{
	return m_fHeight;
}

STObjectBox & Monster::getBodyObjectBox()
{
	return bodyStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getHipObjectBox()
{
	return hipStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getFrontObjectBox()
{
	return frontStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getBackObjectBox()
{
	return backStatic->getFinalObjectBoxList()[0];
}

STObjectBox & Monster::getTailObjectBox()
{
	return tailStatic->getFinalObjectBoxList()[0];
}

void Monster::setHP(float a_fHP)
{
	m_fHP = a_fHP;
}

void Monster::setTailHP(float a_fTailHP)
{
	m_fTailHP = a_fTailHP;
}

void Monster::setHeightMap(CSkinnedMesh * a_pMesh)
{
	{
		auto pstPos = a_pMesh->getPosition();
		a_pMesh->setPosition(D3DXVECTOR3(pstPos.x,
			m_fHeight + 2.5f, pstPos.z));
	}
}

void Monster::createMesh()
{
	monster = new CSkinnedMesh(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Monster/TestMonster.X",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,

		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	tailPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	tailStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	backStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	frontStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	bodyStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	bodyPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	hipPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	hipStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
}


