#pragma once

#include "../Global/Define/KGlobalDefine.h"

class CSkinnedMesh;
class CStaticMesh;

class Monster
{
public:
	enum class EPrevAc
	{
		NONE,
		TAIL_ATTACK,
		RUN_BITE,
		BITE
	};

public:
	Monster();
	virtual ~Monster();

public:
	void init();
	void update();
	void render();

	void tailAttackPattern();
	void runBitePattern();
	void bitePattern();
	void setHeight(float a_fHeight);

public:			// getter, setter

	float getHP();

	float getTailHP();

	float getHeight();

	STObjectBox& getBodyObjectBox();

	STObjectBox& getHipObjectBox();

	STObjectBox& getFrontObjectBox();

	STObjectBox& getBackObjectBox();

	STObjectBox& getTailObjectBox();
	
	std::vector<STBoundingBox> getTailBoundingBoxList();

	void setHP(float a_fHP);

	void setTailHP(float a_fTailHP);

	void setHeightMap(CSkinnedMesh * a_pMesh);

private:

	void createMesh();

private:
	CSkinnedMesh* monster;
	
	bool m_bIsGoal = false;
	bool idle = true;

	bool tailAttack = false;
	bool runBiteAttack = false;
	bool biteAttack = false;

	bool aAnimationRun = false;
	bool biteAnimationRun = false;

	int attackVecIndex = 0;

	float m_fHP;
	float m_fTailHP;
	float m_fHeight;

	std::string m_sCurrentAnimationName;

	D3DXVECTOR3 m_stGoalPosition;
	D3DXVECTOR3 monsterDirection;

	CStaticMesh* tailPosition;
	CStaticMesh* bodyPosition;
	CStaticMesh* hipPosition;

	CStaticMesh* tailStatic;
	CStaticMesh* backStatic;
	CStaticMesh* frontStatic;
	CStaticMesh* bodyStatic;
	CStaticMesh* hipStatic;

	EPrevAc prevAc;

};